# IS-ESM 

Service Request Solution built on the SharePoint platform with C# and Javascript used by the Information System department for sending various network requests. It is a SharePoint Workflow solution with levels of approvals

The Approval workflow consist of two approvals only. From the REQUESTER to the LINE MANAGER to the ESM TEAM and finally goes to the ESM MANAGER for final approval on the request. This solution has 3 functional pages only;

*  New Form: this form is used by the requester to raise a request.

*  ISESMServiceRequestWFTask - used to view contents of the request to be approved. This page has four event buttons;
1.  Approve - upon click of this button, the workflow proceeds to the next phase
2.  Update Request - upon click of this button, the workflow goes back to the Requester who is the ownwer of the request. This page is used if there is an error on the form filled by the requester, a comment is passed by the approval who is requesting for this update. This comment is also attached to the mail sent back to the Requester
3.  Reject - upon click of this button, the workflow terminates signifying the end of the request
4.  Close - upon click of this button, the request is closed. Note: No action is been carried out on the workflow. This is just for the Approval to view and close the form without any action taken

NOTE: this form is a preview only form. Only the comment box is available for the approval to use

*  Update Request Page - this page is used by the Requester, this is the page where the Requester will be able to have a view of the previous form filled and will be able to alter the information filled earlier.


