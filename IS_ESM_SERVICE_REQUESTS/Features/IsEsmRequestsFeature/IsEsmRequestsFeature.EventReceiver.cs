using System;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;

namespace IS_ESM_SERVICE_REQUESTS.Features.IsEsmRequestsFeature
{
	/// <summary>
	/// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
	/// </summary>
	/// <remarks>
	/// The GUID attached to this class may be used during packaging and should not be modified.
	/// </remarks>

	[Guid("6a4f9691-2fd6-4cbc-8073-a11756c1500f")]
	public class IsEsmRequestsFeatureEventReceiver : SPFeatureReceiver
	{
		// Uncomment the method below to handle the event raised after a feature has been activated.

		public override void FeatureActivated(SPFeatureReceiverProperties properties)
		{
			SPWeb web = (SPWeb)properties.Feature.Parent;
			CreateSiteGroups(web.Site.RootWeb);
			AddRequestTasksList(web, properties);
			SPList overtimeHistoryList = AddRequestWorkflowHistoryList(web, properties);
		}

		private SPList AddRequestTasksList(SPWeb web, SPFeatureReceiverProperties properties)
		{
			string taskFormRelativeUrl = "/_layouts/EsmServiceRequest/ISESMServiceRequestWFTask.aspx";// SPUtility.GetLocalizedString("$Resources:Pages_WFTaskFormRelativeUrl", "TravelRequestFile", 1033);
			string CashAdvTasksListInternalName = "ISESMServiceRequestTasks";
			SPList requestTasksList = null;
			try
			{
				string listUrl = String.Format("{0}/lists/{1}", web.Url, CashAdvTasksListInternalName);
				requestTasksList = web.GetList(listUrl);
			}
			catch (FileNotFoundException e)
			{
				requestTasksList = null;
			}

			//For Testing Only
			//if (OvertimeRequestTasksList != null)
			//{
			//    OvertimeRequestTasksList.Delete();
			//    OvertimeRequestTasksList = null;
			//}


			if (requestTasksList == null)
			{
				Guid listID = web.Lists.Add(CashAdvTasksListInternalName, "", SPListTemplateType.Tasks);
				requestTasksList = web.Lists[listID];
				requestTasksList.TitleResource.SetValueForUICulture(new CultureInfo(1036), "ISESMServiceRequestTasks");
				requestTasksList.TitleResource.SetValueForUICulture(new CultureInfo(1033), "ISESMServiceRequestTasks");
				requestTasksList.OnQuickLaunch = true;
				requestTasksList.NavigateForFormsPages = true;
				requestTasksList.Update();

				SPContentType ct = web.Site.RootWeb.ContentTypes[new SPContentTypeId("0x010801")];

				taskFormRelativeUrl = taskFormRelativeUrl.TrimStart('/');

				ct.EditFormUrl = taskFormRelativeUrl;
				ct.DisplayFormUrl = taskFormRelativeUrl;
				ct.Update();

				if (requestTasksList.ContentTypes["WorkFlow Task"] != null)
				{
					requestTasksList.ContentTypes["WorkFlow Task"].Delete();
				}
				requestTasksList.ContentTypes.Add(ct);

				requestTasksList.EventReceivers.Add(SPEventReceiverType.ItemDeleting, properties.Feature.Definition.ReceiverAssembly,
						  "IS_ESM_SERVICE_REQUESTS.TasksListEventHandler");

				if (!requestTasksList.HasUniqueRoleAssignments)
				{
					requestTasksList.BreakRoleInheritance(false);
				}
				AddGroupPermission(web, requestTasksList, SPRoleType.Reader, "All Portal Users");

				requestTasksList.Update();
			}
			return requestTasksList;
		}

		private SPList AddRequestWorkflowHistoryList(SPWeb web, SPFeatureReceiverProperties properties)
		{
			string SecurityRequestWorkflowHistoryListInternalName = "ISESMServiceRequestWorkflowHistory";
			SPList SecurityRequestWorkflowHistoryList = null;
			try
			{
				string listUrl = String.Format("{0}/lists/{1}", web.Url, SecurityRequestWorkflowHistoryListInternalName);
				SecurityRequestWorkflowHistoryList = web.GetList(listUrl);
			}
			catch (System.IO.FileNotFoundException ex)
			{
				SecurityRequestWorkflowHistoryList = null;
			}

			////For Testing Only
			//if (OvertimeRequestWorkflowHistoryList != null)
			//{
			//    OvertimeRequestWorkflowHistoryList.Delete();
			//    OvertimeRequestWorkflowHistoryList = null;
			//}

			if (SecurityRequestWorkflowHistoryList == null)
			{
				Guid listID = web.Lists.Add(SecurityRequestWorkflowHistoryListInternalName, "", SPListTemplateType.WorkflowHistory);
				SecurityRequestWorkflowHistoryList = web.Lists[listID];
				SecurityRequestWorkflowHistoryList.TitleResource.SetValueForUICulture(new CultureInfo(1036), "ISESMServiceRequestWorkflowHistory");
				SecurityRequestWorkflowHistoryList.TitleResource.SetValueForUICulture(new CultureInfo(1033), "ISESMServiceRequestWorkflowHistory");
				SecurityRequestWorkflowHistoryList.Hidden = true;
				SecurityRequestWorkflowHistoryList.Update();
			}
			return SecurityRequestWorkflowHistoryList;
		}

		private void AddGroupPermission(SPWeb web, SPList list, SPRoleType roleType, string groupName)
		{
			try
			{
				SPGroup departmentGroup = web.SiteGroups[groupName];
				if (departmentGroup != null)
				{
					SPPrincipal groupPrincipal = departmentGroup as SPPrincipal;
					SPRoleAssignment newRoleAssignmentToAdd = new SPRoleAssignment(groupPrincipal);
					SPRoleDefinition roleDefinition = web.RoleDefinitions.GetByType(roleType);
					newRoleAssignmentToAdd.RoleDefinitionBindings.Add(roleDefinition);
					list.RoleAssignments.Add(newRoleAssignmentToAdd);
				}
			}
			catch
			{
				//Group does not exist
			}
		}

		private void AddSiteGroup(string groupName, SPWeb web, SPMember owner, SPUser defaultUser, string description)
		{
			try
			{
				SPGroup tempGroup = web.SiteGroups[groupName];
			}
			catch
			{
				web.SiteGroups.Add(groupName, owner, defaultUser, description);
				SPGroup tempGroup = web.SiteGroups[groupName];
				tempGroup.OnlyAllowMembersViewMembership = true;
				tempGroup.Update();
			}
		}

		private void CreateSiteGroups(SPWeb web)
		{
			SPUser currentUser = web.CurrentUser;
			SPMember currentMember = (SPMember)currentUser;

			AddSiteGroup("ESM Team", web, currentMember, currentUser, String.Empty);
			//AddSiteGroup("Travels Head of Department", web, currentMember, currentUser, String.Empty);
			//AddSiteGroup("Travels HR Admin", web, currentMember, currentUser, String.Empty);
		}
		// Uncomment the method below to handle the event raised before a feature is deactivated.

		//public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
		//{
		//}


		// Uncomment the method below to handle the event raised after a feature has been installed.

		//public override void FeatureInstalled(SPFeatureReceiverProperties properties)
		//{
		//}


		// Uncomment the method below to handle the event raised before a feature is uninstalled.

		//public override void FeatureUninstalling(SPFeatureReceiverProperties properties)
		//{
		//}

		// Uncomment the method below to handle the event raised when a feature is upgrading.

		//public override void FeatureUpgrading(SPFeatureReceiverProperties properties, string upgradeActionName, System.Collections.Generic.IDictionary<string, string> parameters)
		//{
		//}
	}
}
