﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace IS_ESM_SERVICE_REQUESTS.Code
{
	class MessagePrompt
	{
		public static void ShowMessage(Page target, string message)
		{
			try
			{
				string alert = "<script language=\"javascript\">" + "alert('" + message + "');" + "<" + "/Script>";
				target.ClientScript.RegisterStartupScript(target.GetType(), "Notice", alert);
			}
			catch (Exception ex)
			{
				ULSLogHandler.LogError(ex);
			}

		}

		public static void ShowMessage(Page target, string message, string URL)
		{
			try
			{
				string alert = "<script type=\"text/javascript\"> " + "alert(\"" + message + "\"); " + "window.location = \"" + URL + "\"; " + "</script>";
				target.ClientScript.RegisterStartupScript(target.GetType(), "Notice", alert);
			}
			catch (Exception ex)
			{
				ULSLogHandler.LogError(ex);
			}

		}
	}
}
