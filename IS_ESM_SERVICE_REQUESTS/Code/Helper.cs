﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using System.Data;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Microsoft.SharePoint.WebControls;

namespace IS_ESM_SERVICE_REQUESTS.Code
{
	class Helper
	{
		public static SPUser GetSPUserFromFieldValue(string user, SPWeb web)
		{
			if (string.IsNullOrEmpty(user))
			{
				return null;
			}
			SPFieldUserValue userValue = new SPFieldUserValue(web, user);

			return userValue == null ? null : userValue.User;
		}
		public static void SetPeoplePickerValue(object userFieldValue, ClientPeoplePicker peControl, SPWeb web)
		{
			if (userFieldValue == null)
			{
				return;
			}

			SPUser CurrentUser = GetSPUserFromFieldValue(userFieldValue.ToString(), web);
			if (!string.IsNullOrEmpty(userFieldValue.ToString()))
			{
				peControl.ResolvedEntities.Clear();
				// PickerEntity object is used by People Picker Control
				List<PickerEntity> entries = new List<PickerEntity>();
				PickerEntity UserEntity = new PickerEntity();

				SPFieldUserValue userValue = new SPFieldUserValue(web, userFieldValue.ToString());

				UserEntity.Key = userValue.LookupValue.ToString();
				entries.Add(UserEntity);
				peControl.AddEntities(entries);
				peControl.Validate();
			}
		}
		internal static SPUser GetUserValue(PeopleEditor people, SPWeb web)
		{
			SPFieldUserValue result = null;
			if (people.ResolvedEntities.Count > 0)
			{
				PickerEntity entry = (PickerEntity)people.ResolvedEntities[0];

				if (entry != null)
				{
					SPUser user = web.EnsureUser(entry.Key);
					return user;

				}
			}
			return null;
		}

	}
}
