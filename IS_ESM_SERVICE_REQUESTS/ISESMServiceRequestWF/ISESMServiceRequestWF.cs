﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Drawing;
using System.Linq;
using System.Workflow.ComponentModel.Compiler;
using System.Workflow.ComponentModel.Serialization;
using System.Workflow.ComponentModel;
using System.Workflow.ComponentModel.Design;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Workflow.Activities.Rules;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Workflow;
using System.Collections.Generic;
using Microsoft.SharePoint.WorkflowActions;
using IS_ESM_SERVICE_REQUESTS.UtilityCode;
using Microsoft.Office.Workflow.Utility;
using System.IO;
using TaviaTech.SPExtensions;

namespace IS_ESM_SERVICE_REQUESTS.ISESMServiceRequestWF
{
    public sealed partial class ISESMServiceRequestWF : StateMachineWorkflowActivity
    {
        public ISESMServiceRequestWF()
        {
            InitializeComponent();
        }
        #region WorkflowVariables

        public SPWorkflowActivationProperties workflowProperties = new Microsoft.SharePoint.Workflow.SPWorkflowActivationProperties();

        //Requester Task
        public Int32 ctRequester_ListItemId = default(System.Int32);
        public System.Collections.Specialized.HybridDictionary ctRequester_SpecialPermissions = new System.Collections.Specialized.HybridDictionary();
        public Guid ctRequester_TaskId = default(System.Guid);
        public SPWorkflowTaskProperties ctRequester_TaskProperties = new Microsoft.SharePoint.Workflow.SPWorkflowTaskProperties();
        public String lthRequesterTaskCreated_HistoryDescription = default(System.String);
        public String lthRequesterTaskCreated_HistoryOutcome = default(System.String);
        public SPWorkflowTaskProperties otcRequester_AfterProperties = new Microsoft.SharePoint.Workflow.SPWorkflowTaskProperties();
        public String lthAfterRequesterAction_HistoryDescription = default(System.String);
        public String lthAfterRequesterAction_HistoryOutcome = default(System.String);
        public Int32 lthAfterRequesterAction_UserId = default(System.Int32);

        //Line Manager Task
        public Int32 ctLineManager_ListItemId = default(System.Int32);
        public System.Collections.Specialized.HybridDictionary ctLineManager_SpecialPermissions = new System.Collections.Specialized.HybridDictionary();
        public Guid ctLineManager_TaskId = default(System.Guid);
        public SPWorkflowTaskProperties ctLineManager_TaskProperties = new Microsoft.SharePoint.Workflow.SPWorkflowTaskProperties();
        public String lthLineManagerTaskCreated_HistoryDescription = default(System.String);
        public String lthLineManagerTaskCreated_HistoryOutcome = default(System.String);
        public SPWorkflowTaskProperties otcLineManager_AfterProperties = new Microsoft.SharePoint.Workflow.SPWorkflowTaskProperties();
        public String lthAfterLineManagerAction_HistoryDescription = default(System.String);
        public String lthAfterLineManagerAction_HistoryOutcome = default(System.String);
        public Int32 lthAfterLineManagerAction_UserId = default(System.Int32);

        //ESM Team Task
        public Int32 ctESMTeam_ListItemId = default(System.Int32);
        public System.Collections.Specialized.HybridDictionary ctESMTeam_SpecialPermissions = new System.Collections.Specialized.HybridDictionary();
        public Guid ctESMTeam_TaskId = default(System.Guid);
        public SPWorkflowTaskProperties ctESMTeam_TaskProperties = new Microsoft.SharePoint.Workflow.SPWorkflowTaskProperties();
        public String lthESMTeamTaskCreated_HistoryDescription = default(System.String);
        public String lthESMTeamTaskCreated_HistoryOutcome = default(System.String);
        public SPWorkflowTaskProperties otcESMTeam_AfterProperties = new Microsoft.SharePoint.Workflow.SPWorkflowTaskProperties();
        public String lthAfterESMTeamAction_HistoryDescription = default(System.String);
        public String lthAfterESMTeamAction_HistoryOutcome = default(System.String);
        public Int32 lthAfterESMTeamAction_UserId = default(System.Int32);

        //ESM Manager Task
        public Int32 ctESMManager_ListItemId = default(System.Int32);
        public System.Collections.Specialized.HybridDictionary ctESMManager_SpecialPermissions = new System.Collections.Specialized.HybridDictionary();
        public Guid ctESMManager_TaskId = default(System.Guid);
        public SPWorkflowTaskProperties ctESMManager_TaskProperties = new Microsoft.SharePoint.Workflow.SPWorkflowTaskProperties();
        public String lthESMManagerTaskCreated_HistoryDescription = default(System.String);
        public String lthESMManagerTaskCreated_HistoryOutcome = default(System.String);
        public SPWorkflowTaskProperties otcESMManager_AfterProperties = new Microsoft.SharePoint.Workflow.SPWorkflowTaskProperties();
        public String lthAfterESMManagerAction_HistoryDescription = default(System.String);
        public String lthAfterESMManagerAction_HistoryOutcome = default(System.String);
        public Int32 lthAfterESMManagerAction_UserId = default(System.Int32);

        #endregion WorkflowVariables

        #region GlobalVariables
        string CurrentWebURL;
        //string VacancyTitle = "";
        //string Department = "";
        List<SPUserRole> _rols;
        int RequestItemID = -1;
        Guid RequestsListId;
        string RequestsListTitle;
        string _LineManagerAction = "";
        string _ESMTeamAction = "";
        string _ESMManagerAction = "";
        string _RequesterAction = "";
        Contact _LineManager;
        Contact _ESMTeam;//pre-configured group
        Contact _ESMManager;
        Contact _Requester;
        string _Comments = "";
        ISESMServiceRequestWFRecjector _Rejector;//to know who return the request to Request
        string _DateRequired = "";
        string _RequestType = "";
        string _WorkflowTaskASPX = "/_layouts/15/EsmServiceRequest/ISESMServiceRequestWFTask.aspx?";
        string ESMTeamEmailAddress = "";
        #endregion GlobalVariables

        private void onWorkflowActivated_Invoked(object sender, ExternalDataEventArgs e)
        {
            try
            {
                ESMTeamEmailAddress = System.Web.Configuration.WebConfigurationManager.AppSettings["ISESMServiceRequest_ESMTeamEmail"].ToString();
                CurrentWebURL = workflowProperties.Web.Url;
                RequestItemID = workflowProperties.ItemId;
                RequestsListId = workflowProperties.ListId;
                RequestsListTitle = workflowProperties.List.Title;
                SPSecurity.RunWithElevatedPrivileges(ImpersonatedonWorkfrloActivated);
            }
            catch (Exception ex)
            {
                ULSLogHandler.LogError(ex);
                LogHelper.LogMessageToFile(ex);
            }
        }

        private void ImpersonatedonWorkfrloActivated()
        {
            SPSite _ImpersonatedSite = null;
            SPWeb _ImpersonatedWeb = null;
            SPList _RequestsList = null;
            SPListItem _RequestItem = null;
            try
            {
                _ImpersonatedSite = new SPSite(CurrentWebURL);
                _ImpersonatedWeb = _ImpersonatedSite.OpenWeb();
                _RequestsList = _ImpersonatedWeb.Lists[RequestsListId];
                _RequestItem = _RequestsList.GetItemById(RequestItemID);
                _ImpersonatedWeb.AllowUnsafeUpdates = true;
                _RequestItem["Title"] = "IS_ESM_Service_Request_" + _RequestItem.ID.ToString();
                _RequestItem.Update();
                _RequestType = _RequestItem["Request Type"].ToString();
                //_RejectComment = _RequestItem.ReadItemFieldString("ChangeRequesterComment").ToString(); //UnComment before making the Final WSP
                _DateRequired = ((DateTime)_RequestItem["Date Required"]).ToString("dd MMM yyyy");
                _ImpersonatedWeb.AllowUnsafeUpdates = false;
                SPUser hrbpValue = SPHandler.GetSPUserFromFieldValue(_RequestItem["Line Manager"].ToString(), _RequestItem.Web);
                _LineManager = Contact.FromName(hrbpValue.LoginName, _RequestItem.Web);
                SPUser _RequesterValue = SPHandler.GetSPUserFromFieldValue(_RequestItem["Author"].ToString(), _RequestItem.Web);
                _Requester = Contact.FromName(_RequesterValue.LoginName, _RequestItem.Web);
                _ESMTeam = Contact.FromPrincipalID(_ImpersonatedWeb.SiteGroups["ESM Team"].ID, _ImpersonatedWeb);
                //_ESMManager = Contact.FromName(_RequestItem["Line Manager"].ToString().Split('#')[1], _ImpersonatedWeb);

                //Asiwaju Edited permission
                _rols = new List<SPUserRole>();
                _rols.Add(new SPUserRole(_LineManager, SPRoleType.Contributor, false));
                _rols.Add(new SPUserRole(_ESMTeam, SPRoleType.Contributor, true));
                _rols.Add(new SPUserRole(_Requester, SPRoleType.Reader, false));
                WorkflowHandler.SetListItemSecurity(workflowProperties, _rols);
            }
            catch (Exception ex)
            {
                ErrorHandler.LogToWindows(ex, "ISESMServiceRequestWF, ImpersonatedonWorkfrloActivated");
                ULSLogHandler.LogError(ex);
                LogHelper.LogMessageToFile(ex);
            }
            finally
            {
                if (_ImpersonatedWeb != null)
                    _ImpersonatedWeb.Dispose();
                if (_ImpersonatedSite != null)
                    _ImpersonatedSite.Dispose();
            }
        }
        #region Line Manager

        private void ctLineManager_MethodInvoking(object sender, EventArgs e)
        {
            try
            {
                ctLineManager_TaskId = Guid.NewGuid();
                ctLineManager_TaskProperties.Title = "Line Manager Task - " + DateTime.Now.ToString();
                ctLineManager_TaskProperties.AssignedTo = _LineManager.LoginName;

                ctLineManager_TaskProperties.ExtendedProperties["DisplayForm"] = "LineManager";
                ctLineManager_TaskProperties.ExtendedProperties["ItemID"] = RequestItemID;

                ctLineManager_TaskProperties.ExtendedProperties["ListTitle"] = RequestsListTitle;
                ctLineManager_TaskProperties.ExtendedProperties["Comments"] = _Comments;
                ///////////////////
                lthLineManagerTaskCreated_HistoryDescription = "Line Manager Task Created";
                lthLineManagerTaskCreated_HistoryOutcome = "The system awaits for Line Manager Approval";

                //Task assign changed by Asiwaju
                //ctLineManager_SpecialPermissions.Add(SPRoleType.Contributor, _LineManager.LoginName);
                WorkflowHandler.ChangeTaskItemSecurity(ctLineManager_SpecialPermissions, ctLineManager_TaskProperties);
            }
            catch (Exception ex)
            {
                ULSLogHandler.LogError(ex);
                LogHelper.LogMessageToFile(ex);
            }
        }

        private void caSendLineManagerTaskCreatedMail_ExecuteCode(object sender, EventArgs e)
        {
            try
            {
                //SendEmail
                List<string> To = new List<string>();
                List<string> Cc = new List<string>();

                if (!String.IsNullOrEmpty(_LineManager.EmailAddress))
                    To.Add(_LineManager.EmailAddress);

                //if (!String.IsNullOrEmpty(_BusinessPartner.EmailAddress))
                //    Cc.Add(_BusinessPartner.EmailAddress);

                string Body = "Dear " + _LineManager.DisplayName + ",<br><br>";
                //A new requisition is awaiting your kind approval
                //if (_Rejector == 0)
                Body += "An IS ESM Service request has been forwarded for your approval.<br>";
                Body += "Requested by " + _Requester.DisplayName + ".";
                //else
                //Body += "Requisition from " + _Requester.DisplayName + " is updated & currently awaiting your kind approval.";
                Body += "<br>Request Type: " + _RequestType;
                Body += "<br>Date Required: " + _DateRequired;
                Body += "<br><hr>For more details please " + WorkflowHandler.getTaskUrl(ctLineManager.ListItemId.ToString(), workflowProperties, _WorkflowTaskASPX) + " To Approve/Disapprove his request.";
                Body += "<br><br>Kind Regards,<br>";
                Body += "IS ESM Services";
                Body += "<br><br>NOTE: This email message is auto generated, Please do not reply to the email address.";
                EMailHandler.SendEmail(To, "workflows@mtn.com", Cc, "IS ESM Service Request by " + _Requester.DisplayName, Body);
            }
            catch (Exception ex)
            {
                ULSLogHandler.LogError(ex);
                LogHelper.LogMessageToFile(ex);
            }

        }

        private void otcLineManager_Invoked(object sender, ExternalDataEventArgs e)
        {
            string _RejectComment = workflowProperties.List.GetItemByIdSelectedFields(workflowProperties.ItemId, "ChangeRequesterComment").
                ReadItemFieldString("ChangeRequesterComment");

            _LineManagerAction = otcLineManager_AfterProperties.ExtendedProperties["Action"].ToString();
            _Comments = otcLineManager_AfterProperties.ExtendedProperties["Comments"].ToString();
            if (otcLineManager_AfterProperties.ExtendedProperties["CurrentUserId"] != null)
                lthAfterLineManagerAction_UserId = int.Parse(otcLineManager_AfterProperties.ExtendedProperties["CurrentUserId"].ToString());

            //Approve or Reject Mail
            //SendEmail
            List<string> To = new List<string>();
            List<string> Cc = new List<string>();

            if (!String.IsNullOrEmpty(_Requester.EmailAddress))
                To.Add(_Requester.EmailAddress);

            string Body = "Dear " + _Requester.DisplayName + ",<br><br>";
            if (!String.IsNullOrEmpty(_LineManager.EmailAddress))
                Cc.Add(_LineManager.EmailAddress);

            //Body += "The Line Manager (" + _LineManager.DisplayName + ") ";

            if (_LineManagerAction == "Approved")
            {
                lthAfterLineManagerAction_HistoryDescription = "Line Manager Approved Requisition";
                lthAfterLineManagerAction_HistoryOutcome = "Line Manager Approved The requisition.";
                Body += "Your request has been approved by your Line Manager (" + _LineManager.DisplayName + ") and has been forwarded to ESM Team for approval.";
            }
            else if (_LineManagerAction == "Rejected")//Recjected
            {
                lthAfterLineManagerAction_HistoryDescription = "Line Manager Rejected Requisition";
                lthAfterLineManagerAction_HistoryOutcome = "Line Manager Rejected The Requisition.";
                Body += "Your request has been rejected by your Line Manager (" + _LineManager.DisplayName + ").<br>Comment: " + _RejectComment + "<br>";
            }
            else //Request Change
            {
                lthAfterLineManagerAction_HistoryDescription = "Line Manager Request Change";
                lthAfterLineManagerAction_HistoryOutcome = "Line Manager Request Change From the Requester.";
            }
            Body += "<br>Request Type: " + _RequestType;
            Body += "<br>Date Required: " + _DateRequired;
            Body += "<br><br>Kind Regards,<br>";
            Body += "IS ESM Services";
            Body += "<br><br>NOTE: This email message is auto generated, Please do not reply to the email address.";
            if (_LineManagerAction == "Approved")
                EMailHandler.SendEmail(To, "workflows@mtn.com", Cc, "IS ESM Service Request Approved by Line Manager", Body);
            else if (_LineManagerAction == "Rejected")//Recjected
                EMailHandler.SendEmail(To, "workflows@mtn.com", Cc, "IS ESM Service Request Rejected by Line Manager", Body);
        }

        private void LineManagerAction(object sender, ConditionalEventArgs e)
        {
            if (_LineManagerAction == "Approved")
            {
                e.Result = true;//Assign Task to the ESM Team
                                //_Rejector = 0;
                                //_HRRecruitAdminRejector = 0;
            }
            else //i.e Did Not Approve
            {
                e.Result = false;
            }
        }

        private void LineManagerDidNotApproveAction(object sender, ConditionalEventArgs e)
        {
            if (_LineManagerAction == "Rejected")
            {
                e.Result = false;//Workflow Ends - Cycle Completed & Requisition Rejected
            }
            else
            {
                e.Result = true;//Assign Task to the Requester
                _Rejector = ISESMServiceRequestWFRecjector.LineManager;
            }
        }

        #endregion Line Manager

        #region ESM Team

        private void ctESMTeam_MethodInvoking(object sender, EventArgs e)
        {
            ctESMTeam_TaskId = Guid.NewGuid();
            ctESMTeam_TaskProperties.Title = "ESM Team Task - " + DateTime.Now.ToString();

            SPFieldUserValue _ESMTeamFieldUserValue;
            SPGroup ESMGroup = workflowProperties.Web.SiteGroups["ESM Team"];
            _ESMTeamFieldUserValue = new SPFieldUserValue(workflowProperties.Web, ESMGroup.ID, ESMGroup.Name);
            ctESMTeam_TaskProperties.AssignedTo = _ESMTeamFieldUserValue.LookupValue;
            ctESMTeam_TaskProperties.ExtendedProperties["DisplayForm"] = "ESMTeam";
            ctESMTeam_TaskProperties.ExtendedProperties["ItemID"] = workflowProperties.ItemId;
            ctESMTeam_TaskProperties.ExtendedProperties["ListTitle"] = workflowProperties.List.Title;
            ctESMTeam_TaskProperties.ExtendedProperties["Comments"] = _Comments;

            ///////////////////
            lthESMTeamTaskCreated_HistoryDescription = "ESM Team Task Created";
            lthESMTeamTaskCreated_HistoryOutcome = "The system awaits for ESM Team Approval";

            WorkflowHandler.ChangeTaskItemSecurity(ctESMTeam_SpecialPermissions, ctESMTeam_TaskProperties);
        }

        private void caSendESMTeamTaskCreatedMail_ExecuteCode(object sender, EventArgs e)
        {
            //SendEmail
            List<string> To = new List<string>();
            List<string> Cc = new List<string>();

            if (!String.IsNullOrEmpty(ESMTeamEmailAddress))
                To.Add(ESMTeamEmailAddress);

            //if (!String.IsNullOrEmpty(_BusinessPartner.EmailAddress))
            //    Cc.Add(_BusinessPartner.EmailAddress);

            string Body = "Dear " + "ESM Team Member" + ",<br><br>";
            //A new requisition is awaiting your kind approval
            //if (_Rejector == 0)
            Body += "An IS ESM Service request has been forwarded for your approval.<br>";
            Body += "Requested by " + _Requester.DisplayName + ".";
            //else
            //    Body += "Requisition from " + _Requester.DisplayName + " is updated & currently awaiting your kind approval.";
            Body += "<br>Request Type: " + _RequestType;
            Body += "<br>Date Required: " + _DateRequired;
            Body += "<br><hr>For more details please " + WorkflowHandler.getTaskUrl(ctESMTeam.ListItemId.ToString(), workflowProperties, _WorkflowTaskASPX) + " To Approve/Disapprove his request.";
            Body += "<br><br>Kind Regards,<br>";
            Body += "IS ESM Services";
            Body += "<br><br>NOTE: This email message is auto generated, Please do not reply to the email address.";
            EMailHandler.SendEmail(To, "workflows@mtn.com", Cc, "IS ESM Service Request by " + _Requester.DisplayName, Body);
        }

        private void otcESMTeam_Invoked(object sender, ExternalDataEventArgs e)
        {
            string _RejectComment = workflowProperties.List.GetItemByIdSelectedFields(workflowProperties.ItemId, "ChangeRequesterComment").
                ReadItemFieldString("ChangeRequesterComment");

            _ESMTeamAction = otcESMTeam_AfterProperties.ExtendedProperties["Action"].ToString();
            _Comments = otcESMTeam_AfterProperties.ExtendedProperties["Comments"].ToString();
            if (otcESMTeam_AfterProperties.ExtendedProperties["CurrentUserId"] != null)
                lthAfterESMTeamAction_UserId = int.Parse(otcESMTeam_AfterProperties.ExtendedProperties["CurrentUserId"].ToString());

            //Approve or Reject Mail
            //SendEmail
            List<string> To = new List<string>();
            List<string> Cc = new List<string>();

            if (!String.IsNullOrEmpty(_Requester.EmailAddress))
                To.Add(_Requester.EmailAddress);

            string Body = "Dear " + _Requester.DisplayName + ",<br><br>";

            if (!String.IsNullOrEmpty(ESMTeamEmailAddress))
                Cc.Add(ESMTeamEmailAddress);

            if (_ESMTeamAction == "Approved")
            {
                lthAfterESMTeamAction_HistoryDescription = "ESM Team Approved Requisition";
                lthAfterESMTeamAction_HistoryOutcome = "ESM Team Approved The requisition.";
                Body += "Your request has been approved by ESM Team and has been forwarded to ESM Manager for approval.";

                SPSecurity.RunWithElevatedPrivileges(delegate ()
                {
                    SPSite _site = new SPSite(CurrentWebURL);
                    SPWeb _web = _site.OpenWeb();
                    _ESMManager = Contact.FromName(otcESMTeam_AfterProperties.ExtendedProperties["ESMManagerLoginName"].ToString(), _web);
                    List<SPUserRole> _ESMManagerRol = new List<SPUserRole>();
                    _ESMManagerRol.Add(new SPUserRole(_ESMManager, SPRoleType.Reader, false));
                    WorkflowHandler.SetListItemSecurity(workflowProperties, _ESMManagerRol);
                    _web.Dispose();
                    _site.Dispose();
                });

            }
            else if (_ESMTeamAction == "Rejected")//Recjected
            {
                lthAfterESMTeamAction_HistoryDescription = "ESM Team Rejected Requisition";
                lthAfterESMTeamAction_HistoryOutcome = "ESM Team Rejected The Requisition.";
                Body += "Your request has been rejected by ESM Team. <br>Comment: " + _RejectComment + "<br>";
            }
            else //Request Change
            {
                lthAfterESMTeamAction_HistoryDescription = "ESM Team Request Change";
                lthAfterESMTeamAction_HistoryOutcome = "ESM Team Request Change From the Requester.";
            }

            Body += "<br>Request Type: " + _RequestType;
            Body += "<br>Date Required: " + _DateRequired;
            Body += "<br>Kindest Regards,";
            Body += "<br>IS ESM Service";
            if (_ESMTeamAction == "Approved")
                EMailHandler.SendEmail(To, "workflows@mtn.com", Cc, "IS ESM Service Request Approved by ESM Team", Body);
            else if (_ESMTeamAction == "Rejected")//Recjected
                EMailHandler.SendEmail(To, "workflows@mtn.com", Cc, "IS ESM Service Request Rejected by ESM Team", Body);
        }

        private void ESMTeamAction(object sender, ConditionalEventArgs e)
        {
            if (_ESMTeamAction == "Approved")
            {
                e.Result = true;//Assign Task to the ESM Team
                                //_Rejector = 0;
                                //_HRRecruitAdminRejector = 0;
            }
            else //i.e Did Not Approve
            {
                e.Result = false;
            }
        }

        private void ESMTeamDidNotApproveAction(object sender, ConditionalEventArgs e)
        {
            if (_ESMTeamAction == "Rejected")
            {
                e.Result = false;//Workflow Ends - Cycle Completed & Requisition Rejected
            }
            else
            {
                e.Result = true;//Assign Task to the Requester
                _Rejector = ISESMServiceRequestWFRecjector.ESMTeam;
            }
        }
        #endregion ESM Team

        #region ESM Manager

        private void ctESMManager_MethodInvoking(object sender, EventArgs e)
        {
            ctESMManager_TaskId = Guid.NewGuid();
            ctESMManager_TaskProperties.Title = "ESM Manager Task - " + DateTime.Now.ToString();
            ctESMManager_TaskProperties.AssignedTo = _ESMManager.LoginName;
            ctESMManager_TaskProperties.ExtendedProperties["DisplayForm"] = "ESMManager";
            ctESMManager_TaskProperties.ExtendedProperties["ItemID"] = workflowProperties.ItemId;
            ctESMManager_TaskProperties.ExtendedProperties["ListTitle"] = workflowProperties.List.Title;
            ctESMManager_TaskProperties.ExtendedProperties["Comments"] = _Comments;

            ///////////////////
            lthESMManagerTaskCreated_HistoryDescription = "ESM Manager Task Created";
            lthESMManagerTaskCreated_HistoryOutcome = "The system awaits for ESM Manager Approval";

            WorkflowHandler.ChangeTaskItemSecurity(ctESMManager_SpecialPermissions, ctESMManager_TaskProperties);
        }

        private void caSendESMManagerTaskCreatedMail_ExecuteCode(object sender, EventArgs e)
        {
            //SendEmail
            List<string> To = new List<string>();
            List<string> Cc = new List<string>();

            if (!String.IsNullOrEmpty(_ESMManager.EmailAddress))
                To.Add(_ESMManager.EmailAddress);

            //if (!String.IsNullOrEmpty(_BusinessPartner.EmailAddress))
            //    Cc.Add(_BusinessPartner.EmailAddress);

            string Body = "Dear " + _ESMManager.DisplayName + ",<br><br>";
            //A new requisition is awaiting your kind approval
            //if (_Rejector == 0)
            Body += "An IS ESM Service request has been forwarded for your approval.<br>";
            Body += "Requested by " + _Requester.DisplayName + ".";
            //else
            //    Body += "Requisition from " + _Requester.DisplayName + " is updated & currently awaiting your kind approval.";
            Body += "<br>Request Type: " + _RequestType;
            Body += "<br>Date Required: " + _DateRequired;
            Body += "<br><hr>For more details please " + WorkflowHandler.getTaskUrl(ctESMManager.ListItemId.ToString(), workflowProperties, _WorkflowTaskASPX) + " To Approve/Disapprove his request.";
            Body += "<br><br>Kind Regards,<br>";
            Body += "IS ESM Services";
            Body += "<br><br>NOTE: This email message is auto generated, Please do not reply to the email address.";
            EMailHandler.SendEmail(To, "workflows@mtn.com", Cc, "IS ESM Service Request by " + _Requester.DisplayName, Body);
        }

        private void otcESMManager_Invoked(object sender, ExternalDataEventArgs e)
        {
            _ESMManagerAction = otcESMManager_AfterProperties.ExtendedProperties["Action"].ToString();
            _Comments = otcESMManager_AfterProperties.ExtendedProperties["Comments"].ToString();
            if (otcESMManager_AfterProperties.ExtendedProperties["CurrentUserId"] != null)
                lthAfterESMManagerAction_UserId = int.Parse(otcESMManager_AfterProperties.ExtendedProperties["CurrentUserId"].ToString());

            string _RejectComment = workflowProperties.List.GetItemByIdSelectedFields(workflowProperties.ItemId, "ChangeRequesterComment")
                .ReadItemFieldString("ChangeRequesterComment");

            //Approve or Reject Mail
            //SendEmail
            List<string> To = new List<string>();
            List<string> Cc = new List<string>();

            if (!String.IsNullOrEmpty(_Requester.EmailAddress))
                To.Add(_Requester.EmailAddress);

            string Body = "Dear " + _Requester.DisplayName + ",<br><br>";
            if (!String.IsNullOrEmpty(_ESMManager.EmailAddress))
                Cc.Add(_ESMManager.EmailAddress);

            if (_ESMManagerAction == "Approved")
            {
                lthAfterESMManagerAction_HistoryDescription = "ESM Manager Approved Requisition";
                lthAfterESMManagerAction_HistoryOutcome = "ESM Manager Approved The requisition.";
                Body += "Your request has been approved by ESM Manager (" + _ESMManager.DisplayName + ").";
            }
            else if (_ESMManagerAction == "Rejected")//Recjected
            {
                lthAfterESMManagerAction_HistoryDescription = "ESM Manager Rejected Requisition";
                lthAfterESMManagerAction_HistoryOutcome = "ESM Manager Rejected The Requisition.";

                Body += "Your request has been rejected by ESM Manager (" + _ESMManager.DisplayName + ").<br>Comment: " + _RejectComment + "<br>";
            }
            else //Request Change
            {
                lthAfterESMManagerAction_HistoryDescription = "ESM Manager Request Change";
                lthAfterESMManagerAction_HistoryOutcome = "ESM Manager Request Change From the Requester.";
            }

            Body += "<br>Request Type: " + _RequestType;
            Body += "<br>Date Required: " + _DateRequired;
            Body += "<br><br>Kind Regards,<br>";
            Body += "IS ESM Services";
            Body += "<br><br>NOTE: This email message is auto generated, Please do not reply to the email address.";
            if (_ESMManagerAction == "Approved")
                EMailHandler.SendEmail(To, "workflows@mtn.com", Cc, "IS ESM Service Request Approved by ESM Manager", Body);
            else if (_ESMManagerAction == "Rejected")//Recjected
                EMailHandler.SendEmail(To, "workflows@mtn.com", Cc, "IS ESM Service Request Rejected by ESM Manager", Body);
        }

        private void ESMManagerAction(object sender, ConditionalEventArgs e)
        {
            if (_ESMManagerAction == "Approved")
            {
                e.Result = true;//Workflow Ends - Cycle Completed & Requisition Approved
                                //_Rejector = 0;
                                //_HRRecruitAdminRejector = 0;
            }
            else //i.e Did Not Approve
            {
                e.Result = false;
            }
        }

        private void ESMManagerDidNotApproveAction(object sender, ConditionalEventArgs e)
        {
            if (_ESMManagerAction == "Rejected")
            {
                e.Result = false;//Workflow Ends - Cycle Completed & Requisition Rejected
            }
            else
            {
                e.Result = true;//Assign Task to the Requester
                _Rejector = ISESMServiceRequestWFRecjector.ESMManager;
            }
        }

        #endregion ESM Manager

        #region Requester

        private void ctRequester_MethodInvoking(object sender, EventArgs e)
        {
            ctRequester_TaskId = Guid.NewGuid();
            ctRequester_TaskProperties.Title = "Requester Task - " + DateTime.Now.ToString();
            ctRequester_TaskProperties.AssignedTo = _Requester.LoginName;
            ctRequester_TaskProperties.ExtendedProperties["DisplayForm"] = "Requester";
            ctRequester_TaskProperties.ExtendedProperties["ItemID"] = RequestItemID;
            ctRequester_TaskProperties.ExtendedProperties["ListTitle"] = RequestsListTitle;
            ctRequester_TaskProperties.ExtendedProperties["Comments"] = _Comments;
            ctRequester_TaskProperties.ExtendedProperties["Rejector"] = _Rejector;
            ///////////////////
            lthRequesterTaskCreated_HistoryDescription = "Requester Task Created";
            lthRequesterTaskCreated_HistoryOutcome = "The system awaits for Requester Update";

            WorkflowHandler.ChangeTaskItemSecurity(ctRequester_SpecialPermissions, ctRequester_TaskProperties);
        }

        private void caSendRequesterTaskCreatedMail_ExecuteCode(object sender, EventArgs e)
        {
            //SendEmail
            List<string> To = new List<string>();
            List<string> Cc = new List<string>();

            if (!String.IsNullOrEmpty(_Requester.EmailAddress))
                To.Add(_Requester.EmailAddress);

            string Body = "Dear " + _Requester.DisplayName + ",<br><br>";
            Body += "Your IS ESM Service request has been forwarded to you from ";
            //if (_Rejector == 0)
            if (_Rejector == ISESMServiceRequestWFRecjector.LineManager)
            {
                if (!String.IsNullOrEmpty(_LineManager.EmailAddress))
                    Cc.Add(_LineManager.EmailAddress);
                Body += "the Line Manager (" + _LineManager.DisplayName + ")";
            }
            else if (_Rejector == ISESMServiceRequestWFRecjector.ESMTeam)
            {
                if (!String.IsNullOrEmpty(ESMTeamEmailAddress))
                    Cc.Add(ESMTeamEmailAddress);
                Body += "the ESM Team";
            }
            else if (_Rejector == ISESMServiceRequestWFRecjector.ESMManager)
            {
                if (!String.IsNullOrEmpty(_ESMManager.EmailAddress))
                    Cc.Add(_ESMManager.EmailAddress);
                Body += "the ESM Manager (" + _ESMManager.DisplayName + ")";
            }
            Body += " for update.";
            Body += "<br>Request Type: " + _RequestType;
            Body += "<br>Date Required: " + _DateRequired;
            Body += "<br><hr>For more details please " + WorkflowHandler.getTaskUrl(ctRequester.ListItemId.ToString(), workflowProperties, _WorkflowTaskASPX) + " To Update/Cancel your request.";
            Body += "<br><br>Kind Regards,<br>";
            Body += "IS ESM Services";
            Body += "<br><br>NOTE: This email message is auto generated, Please do not reply to the email address.";

            EMailHandler.SendEmail(To, "workflows@mtn.com", Cc, "IS ESM Service Request by " + _Requester.DisplayName, Body);
        }

        private void otcRequester_Invoked(object sender, ExternalDataEventArgs e)
        {
            _RequesterAction = otcRequester_AfterProperties.ExtendedProperties["Action"].ToString();
            _Comments = otcRequester_AfterProperties.ExtendedProperties["Comments"].ToString();
            if (otcRequester_AfterProperties.ExtendedProperties["CurrentUserId"] != null)
                lthAfterRequesterAction_UserId = int.Parse(otcRequester_AfterProperties.ExtendedProperties["CurrentUserId"].ToString());
            if (_RequesterAction == "Approved")
            {
                lthAfterRequesterAction_HistoryDescription = "Requester Updated Requisition";
                lthAfterRequesterAction_HistoryOutcome = "Requester Updated The requisition.";
            }
            else// if (_RequesterAction == "Rejected")//Recjected i.e he cancelled the request
            {
                lthAfterRequesterAction_HistoryDescription = "Requester Canceled Requisition";
                lthAfterRequesterAction_HistoryOutcome = "Requester Canceled The Requisition.";
            }
        }

        private void RequesterAction(object sender, ConditionalEventArgs e)
        {
            if (_RequesterAction == "Approved")
            {
                e.Result = true;//Assign Task to the Line Manager
            }
            else //i.e Canceled Request
            {
                e.Result = false;
            }
        }

        #endregion Requester

        private void ssESMManagerRejected_MethodInvoking(object sender, EventArgs e)
        {

        }

        private void ssPendingLineManagerApproval_MethodInvoking(object sender, EventArgs e)
        {

        }

        private void ssPendingRequesterUpdate_MethodInvoking(object sender, EventArgs e)
        {

        }

        private void ssPendingESMTeamApproval_MethodInvoking(object sender, EventArgs e)
        {

        }

        private void ssPendingESMManagerApproval_MethodInvoking(object sender, EventArgs e)
        {

        }

        private void dtLineManager_MethodInvoking(object sender, EventArgs e)
        {

        }
    }
    public enum ISESMServiceRequestWFRecjector
	{
		LineManager = 0, ESMTeam = 1, ESMManager = 2
	}
}
