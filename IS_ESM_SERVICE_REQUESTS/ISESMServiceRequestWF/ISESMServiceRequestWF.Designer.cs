﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Drawing;
using System.Reflection;
using System.Workflow.ComponentModel.Compiler;
using System.Workflow.ComponentModel.Serialization;
using System.Workflow.ComponentModel;
using System.Workflow.ComponentModel.Design;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Workflow.Activities.Rules;

namespace IS_ESM_SERVICE_REQUESTS.ISESMServiceRequestWF
{
    public sealed partial class ISESMServiceRequestWF
    {
        #region Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCode]
        private void InitializeComponent()
        {
            this.CanModifyActivities = true;
            System.Workflow.Runtime.CorrelationToken correlationtoken1 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.Runtime.CorrelationToken correlationtoken2 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.Runtime.CorrelationToken correlationtoken3 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.Activities.CodeCondition codecondition1 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.CodeCondition codecondition2 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.CodeCondition codecondition3 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Runtime.CorrelationToken correlationtoken4 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.Runtime.CorrelationToken correlationtoken5 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.Activities.CodeCondition codecondition4 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.CodeCondition codecondition5 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.CodeCondition codecondition6 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.CodeCondition codecondition7 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Runtime.CorrelationToken correlationtoken6 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind1 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind2 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind3 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind4 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind5 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken7 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind6 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind7 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind8 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken8 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.Runtime.CorrelationToken correlationtoken9 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind9 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind10 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind11 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind12 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken10 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind13 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind14 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind15 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind16 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind17 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken11 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind18 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind19 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind20 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken12 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.Runtime.CorrelationToken correlationtoken13 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind21 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind22 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind23 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind24 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken14 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind25 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind26 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind27 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind28 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind29 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken15 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind30 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind31 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind32 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken16 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.Runtime.CorrelationToken correlationtoken17 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind33 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind34 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind35 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind36 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken18 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind37 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind38 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind39 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind40 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind41 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken19 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind42 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind43 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind44 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken20 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.Runtime.CorrelationToken correlationtoken21 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind45 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind46 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind47 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind48 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken22 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind49 = new System.Workflow.ComponentModel.ActivityBind();
            this.ssaESMManagerRejected = new System.Workflow.Activities.SetStateActivity();
            this.ssESMManagerRejected = new Microsoft.SharePoint.WorkflowActions.SetState();
            this.ssaESMToRequester = new System.Workflow.Activities.SetStateActivity();
            this.ssaESMTeamRejected = new System.Workflow.Activities.SetStateActivity();
            this.ssESMTeamRejected = new Microsoft.SharePoint.WorkflowActions.SetState();
            this.ssaESMTeamToRequester = new System.Workflow.Activities.SetStateActivity();
            this.ssaLineManagerRejected = new System.Workflow.Activities.SetStateActivity();
            this.ssLineManagerRejected = new Microsoft.SharePoint.WorkflowActions.SetState();
            this.ssaLineManagerToRequester = new System.Workflow.Activities.SetStateActivity();
            this.ESMManagerRejected = new System.Workflow.Activities.IfElseBranchActivity();
            this.ESMManagerRequestChange = new System.Workflow.Activities.IfElseBranchActivity();
            this.ESMTeamRejected = new System.Workflow.Activities.IfElseBranchActivity();
            this.ESMTeamRequestChange = new System.Workflow.Activities.IfElseBranchActivity();
            this.LineManagerRejected = new System.Workflow.Activities.IfElseBranchActivity();
            this.LineManagerRequestChange = new System.Workflow.Activities.IfElseBranchActivity();
            this.ssaRequesterToCompleteState = new System.Workflow.Activities.SetStateActivity();
            this.ssRequisitionCanceled = new Microsoft.SharePoint.WorkflowActions.SetState();
            this.ssaRequesterToLineManager = new System.Workflow.Activities.SetStateActivity();
            this.ifElseActivity6 = new System.Workflow.Activities.IfElseActivity();
            this.ssaESMManagerToCompleteState = new System.Workflow.Activities.SetStateActivity();
            this.ssRequisitionApproved = new Microsoft.SharePoint.WorkflowActions.SetState();
            this.ifElseActivity7 = new System.Workflow.Activities.IfElseActivity();
            this.ssaESMTeamToESMManager = new System.Workflow.Activities.SetStateActivity();
            this.ifElseActivity5 = new System.Workflow.Activities.IfElseActivity();
            this.ssaLineManagerToESMTeam = new System.Workflow.Activities.SetStateActivity();
            this.ifElseBranchActivity2 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity1 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ESMManagerDidNotApprove = new System.Workflow.Activities.IfElseBranchActivity();
            this.ESMManagerApproved = new System.Workflow.Activities.IfElseBranchActivity();
            this.ESMTeamDidNotApprove = new System.Workflow.Activities.IfElseBranchActivity();
            this.ESMTeamApproved = new System.Workflow.Activities.IfElseBranchActivity();
            this.LineManagerDidNotApprove = new System.Workflow.Activities.IfElseBranchActivity();
            this.LineManagerApproved = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseActivity1 = new System.Workflow.Activities.IfElseActivity();
            this.dtRequester = new Microsoft.SharePoint.WorkflowActions.DeleteTask();
            this.lthAfterRequesterAction = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.otcRequester = new Microsoft.SharePoint.WorkflowActions.OnTaskChanged();
            this.caSendRequesterTaskCreatedMail = new System.Workflow.Activities.CodeActivity();
            this.lthRequesterTaskCreated = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.ssPendingRequesterUpdate = new Microsoft.SharePoint.WorkflowActions.SetState();
            this.ctRequester = new Microsoft.SharePoint.WorkflowActions.CreateTask();
            this.ifElseActivity4 = new System.Workflow.Activities.IfElseActivity();
            this.dtESMManager = new Microsoft.SharePoint.WorkflowActions.DeleteTask();
            this.lthAfterESMManagerAction = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.otcESMManager = new Microsoft.SharePoint.WorkflowActions.OnTaskChanged();
            this.caSendESMManagerTaskCreatedMail = new System.Workflow.Activities.CodeActivity();
            this.lthESMManagerTaskCreated = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.ssPendingESMManagerApproval = new Microsoft.SharePoint.WorkflowActions.SetState();
            this.ctESMManager = new Microsoft.SharePoint.WorkflowActions.CreateTask();
            this.ifElseActivity3 = new System.Workflow.Activities.IfElseActivity();
            this.dtESMTeam = new Microsoft.SharePoint.WorkflowActions.DeleteTask();
            this.lthAfterESMTeamAction = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.otcESMTeam = new Microsoft.SharePoint.WorkflowActions.OnTaskChanged();
            this.caSendESMTeamTaskCreatedMail = new System.Workflow.Activities.CodeActivity();
            this.lthESMTeamTaskCreated = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.ssPendingESMTeamApproval = new Microsoft.SharePoint.WorkflowActions.SetState();
            this.ctESMTeam = new Microsoft.SharePoint.WorkflowActions.CreateTask();
            this.ifElseActivity2 = new System.Workflow.Activities.IfElseActivity();
            this.dtLineManager = new Microsoft.SharePoint.WorkflowActions.DeleteTask();
            this.lthAfterLineManagerAction = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.otcLineManager = new Microsoft.SharePoint.WorkflowActions.OnTaskChanged();
            this.caSendLineManagerTaskCreatedMail = new System.Workflow.Activities.CodeActivity();
            this.lthLineManagerTaskCreated = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.ssPendingLineManagerApproval = new Microsoft.SharePoint.WorkflowActions.SetState();
            this.ctLineManager = new Microsoft.SharePoint.WorkflowActions.CreateTask();
            this.ssaToLineManager = new System.Workflow.Activities.SetStateActivity();
            this.onWorkflowActivated = new Microsoft.SharePoint.WorkflowActions.OnWorkflowActivated();
            this.edaRequester = new System.Workflow.Activities.EventDrivenActivity();
            this.siaRequester = new System.Workflow.Activities.StateInitializationActivity();
            this.edaESMManager = new System.Workflow.Activities.EventDrivenActivity();
            this.siaESMManager = new System.Workflow.Activities.StateInitializationActivity();
            this.edaESMTeam = new System.Workflow.Activities.EventDrivenActivity();
            this.siaESMTeam = new System.Workflow.Activities.StateInitializationActivity();
            this.edaLineManager = new System.Workflow.Activities.EventDrivenActivity();
            this.siaLinemanager = new System.Workflow.Activities.StateInitializationActivity();
            this.eventDrivenActivity1 = new System.Workflow.Activities.EventDrivenActivity();
            this.saCompleteState = new System.Workflow.Activities.StateActivity();
            this.saRequester = new System.Workflow.Activities.StateActivity();
            this.saESMManager = new System.Workflow.Activities.StateActivity();
            this.saESMTeam = new System.Workflow.Activities.StateActivity();
            this.saLineManager = new System.Workflow.Activities.StateActivity();
            this.ISESMServiceRequestWFInitialState = new System.Workflow.Activities.StateActivity();
            // 
            // ssaESMManagerRejected
            // 
            this.ssaESMManagerRejected.Name = "ssaESMManagerRejected";
            this.ssaESMManagerRejected.TargetStateName = "saCompleteState";
            // 
            // ssESMManagerRejected
            // 
            correlationtoken1.Name = "workflowToken";
            correlationtoken1.OwnerActivityName = "ISESMServiceRequestWF";
            this.ssESMManagerRejected.CorrelationToken = correlationtoken1;
            this.ssESMManagerRejected.Name = "ssESMManagerRejected";
            this.ssESMManagerRejected.State = 20;
            // 
            // ssaESMToRequester
            // 
            this.ssaESMToRequester.Name = "ssaESMToRequester";
            this.ssaESMToRequester.TargetStateName = "saRequester";
            // 
            // ssaESMTeamRejected
            // 
            this.ssaESMTeamRejected.Name = "ssaESMTeamRejected";
            this.ssaESMTeamRejected.TargetStateName = "saCompleteState";
            // 
            // ssESMTeamRejected
            // 
            correlationtoken2.Name = "workflowToken";
            correlationtoken2.OwnerActivityName = "ISESMServiceRequestWF";
            this.ssESMTeamRejected.CorrelationToken = correlationtoken2;
            this.ssESMTeamRejected.Name = "ssESMTeamRejected";
            this.ssESMTeamRejected.State = 20;
            // 
            // ssaESMTeamToRequester
            // 
            this.ssaESMTeamToRequester.Name = "ssaESMTeamToRequester";
            this.ssaESMTeamToRequester.TargetStateName = "saRequester";
            // 
            // ssaLineManagerRejected
            // 
            this.ssaLineManagerRejected.Name = "ssaLineManagerRejected";
            this.ssaLineManagerRejected.TargetStateName = "saCompleteState";
            // 
            // ssLineManagerRejected
            // 
            correlationtoken3.Name = "workflowToken";
            correlationtoken3.OwnerActivityName = "ISESMServiceRequestWF";
            this.ssLineManagerRejected.CorrelationToken = correlationtoken3;
            this.ssLineManagerRejected.Name = "ssLineManagerRejected";
            this.ssLineManagerRejected.State = 20;
            // 
            // ssaLineManagerToRequester
            // 
            this.ssaLineManagerToRequester.Name = "ssaLineManagerToRequester";
            this.ssaLineManagerToRequester.TargetStateName = "saRequester";
            // 
            // ESMManagerRejected
            // 
            this.ESMManagerRejected.Activities.Add(this.ssESMManagerRejected);
            this.ESMManagerRejected.Activities.Add(this.ssaESMManagerRejected);
            this.ESMManagerRejected.Name = "ESMManagerRejected";
            // 
            // ESMManagerRequestChange
            // 
            this.ESMManagerRequestChange.Activities.Add(this.ssaESMToRequester);
            codecondition1.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.ESMManagerDidNotApproveAction);
            this.ESMManagerRequestChange.Condition = codecondition1;
            this.ESMManagerRequestChange.Name = "ESMManagerRequestChange";
            // 
            // ESMTeamRejected
            // 
            this.ESMTeamRejected.Activities.Add(this.ssESMTeamRejected);
            this.ESMTeamRejected.Activities.Add(this.ssaESMTeamRejected);
            this.ESMTeamRejected.Name = "ESMTeamRejected";
            // 
            // ESMTeamRequestChange
            // 
            this.ESMTeamRequestChange.Activities.Add(this.ssaESMTeamToRequester);
            codecondition2.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.ESMTeamDidNotApproveAction);
            this.ESMTeamRequestChange.Condition = codecondition2;
            this.ESMTeamRequestChange.Name = "ESMTeamRequestChange";
            // 
            // LineManagerRejected
            // 
            this.LineManagerRejected.Activities.Add(this.ssLineManagerRejected);
            this.LineManagerRejected.Activities.Add(this.ssaLineManagerRejected);
            this.LineManagerRejected.Name = "LineManagerRejected";
            // 
            // LineManagerRequestChange
            // 
            this.LineManagerRequestChange.Activities.Add(this.ssaLineManagerToRequester);
            codecondition3.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.LineManagerDidNotApproveAction);
            this.LineManagerRequestChange.Condition = codecondition3;
            this.LineManagerRequestChange.Name = "LineManagerRequestChange";
            // 
            // ssaRequesterToCompleteState
            // 
            this.ssaRequesterToCompleteState.Name = "ssaRequesterToCompleteState";
            this.ssaRequesterToCompleteState.TargetStateName = "saCompleteState";
            // 
            // ssRequisitionCanceled
            // 
            correlationtoken4.Name = "workflowToken";
            correlationtoken4.OwnerActivityName = "ISESMServiceRequestWF";
            this.ssRequisitionCanceled.CorrelationToken = correlationtoken4;
            this.ssRequisitionCanceled.Name = "ssRequisitionCanceled";
            this.ssRequisitionCanceled.State = 21;
            // 
            // ssaRequesterToLineManager
            // 
            this.ssaRequesterToLineManager.Name = "ssaRequesterToLineManager";
            this.ssaRequesterToLineManager.TargetStateName = "saLineManager";
            // 
            // ifElseActivity6
            // 
            this.ifElseActivity6.Activities.Add(this.ESMManagerRequestChange);
            this.ifElseActivity6.Activities.Add(this.ESMManagerRejected);
            this.ifElseActivity6.Name = "ifElseActivity6";
            // 
            // ssaESMManagerToCompleteState
            // 
            this.ssaESMManagerToCompleteState.Name = "ssaESMManagerToCompleteState";
            this.ssaESMManagerToCompleteState.TargetStateName = "saCompleteState";
            // 
            // ssRequisitionApproved
            // 
            correlationtoken5.Name = "workflowToken";
            correlationtoken5.OwnerActivityName = "ISESMServiceRequestWF";
            this.ssRequisitionApproved.CorrelationToken = correlationtoken5;
            this.ssRequisitionApproved.Name = "ssRequisitionApproved";
            this.ssRequisitionApproved.State = 19;
            // 
            // ifElseActivity7
            // 
            this.ifElseActivity7.Activities.Add(this.ESMTeamRequestChange);
            this.ifElseActivity7.Activities.Add(this.ESMTeamRejected);
            this.ifElseActivity7.Name = "ifElseActivity7";
            // 
            // ssaESMTeamToESMManager
            // 
            this.ssaESMTeamToESMManager.Name = "ssaESMTeamToESMManager";
            this.ssaESMTeamToESMManager.TargetStateName = "saESMManager";
            // 
            // ifElseActivity5
            // 
            this.ifElseActivity5.Activities.Add(this.LineManagerRequestChange);
            this.ifElseActivity5.Activities.Add(this.LineManagerRejected);
            this.ifElseActivity5.Name = "ifElseActivity5";
            // 
            // ssaLineManagerToESMTeam
            // 
            this.ssaLineManagerToESMTeam.Name = "ssaLineManagerToESMTeam";
            this.ssaLineManagerToESMTeam.TargetStateName = "saESMTeam";
            // 
            // ifElseBranchActivity2
            // 
            this.ifElseBranchActivity2.Activities.Add(this.ssRequisitionCanceled);
            this.ifElseBranchActivity2.Activities.Add(this.ssaRequesterToCompleteState);
            this.ifElseBranchActivity2.Name = "ifElseBranchActivity2";
            // 
            // ifElseBranchActivity1
            // 
            this.ifElseBranchActivity1.Activities.Add(this.ssaRequesterToLineManager);
            codecondition4.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.RequesterAction);
            this.ifElseBranchActivity1.Condition = codecondition4;
            this.ifElseBranchActivity1.Name = "ifElseBranchActivity1";
            // 
            // ESMManagerDidNotApprove
            // 
            this.ESMManagerDidNotApprove.Activities.Add(this.ifElseActivity6);
            this.ESMManagerDidNotApprove.Name = "ESMManagerDidNotApprove";
            // 
            // ESMManagerApproved
            // 
            this.ESMManagerApproved.Activities.Add(this.ssRequisitionApproved);
            this.ESMManagerApproved.Activities.Add(this.ssaESMManagerToCompleteState);
            codecondition5.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.ESMManagerAction);
            this.ESMManagerApproved.Condition = codecondition5;
            this.ESMManagerApproved.Name = "ESMManagerApproved";
            // 
            // ESMTeamDidNotApprove
            // 
            this.ESMTeamDidNotApprove.Activities.Add(this.ifElseActivity7);
            this.ESMTeamDidNotApprove.Name = "ESMTeamDidNotApprove";
            // 
            // ESMTeamApproved
            // 
            this.ESMTeamApproved.Activities.Add(this.ssaESMTeamToESMManager);
            codecondition6.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.ESMTeamAction);
            this.ESMTeamApproved.Condition = codecondition6;
            this.ESMTeamApproved.Name = "ESMTeamApproved";
            // 
            // LineManagerDidNotApprove
            // 
            this.LineManagerDidNotApprove.Activities.Add(this.ifElseActivity5);
            this.LineManagerDidNotApprove.Name = "LineManagerDidNotApprove";
            // 
            // LineManagerApproved
            // 
            this.LineManagerApproved.Activities.Add(this.ssaLineManagerToESMTeam);
            codecondition7.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.LineManagerAction);
            this.LineManagerApproved.Condition = codecondition7;
            this.LineManagerApproved.Name = "LineManagerApproved";
            // 
            // ifElseActivity1
            // 
            this.ifElseActivity1.Activities.Add(this.ifElseBranchActivity1);
            this.ifElseActivity1.Activities.Add(this.ifElseBranchActivity2);
            this.ifElseActivity1.Name = "ifElseActivity1";
            // 
            // dtRequester
            // 
            correlationtoken6.Name = "RequesterToken";
            correlationtoken6.OwnerActivityName = "saRequester";
            this.dtRequester.CorrelationToken = correlationtoken6;
            this.dtRequester.Name = "dtRequester";
            activitybind1.Name = "ISESMServiceRequestWF";
            activitybind1.Path = "ctRequester_TaskId";
            this.dtRequester.SetBinding(Microsoft.SharePoint.WorkflowActions.DeleteTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind1)));
            // 
            // lthAfterRequesterAction
            // 
            this.lthAfterRequesterAction.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.lthAfterRequesterAction.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            activitybind2.Name = "ISESMServiceRequestWF";
            activitybind2.Path = "lthAfterRequesterAction_HistoryDescription";
            activitybind3.Name = "ISESMServiceRequestWF";
            activitybind3.Path = "lthAfterRequesterAction_HistoryOutcome";
            this.lthAfterRequesterAction.Name = "lthAfterRequesterAction";
            this.lthAfterRequesterAction.OtherData = "";
            activitybind4.Name = "ISESMServiceRequestWF";
            activitybind4.Path = "lthAfterRequesterAction_UserId";
            this.lthAfterRequesterAction.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryDescriptionProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind2)));
            this.lthAfterRequesterAction.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind3)));
            this.lthAfterRequesterAction.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.UserIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind4)));
            // 
            // otcRequester
            // 
            activitybind5.Name = "ISESMServiceRequestWF";
            activitybind5.Path = "otcRequester_AfterProperties";
            this.otcRequester.BeforeProperties = null;
            correlationtoken7.Name = "RequesterToken";
            correlationtoken7.OwnerActivityName = "saRequester";
            this.otcRequester.CorrelationToken = correlationtoken7;
            this.otcRequester.Executor = null;
            this.otcRequester.Name = "otcRequester";
            activitybind6.Name = "ISESMServiceRequestWF";
            activitybind6.Path = "ctRequester_TaskId";
            this.otcRequester.Invoked += new System.EventHandler<System.Workflow.Activities.ExternalDataEventArgs>(this.otcRequester_Invoked);
            this.otcRequester.SetBinding(Microsoft.SharePoint.WorkflowActions.OnTaskChanged.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind6)));
            this.otcRequester.SetBinding(Microsoft.SharePoint.WorkflowActions.OnTaskChanged.AfterPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind5)));
            // 
            // caSendRequesterTaskCreatedMail
            // 
            this.caSendRequesterTaskCreatedMail.Name = "caSendRequesterTaskCreatedMail";
            this.caSendRequesterTaskCreatedMail.ExecuteCode += new System.EventHandler(this.caSendRequesterTaskCreatedMail_ExecuteCode);
            // 
            // lthRequesterTaskCreated
            // 
            this.lthRequesterTaskCreated.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.lthRequesterTaskCreated.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            activitybind7.Name = "ISESMServiceRequestWF";
            activitybind7.Path = "lthRequesterTaskCreated_HistoryDescription";
            activitybind8.Name = "ISESMServiceRequestWF";
            activitybind8.Path = "lthRequesterTaskCreated_HistoryOutcome";
            this.lthRequesterTaskCreated.Name = "lthRequesterTaskCreated";
            this.lthRequesterTaskCreated.OtherData = "";
            this.lthRequesterTaskCreated.UserId = -1;
            this.lthRequesterTaskCreated.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryDescriptionProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind7)));
            this.lthRequesterTaskCreated.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind8)));
            // 
            // ssPendingRequesterUpdate
            // 
            correlationtoken8.Name = "workflowToken";
            correlationtoken8.OwnerActivityName = "ISESMServiceRequestWF";
            this.ssPendingRequesterUpdate.CorrelationToken = correlationtoken8;
            this.ssPendingRequesterUpdate.Name = "ssPendingRequesterUpdate";
            this.ssPendingRequesterUpdate.State = 15;
            this.ssPendingRequesterUpdate.MethodInvoking += new System.EventHandler(this.ssPendingRequesterUpdate_MethodInvoking);
            // 
            // ctRequester
            // 
            correlationtoken9.Name = "RequesterToken";
            correlationtoken9.OwnerActivityName = "saRequester";
            this.ctRequester.CorrelationToken = correlationtoken9;
            activitybind9.Name = "ISESMServiceRequestWF";
            activitybind9.Path = "ctRequester_ListItemId";
            this.ctRequester.Name = "ctRequester";
            activitybind10.Name = "ISESMServiceRequestWF";
            activitybind10.Path = "ctRequester_SpecialPermissions";
            activitybind11.Name = "ISESMServiceRequestWF";
            activitybind11.Path = "ctRequester_TaskId";
            activitybind12.Name = "ISESMServiceRequestWF";
            activitybind12.Path = "ctRequester_TaskProperties";
            this.ctRequester.MethodInvoking += new System.EventHandler(this.ctRequester_MethodInvoking);
            this.ctRequester.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind12)));
            this.ctRequester.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind11)));
            this.ctRequester.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.ListItemIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind9)));
            this.ctRequester.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.SpecialPermissionsProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind10)));
            // 
            // ifElseActivity4
            // 
            this.ifElseActivity4.Activities.Add(this.ESMManagerApproved);
            this.ifElseActivity4.Activities.Add(this.ESMManagerDidNotApprove);
            this.ifElseActivity4.Name = "ifElseActivity4";
            // 
            // dtESMManager
            // 
            correlationtoken10.Name = "ESMManagerToken";
            correlationtoken10.OwnerActivityName = "saESMManager";
            this.dtESMManager.CorrelationToken = correlationtoken10;
            this.dtESMManager.Name = "dtESMManager";
            activitybind13.Name = "ISESMServiceRequestWF";
            activitybind13.Path = "ctESMManager_TaskId";
            this.dtESMManager.SetBinding(Microsoft.SharePoint.WorkflowActions.DeleteTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind13)));
            // 
            // lthAfterESMManagerAction
            // 
            this.lthAfterESMManagerAction.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.lthAfterESMManagerAction.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            activitybind14.Name = "ISESMServiceRequestWF";
            activitybind14.Path = "lthAfterESMManagerAction_HistoryDescription";
            activitybind15.Name = "ISESMServiceRequestWF";
            activitybind15.Path = "lthAfterESMManagerAction_HistoryOutcome";
            this.lthAfterESMManagerAction.Name = "lthAfterESMManagerAction";
            this.lthAfterESMManagerAction.OtherData = "";
            activitybind16.Name = "ISESMServiceRequestWF";
            activitybind16.Path = "lthAfterESMManagerAction_UserId";
            this.lthAfterESMManagerAction.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryDescriptionProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind14)));
            this.lthAfterESMManagerAction.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind15)));
            this.lthAfterESMManagerAction.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.UserIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind16)));
            // 
            // otcESMManager
            // 
            activitybind17.Name = "ISESMServiceRequestWF";
            activitybind17.Path = "otcESMManager_AfterProperties";
            this.otcESMManager.BeforeProperties = null;
            correlationtoken11.Name = "ESMManagerToken";
            correlationtoken11.OwnerActivityName = "saESMManager";
            this.otcESMManager.CorrelationToken = correlationtoken11;
            this.otcESMManager.Executor = null;
            this.otcESMManager.Name = "otcESMManager";
            activitybind18.Name = "ISESMServiceRequestWF";
            activitybind18.Path = "ctESMManager_TaskId";
            this.otcESMManager.Invoked += new System.EventHandler<System.Workflow.Activities.ExternalDataEventArgs>(this.otcESMManager_Invoked);
            this.otcESMManager.SetBinding(Microsoft.SharePoint.WorkflowActions.OnTaskChanged.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind18)));
            this.otcESMManager.SetBinding(Microsoft.SharePoint.WorkflowActions.OnTaskChanged.AfterPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind17)));
            // 
            // caSendESMManagerTaskCreatedMail
            // 
            this.caSendESMManagerTaskCreatedMail.Name = "caSendESMManagerTaskCreatedMail";
            this.caSendESMManagerTaskCreatedMail.ExecuteCode += new System.EventHandler(this.caSendESMManagerTaskCreatedMail_ExecuteCode);
            // 
            // lthESMManagerTaskCreated
            // 
            this.lthESMManagerTaskCreated.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.lthESMManagerTaskCreated.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            activitybind19.Name = "ISESMServiceRequestWF";
            activitybind19.Path = "lthESMManagerTaskCreated_HistoryDescription";
            activitybind20.Name = "ISESMServiceRequestWF";
            activitybind20.Path = "lthESMManagerTaskCreated_HistoryOutcome";
            this.lthESMManagerTaskCreated.Name = "lthESMManagerTaskCreated";
            this.lthESMManagerTaskCreated.OtherData = "";
            this.lthESMManagerTaskCreated.UserId = -1;
            this.lthESMManagerTaskCreated.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryDescriptionProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind19)));
            this.lthESMManagerTaskCreated.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind20)));
            // 
            // ssPendingESMManagerApproval
            // 
            correlationtoken12.Name = "workflowToken";
            correlationtoken12.OwnerActivityName = "ISESMServiceRequestWF";
            this.ssPendingESMManagerApproval.CorrelationToken = correlationtoken12;
            this.ssPendingESMManagerApproval.Name = "ssPendingESMManagerApproval";
            this.ssPendingESMManagerApproval.State = 18;
            this.ssPendingESMManagerApproval.MethodInvoking += new System.EventHandler(this.ssPendingESMManagerApproval_MethodInvoking);
            // 
            // ctESMManager
            // 
            correlationtoken13.Name = "ESMManagerToken";
            correlationtoken13.OwnerActivityName = "saESMManager";
            this.ctESMManager.CorrelationToken = correlationtoken13;
            activitybind21.Name = "ISESMServiceRequestWF";
            activitybind21.Path = "ctESMManager_ListItemId";
            this.ctESMManager.Name = "ctESMManager";
            activitybind22.Name = "ISESMServiceRequestWF";
            activitybind22.Path = "ctESMManager_SpecialPermissions";
            activitybind23.Name = "ISESMServiceRequestWF";
            activitybind23.Path = "ctESMManager_TaskId";
            activitybind24.Name = "ISESMServiceRequestWF";
            activitybind24.Path = "ctESMManager_TaskProperties";
            this.ctESMManager.MethodInvoking += new System.EventHandler(this.ctESMManager_MethodInvoking);
            this.ctESMManager.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind24)));
            this.ctESMManager.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind23)));
            this.ctESMManager.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.ListItemIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind21)));
            this.ctESMManager.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.SpecialPermissionsProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind22)));
            // 
            // ifElseActivity3
            // 
            this.ifElseActivity3.Activities.Add(this.ESMTeamApproved);
            this.ifElseActivity3.Activities.Add(this.ESMTeamDidNotApprove);
            this.ifElseActivity3.Name = "ifElseActivity3";
            // 
            // dtESMTeam
            // 
            correlationtoken14.Name = "ESMTeamToken";
            correlationtoken14.OwnerActivityName = "saESMTeam";
            this.dtESMTeam.CorrelationToken = correlationtoken14;
            this.dtESMTeam.Name = "dtESMTeam";
            activitybind25.Name = "ISESMServiceRequestWF";
            activitybind25.Path = "ctESMTeam_TaskId";
            this.dtESMTeam.SetBinding(Microsoft.SharePoint.WorkflowActions.DeleteTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind25)));
            // 
            // lthAfterESMTeamAction
            // 
            this.lthAfterESMTeamAction.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.lthAfterESMTeamAction.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            activitybind26.Name = "ISESMServiceRequestWF";
            activitybind26.Path = "lthAfterESMTeamAction_HistoryDescription";
            activitybind27.Name = "ISESMServiceRequestWF";
            activitybind27.Path = "lthAfterESMTeamAction_HistoryOutcome";
            this.lthAfterESMTeamAction.Name = "lthAfterESMTeamAction";
            this.lthAfterESMTeamAction.OtherData = "";
            activitybind28.Name = "ISESMServiceRequestWF";
            activitybind28.Path = "lthAfterESMTeamAction_UserId";
            this.lthAfterESMTeamAction.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryDescriptionProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind26)));
            this.lthAfterESMTeamAction.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind27)));
            this.lthAfterESMTeamAction.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.UserIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind28)));
            // 
            // otcESMTeam
            // 
            activitybind29.Name = "ISESMServiceRequestWF";
            activitybind29.Path = "otcESMTeam_AfterProperties";
            this.otcESMTeam.BeforeProperties = null;
            correlationtoken15.Name = "ESMTeamToken";
            correlationtoken15.OwnerActivityName = "saESMTeam";
            this.otcESMTeam.CorrelationToken = correlationtoken15;
            this.otcESMTeam.Executor = null;
            this.otcESMTeam.Name = "otcESMTeam";
            activitybind30.Name = "ISESMServiceRequestWF";
            activitybind30.Path = "ctESMTeam_TaskId";
            this.otcESMTeam.Invoked += new System.EventHandler<System.Workflow.Activities.ExternalDataEventArgs>(this.otcESMTeam_Invoked);
            this.otcESMTeam.SetBinding(Microsoft.SharePoint.WorkflowActions.OnTaskChanged.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind30)));
            this.otcESMTeam.SetBinding(Microsoft.SharePoint.WorkflowActions.OnTaskChanged.AfterPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind29)));
            // 
            // caSendESMTeamTaskCreatedMail
            // 
            this.caSendESMTeamTaskCreatedMail.Name = "caSendESMTeamTaskCreatedMail";
            this.caSendESMTeamTaskCreatedMail.ExecuteCode += new System.EventHandler(this.caSendESMTeamTaskCreatedMail_ExecuteCode);
            // 
            // lthESMTeamTaskCreated
            // 
            this.lthESMTeamTaskCreated.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.lthESMTeamTaskCreated.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            activitybind31.Name = "ISESMServiceRequestWF";
            activitybind31.Path = "lthESMTeamTaskCreated_HistoryDescription";
            activitybind32.Name = "ISESMServiceRequestWF";
            activitybind32.Path = "lthESMTeamTaskCreated_HistoryOutcome";
            this.lthESMTeamTaskCreated.Name = "lthESMTeamTaskCreated";
            this.lthESMTeamTaskCreated.OtherData = "";
            this.lthESMTeamTaskCreated.UserId = -1;
            this.lthESMTeamTaskCreated.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryDescriptionProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind31)));
            this.lthESMTeamTaskCreated.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind32)));
            // 
            // ssPendingESMTeamApproval
            // 
            correlationtoken16.Name = "workflowToken";
            correlationtoken16.OwnerActivityName = "ISESMServiceRequestWF";
            this.ssPendingESMTeamApproval.CorrelationToken = correlationtoken16;
            this.ssPendingESMTeamApproval.Name = "ssPendingESMTeamApproval";
            this.ssPendingESMTeamApproval.State = 17;
            this.ssPendingESMTeamApproval.MethodInvoking += new System.EventHandler(this.ssPendingESMTeamApproval_MethodInvoking);
            // 
            // ctESMTeam
            // 
            correlationtoken17.Name = "ESMTeamToken";
            correlationtoken17.OwnerActivityName = "saESMTeam";
            this.ctESMTeam.CorrelationToken = correlationtoken17;
            activitybind33.Name = "ISESMServiceRequestWF";
            activitybind33.Path = "ctESMTeam_ListItemId";
            this.ctESMTeam.Name = "ctESMTeam";
            activitybind34.Name = "ISESMServiceRequestWF";
            activitybind34.Path = "ctESMTeam_SpecialPermissions";
            activitybind35.Name = "ISESMServiceRequestWF";
            activitybind35.Path = "ctESMTeam_TaskId";
            activitybind36.Name = "ISESMServiceRequestWF";
            activitybind36.Path = "ctESMTeam_TaskProperties";
            this.ctESMTeam.MethodInvoking += new System.EventHandler(this.ctESMTeam_MethodInvoking);
            this.ctESMTeam.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind36)));
            this.ctESMTeam.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind35)));
            this.ctESMTeam.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.ListItemIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind33)));
            this.ctESMTeam.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.SpecialPermissionsProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind34)));
            // 
            // ifElseActivity2
            // 
            this.ifElseActivity2.Activities.Add(this.LineManagerApproved);
            this.ifElseActivity2.Activities.Add(this.LineManagerDidNotApprove);
            this.ifElseActivity2.Name = "ifElseActivity2";
            // 
            // dtLineManager
            // 
            correlationtoken18.Name = "LineManagerToken";
            correlationtoken18.OwnerActivityName = "saLineManager";
            this.dtLineManager.CorrelationToken = correlationtoken18;
            this.dtLineManager.Name = "dtLineManager";
            activitybind37.Name = "ISESMServiceRequestWF";
            activitybind37.Path = "ctLineManager_TaskId";
            this.dtLineManager.MethodInvoking += new System.EventHandler(this.dtLineManager_MethodInvoking);
            this.dtLineManager.SetBinding(Microsoft.SharePoint.WorkflowActions.DeleteTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind37)));
            // 
            // lthAfterLineManagerAction
            // 
            this.lthAfterLineManagerAction.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.lthAfterLineManagerAction.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            activitybind38.Name = "ISESMServiceRequestWF";
            activitybind38.Path = "lthAfterLineManagerAction_HistoryDescription";
            activitybind39.Name = "ISESMServiceRequestWF";
            activitybind39.Path = "lthAfterLineManagerAction_HistoryOutcome";
            this.lthAfterLineManagerAction.Name = "lthAfterLineManagerAction";
            this.lthAfterLineManagerAction.OtherData = "";
            activitybind40.Name = "ISESMServiceRequestWF";
            activitybind40.Path = "lthAfterLineManagerAction_UserId";
            this.lthAfterLineManagerAction.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryDescriptionProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind38)));
            this.lthAfterLineManagerAction.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind39)));
            this.lthAfterLineManagerAction.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.UserIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind40)));
            // 
            // otcLineManager
            // 
            activitybind41.Name = "ISESMServiceRequestWF";
            activitybind41.Path = "otcLineManager_AfterProperties";
            this.otcLineManager.BeforeProperties = null;
            correlationtoken19.Name = "LineManagerToken";
            correlationtoken19.OwnerActivityName = "saLineManager";
            this.otcLineManager.CorrelationToken = correlationtoken19;
            this.otcLineManager.Executor = null;
            this.otcLineManager.Name = "otcLineManager";
            activitybind42.Name = "ISESMServiceRequestWF";
            activitybind42.Path = "ctLineManager_TaskId";
            this.otcLineManager.Invoked += new System.EventHandler<System.Workflow.Activities.ExternalDataEventArgs>(this.otcLineManager_Invoked);
            this.otcLineManager.SetBinding(Microsoft.SharePoint.WorkflowActions.OnTaskChanged.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind42)));
            this.otcLineManager.SetBinding(Microsoft.SharePoint.WorkflowActions.OnTaskChanged.AfterPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind41)));
            // 
            // caSendLineManagerTaskCreatedMail
            // 
            this.caSendLineManagerTaskCreatedMail.Name = "caSendLineManagerTaskCreatedMail";
            this.caSendLineManagerTaskCreatedMail.ExecuteCode += new System.EventHandler(this.caSendLineManagerTaskCreatedMail_ExecuteCode);
            // 
            // lthLineManagerTaskCreated
            // 
            this.lthLineManagerTaskCreated.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.lthLineManagerTaskCreated.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            activitybind43.Name = "ISESMServiceRequestWF";
            activitybind43.Path = "lthLineManagerTaskCreated_HistoryDescription";
            activitybind44.Name = "ISESMServiceRequestWF";
            activitybind44.Path = "lthLineManagerTaskCreated_HistoryOutcome";
            this.lthLineManagerTaskCreated.Name = "lthLineManagerTaskCreated";
            this.lthLineManagerTaskCreated.OtherData = "";
            this.lthLineManagerTaskCreated.UserId = -1;
            this.lthLineManagerTaskCreated.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryDescriptionProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind43)));
            this.lthLineManagerTaskCreated.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind44)));
            // 
            // ssPendingLineManagerApproval
            // 
            correlationtoken20.Name = "workflowToken";
            correlationtoken20.OwnerActivityName = "ISESMServiceRequestWF";
            this.ssPendingLineManagerApproval.CorrelationToken = correlationtoken20;
            this.ssPendingLineManagerApproval.Name = "ssPendingLineManagerApproval";
            this.ssPendingLineManagerApproval.State = 16;
            this.ssPendingLineManagerApproval.MethodInvoking += new System.EventHandler(this.ssPendingLineManagerApproval_MethodInvoking);
            // 
            // ctLineManager
            // 
            correlationtoken21.Name = "LineManagerToken";
            correlationtoken21.OwnerActivityName = "saLineManager";
            this.ctLineManager.CorrelationToken = correlationtoken21;
            activitybind45.Name = "ISESMServiceRequestWF";
            activitybind45.Path = "ctLineManager_ListItemId";
            this.ctLineManager.Name = "ctLineManager";
            activitybind46.Name = "ISESMServiceRequestWF";
            activitybind46.Path = "ctLineManager_SpecialPermissions";
            activitybind47.Name = "ISESMServiceRequestWF";
            activitybind47.Path = "ctLineManager_TaskId";
            activitybind48.Name = "ISESMServiceRequestWF";
            activitybind48.Path = "ctLineManager_TaskProperties";
            this.ctLineManager.MethodInvoking += new System.EventHandler(this.ctLineManager_MethodInvoking);
            this.ctLineManager.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind48)));
            this.ctLineManager.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind47)));
            this.ctLineManager.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.ListItemIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind45)));
            this.ctLineManager.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.SpecialPermissionsProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind46)));
            // 
            // ssaToLineManager
            // 
            this.ssaToLineManager.Name = "ssaToLineManager";
            this.ssaToLineManager.TargetStateName = "saLineManager";
            // 
            // onWorkflowActivated
            // 
            correlationtoken22.Name = "workflowToken";
            correlationtoken22.OwnerActivityName = "ISESMServiceRequestWF";
            this.onWorkflowActivated.CorrelationToken = correlationtoken22;
            this.onWorkflowActivated.EventName = "OnWorkflowActivated";
            this.onWorkflowActivated.Name = "onWorkflowActivated";
            activitybind49.Name = "ISESMServiceRequestWF";
            activitybind49.Path = "workflowProperties";
            this.onWorkflowActivated.Invoked += new System.EventHandler<System.Workflow.Activities.ExternalDataEventArgs>(this.onWorkflowActivated_Invoked);
            this.onWorkflowActivated.SetBinding(Microsoft.SharePoint.WorkflowActions.OnWorkflowActivated.WorkflowPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind49)));
            // 
            // edaRequester
            // 
            this.edaRequester.Activities.Add(this.otcRequester);
            this.edaRequester.Activities.Add(this.lthAfterRequesterAction);
            this.edaRequester.Activities.Add(this.dtRequester);
            this.edaRequester.Activities.Add(this.ifElseActivity1);
            this.edaRequester.Name = "edaRequester";
            // 
            // siaRequester
            // 
            this.siaRequester.Activities.Add(this.ctRequester);
            this.siaRequester.Activities.Add(this.ssPendingRequesterUpdate);
            this.siaRequester.Activities.Add(this.lthRequesterTaskCreated);
            this.siaRequester.Activities.Add(this.caSendRequesterTaskCreatedMail);
            this.siaRequester.Name = "siaRequester";
            // 
            // edaESMManager
            // 
            this.edaESMManager.Activities.Add(this.otcESMManager);
            this.edaESMManager.Activities.Add(this.lthAfterESMManagerAction);
            this.edaESMManager.Activities.Add(this.dtESMManager);
            this.edaESMManager.Activities.Add(this.ifElseActivity4);
            this.edaESMManager.Name = "edaESMManager";
            // 
            // siaESMManager
            // 
            this.siaESMManager.Activities.Add(this.ctESMManager);
            this.siaESMManager.Activities.Add(this.ssPendingESMManagerApproval);
            this.siaESMManager.Activities.Add(this.lthESMManagerTaskCreated);
            this.siaESMManager.Activities.Add(this.caSendESMManagerTaskCreatedMail);
            this.siaESMManager.Name = "siaESMManager";
            // 
            // edaESMTeam
            // 
            this.edaESMTeam.Activities.Add(this.otcESMTeam);
            this.edaESMTeam.Activities.Add(this.lthAfterESMTeamAction);
            this.edaESMTeam.Activities.Add(this.dtESMTeam);
            this.edaESMTeam.Activities.Add(this.ifElseActivity3);
            this.edaESMTeam.Name = "edaESMTeam";
            // 
            // siaESMTeam
            // 
            this.siaESMTeam.Activities.Add(this.ctESMTeam);
            this.siaESMTeam.Activities.Add(this.ssPendingESMTeamApproval);
            this.siaESMTeam.Activities.Add(this.lthESMTeamTaskCreated);
            this.siaESMTeam.Activities.Add(this.caSendESMTeamTaskCreatedMail);
            this.siaESMTeam.Name = "siaESMTeam";
            // 
            // edaLineManager
            // 
            this.edaLineManager.Activities.Add(this.otcLineManager);
            this.edaLineManager.Activities.Add(this.lthAfterLineManagerAction);
            this.edaLineManager.Activities.Add(this.dtLineManager);
            this.edaLineManager.Activities.Add(this.ifElseActivity2);
            this.edaLineManager.Name = "edaLineManager";
            // 
            // siaLinemanager
            // 
            this.siaLinemanager.Activities.Add(this.ctLineManager);
            this.siaLinemanager.Activities.Add(this.ssPendingLineManagerApproval);
            this.siaLinemanager.Activities.Add(this.lthLineManagerTaskCreated);
            this.siaLinemanager.Activities.Add(this.caSendLineManagerTaskCreatedMail);
            this.siaLinemanager.Name = "siaLinemanager";
            // 
            // eventDrivenActivity1
            // 
            this.eventDrivenActivity1.Activities.Add(this.onWorkflowActivated);
            this.eventDrivenActivity1.Activities.Add(this.ssaToLineManager);
            this.eventDrivenActivity1.Name = "eventDrivenActivity1";
            // 
            // saCompleteState
            // 
            this.saCompleteState.Name = "saCompleteState";
            // 
            // saRequester
            // 
            this.saRequester.Activities.Add(this.siaRequester);
            this.saRequester.Activities.Add(this.edaRequester);
            this.saRequester.Name = "saRequester";
            // 
            // saESMManager
            // 
            this.saESMManager.Activities.Add(this.siaESMManager);
            this.saESMManager.Activities.Add(this.edaESMManager);
            this.saESMManager.Name = "saESMManager";
            // 
            // saESMTeam
            // 
            this.saESMTeam.Activities.Add(this.siaESMTeam);
            this.saESMTeam.Activities.Add(this.edaESMTeam);
            this.saESMTeam.Name = "saESMTeam";
            // 
            // saLineManager
            // 
            this.saLineManager.Activities.Add(this.siaLinemanager);
            this.saLineManager.Activities.Add(this.edaLineManager);
            this.saLineManager.Name = "saLineManager";
            // 
            // ISESMServiceRequestWFInitialState
            // 
            this.ISESMServiceRequestWFInitialState.Activities.Add(this.eventDrivenActivity1);
            this.ISESMServiceRequestWFInitialState.Name = "ISESMServiceRequestWFInitialState";
            // 
            // ISESMServiceRequestWF
            // 
            this.Activities.Add(this.ISESMServiceRequestWFInitialState);
            this.Activities.Add(this.saLineManager);
            this.Activities.Add(this.saESMTeam);
            this.Activities.Add(this.saESMManager);
            this.Activities.Add(this.saRequester);
            this.Activities.Add(this.saCompleteState);
            this.CompletedStateName = "saCompleteState";
            this.DynamicUpdateCondition = null;
            this.InitialStateName = "ISESMServiceRequestWFInitialState";
            this.Name = "ISESMServiceRequestWF";
            this.CanModifyActivities = false;

        }

        #endregion

        private SetStateActivity ssaToLineManager;
        private Microsoft.SharePoint.WorkflowActions.OnWorkflowActivated onWorkflowActivated;
        private EventDrivenActivity eventDrivenActivity1;
        private StateActivity saCompleteState;
        private SetStateActivity ssaRequesterToCompleteState;
        private Microsoft.SharePoint.WorkflowActions.SetState ssRequisitionCanceled;
        private SetStateActivity ssaRequesterToLineManager;
        private IfElseBranchActivity ifElseBranchActivity2;
        private IfElseBranchActivity ifElseBranchActivity1;
        private IfElseActivity ifElseActivity1;
        private Microsoft.SharePoint.WorkflowActions.DeleteTask dtRequester;
        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity lthAfterRequesterAction;
        private Microsoft.SharePoint.WorkflowActions.OnTaskChanged otcRequester;
        private CodeActivity caSendRequesterTaskCreatedMail;
        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity lthRequesterTaskCreated;
        private Microsoft.SharePoint.WorkflowActions.SetState ssPendingRequesterUpdate;
        private Microsoft.SharePoint.WorkflowActions.CreateTask ctRequester;
        private EventDrivenActivity edaRequester;
        private StateInitializationActivity siaRequester;
        private StateActivity saRequester;
        private SetStateActivity ssaESMManagerRejected;
        private Microsoft.SharePoint.WorkflowActions.SetState ssESMManagerRejected;
        private SetStateActivity ssaESMToRequester;
        private IfElseBranchActivity ESMManagerRejected;
        private IfElseBranchActivity ESMManagerRequestChange;
        private IfElseActivity ifElseActivity6;
        private SetStateActivity ssaESMManagerToCompleteState;
        private Microsoft.SharePoint.WorkflowActions.SetState ssRequisitionApproved;
        private IfElseBranchActivity ESMManagerDidNotApprove;
        private IfElseBranchActivity ESMManagerApproved;
        private IfElseActivity ifElseActivity4;
        private Microsoft.SharePoint.WorkflowActions.DeleteTask dtESMManager;
        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity lthAfterESMManagerAction;
        private Microsoft.SharePoint.WorkflowActions.OnTaskChanged otcESMManager;
        private CodeActivity caSendESMManagerTaskCreatedMail;
        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity lthESMManagerTaskCreated;
        private Microsoft.SharePoint.WorkflowActions.SetState ssPendingESMManagerApproval;
        private Microsoft.SharePoint.WorkflowActions.CreateTask ctESMManager;
        private EventDrivenActivity edaESMManager;
        private StateInitializationActivity siaESMManager;
        private StateActivity saESMManager;
        private SetStateActivity ssaESMTeamRejected;
        private Microsoft.SharePoint.WorkflowActions.SetState ssESMTeamRejected;
        private SetStateActivity ssaESMTeamToRequester;
        private IfElseBranchActivity ESMTeamRejected;
        private IfElseBranchActivity ESMTeamRequestChange;
        private IfElseActivity ifElseActivity7;
        private SetStateActivity ssaESMTeamToESMManager;
        private IfElseBranchActivity ESMTeamDidNotApprove;
        private IfElseBranchActivity ESMTeamApproved;
        private IfElseActivity ifElseActivity3;
        private Microsoft.SharePoint.WorkflowActions.DeleteTask dtESMTeam;
        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity lthAfterESMTeamAction;
        private Microsoft.SharePoint.WorkflowActions.OnTaskChanged otcESMTeam;
        private CodeActivity caSendESMTeamTaskCreatedMail;
        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity lthESMTeamTaskCreated;
        private Microsoft.SharePoint.WorkflowActions.SetState ssPendingESMTeamApproval;
        private Microsoft.SharePoint.WorkflowActions.CreateTask ctESMTeam;
        private EventDrivenActivity edaESMTeam;
        private StateInitializationActivity siaESMTeam;
        private StateActivity saESMTeam;
        private SetStateActivity ssaLineManagerRejected;
        private Microsoft.SharePoint.WorkflowActions.SetState ssLineManagerRejected;
        private SetStateActivity ssaLineManagerToRequester;
        private IfElseBranchActivity LineManagerRejected;
        private IfElseBranchActivity LineManagerRequestChange;
        private IfElseActivity ifElseActivity5;
        private SetStateActivity ssaLineManagerToESMTeam;
        private IfElseBranchActivity LineManagerDidNotApprove;
        private IfElseBranchActivity LineManagerApproved;
        private IfElseActivity ifElseActivity2;
        private Microsoft.SharePoint.WorkflowActions.DeleteTask dtLineManager;
        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity lthAfterLineManagerAction;
        private Microsoft.SharePoint.WorkflowActions.OnTaskChanged otcLineManager;
        private CodeActivity caSendLineManagerTaskCreatedMail;
        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity lthLineManagerTaskCreated;
        private Microsoft.SharePoint.WorkflowActions.SetState ssPendingLineManagerApproval;
        private Microsoft.SharePoint.WorkflowActions.CreateTask ctLineManager;
        private EventDrivenActivity edaLineManager;
        private StateInitializationActivity siaLinemanager;
        private StateActivity saLineManager;
        private StateActivity ISESMServiceRequestWFInitialState;
    }
}
