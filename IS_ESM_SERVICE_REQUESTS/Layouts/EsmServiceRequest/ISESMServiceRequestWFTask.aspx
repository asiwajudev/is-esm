﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ISESMServiceRequestWFTask.aspx.cs" Inherits="IS_ESM_SERVICE_REQUESTS.Layouts.IS_ESM_SERVICE_REQUESTS.ISESMServiceRequestWFTask" DynamicMasterPageFile="~masterurl/default.master" %>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <link href="ClientStyles/Common.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
    IS ESM Service Request
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea"
    runat="server">
    IS ESM Service Request
</asp:Content>
<asp:Content ID="HomePageContent" ContentPlaceHolderID="PlaceHolderMain" runat="Server">
    <style type="text/css">
        .commentstablestyle {
            font-size: 12px;
            font-family: arial;
            color: #505050;
            border: solid 2px #CDCDCD;
            border-collapse: collapse;
            margin-bottom: 2px;
            background-color: #FEFEFE;
        }

        .commentsdate {
            font-size: 11px;
            color: #990000;
        }
    </style>
    <table bordercolor="#008000" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td style="width: 100%" valign="top" align="left">
                <table class="ms-formtable" style="margin-top: 8px" cellspacing="0" cellpadding="0"
                    width="100%" border="0">
                    <tr runat="server" id="rowMessage" visible="false">
                        <td style="color: Red; font-weight: bold; height: 30px" colspan="2" valign="top"
                            nowrap width="590"></td>
                    </tr>
                    <tr>
                        <td class="td_Label" valign="top" width="auto" colspan="2" style="font-weight: bold">
                            <div class="div_Header">
                                Approval
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Request Type<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblRequestType" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>User Name<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblUserName" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Telephone No<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblTelephoneNo" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>E-Mail<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblEMail" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Office Location<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblOfficeLocation" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Designation</nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblDesignation" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Line Manager<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblLineManager" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Remedy Incident Number</nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblChangeNumber" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:InputFormTextBox ID="txtChangeNumber" RichText="false" TextMode="Singleline"
                                runat="server" MaxLength="8" title="Change Number" class="ms-long" Visible="false" />
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Date Required<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblDateRequired" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:DateTimeControl ID="dtcDateRequired" LocaleId="2057" ToolTip="DD/MM/YYYY"
                                runat="server" DateOnly="true" IsRequiredField="true" Visible="false"></SharePoint:DateTimeControl>
                        </td>
                    </tr>
                    <tr runat="server" id="rowESMManager" style="display: none;">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>ESM Manager<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <SharePoint:PeopleEditor ID="_peESMManager" runat="server" Rows="1" MaximumEntities="1"
                                MultiSelect="false" PlaceButtonsUnderEntityEditor="false" PrincipalSource="All"
                                SelectionSet="User" AllowEmpty="false" />
                            <span style="display: none" class="ms-error" id="_spanESMManager" runat="server">You
                                must specify a value for this required field.</span>
                        </td>
                    </tr>
                    <tr id="MonitoringRow1" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Server Name<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblServerName" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:InputFormTextBox Visible="false" ID="txtServerName" RichText="false"
                                TextMode="Singleline" runat="server" MaxLength="32" title="Server Name" class="ms-long" />
                            <SharePoint:InputFormRequiredFieldValidator Visible="false" ID="txtServerNameValidator"
                                class="ms-error" ControlToValidate="txtServerName" Text="Error" runat="server"
                                ErrorMessage="You must specify a value for this required field." EnableClientScript="true"
                                Display="Dynamic">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </td>
                    </tr>
                    <tr id="MonitoringRow2" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>IP Address<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblIPAddress" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:InputFormTextBox Visible="false" ID="txtIPAddress" RichText="false" TextMode="Singleline"
                                runat="server" MaxLength="15" title="IP Address" class="ms-long" />
                            <SharePoint:InputFormRequiredFieldValidator Visible="false" ID="txtIPAddressValidator"
                                class="ms-error" ControlToValidate="txtIPAddress" Text="Error" runat="server"
                                ErrorMessage="You must specify a value for this required field." EnableClientScript="true"
                                Display="Dynamic">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </td>
                    </tr>
                    <tr id="MonitoringRow3" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Is DMZ Server<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblIsDMZServer" Text="&nbsp;" runat="server"></asp:Label>
                            <asp:RadioButtonList Visible="false" ID="rblIsDMZServer" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="True">Yes</asp:ListItem>
                                <asp:ListItem Selected="True" Value="False">No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr id="MonitoringRow4" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Operating System<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblOperatingSystem" Text="&nbsp;" runat="server"></asp:Label>
                            <asp:RadioButtonList Visible="false" ID="rblOperatingSystem" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Value="Windows">Windows</asp:ListItem>
                                <asp:ListItem Value="Unix">Unix</asp:ListItem>
                                <asp:ListItem Value="Linux">Linux</asp:ListItem>
                                <asp:ListItem Value="Others">Others</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr id="MonitoringRow5" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Server Class<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblServerClass" Text="&nbsp;" runat="server"></asp:Label>
                            <asp:RadioButtonList Visible="false" ID="rblServerClass" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="Essential Business Support" Selected="True">Essential Business Support</asp:ListItem>
                                <asp:ListItem Value="Mission Critical">Mission Critical</asp:ListItem>
                                <asp:ListItem Value="Service Enabler">Service Enabler</asp:ListItem>
                                <asp:ListItem Value="Subscriber Critical">Subscriber Critical</asp:ListItem>
                                <asp:ListItem Value="Core Services">Core Services</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr id="MonitoringRow20" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Application Name <SPAN class="ms-formvalidation"> *</SPAN><br> State the Application that the Server host</nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblApplicationName" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:InputFormTextBox ID="txtApplicationName" RichText="false" TextMode="Singleline"
                                runat="server" MaxLength="32" title="Server Name" class="ms-long" Visible="false" />
                            <SharePoint:InputFormRequiredFieldValidator ID="txtApplicationNameValidator" class="ms-error"
                                ControlToValidate="txtApplicationName" Text="Error" runat="server" ErrorMessage="You must specify a value for this required field."
                                EnableClientScript="true" Display="Dynamic" Visible="false">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </td>
                    </tr>
                    <tr id="MonitoringRow21" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Global_zone/Physical Server<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblGlobalZone" Text="&nbsp;" runat="server"></asp:Label>
                            <asp:RadioButtonList ID="rblGlobalZone" runat="server" RepeatDirection="Horizontal" Visible="false">
                                <asp:ListItem Value="True">Yes</asp:ListItem>
                                <asp:ListItem Value="False" Selected="True">No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr id="MonitoringRow6" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Oracle Installed<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblOracleInstalled" Text="&nbsp;" runat="server"></asp:Label>
                            <asp:RadioButtonList Visible="false" ID="rblOracleInstalled" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="True">Yes</asp:ListItem>
                                <asp:ListItem Value="False" Selected="True">No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr id="MonitoringRow7" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>MS Exchange Installed<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblMSExchangeInstalled" Text="&nbsp;" runat="server"></asp:Label>
                            <asp:RadioButtonList Visible="false" ID="rblMSExchangeInstalled" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="True">Yes</asp:ListItem>
                                <asp:ListItem Value="False" Selected="True">No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr id="MonitoringRow8" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>MSSQL Installed<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblMSSQLInstalled" Text="&nbsp;" runat="server"></asp:Label>
                            <asp:RadioButtonList Visible="false" ID="rblMSSQLInstalled" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="True">Yes</asp:ListItem>
                                <asp:ListItem Value="False" Selected="True">No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr id="MonitoringRow9" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Other Applications</nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblOtherApplications" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:InputFormTextBox Visible="false" ID="txtOtherApplications" RichText="false"
                                TextMode="MultiLine" runat="server" Rows="6" title="Other Applications"
                                class="ms-long" />
                        </td>
                    </tr>
                    <tr id="MonitoringRow10" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Log File</nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblLogFile" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:InputFormTextBox Visible="false" ID="txtLogFile" RichText="false" TextMode="Singleline"
                                runat="server" MaxLength="255" title="Log File" class="ms-long" />
                        </td>
                    </tr>
                    <tr id="MonitoringRow11" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">Search String / Monitoring Condition Note
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblSearchString_MonitoringConditionNote" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:InputFormTextBox Visible="false" ID="txtSearchString_MonitoringConditionNote"
                                RichText="false" TextMode="MultiLine" runat="server" Rows="4"
                                title="Other Applications" class="ms-long" />
                        </td>
                    </tr>
                    <tr id="MonitoringRow12" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Processes / Services</nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblProcesses_Services" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:InputFormTextBox Visible="false" ID="txtProcesses_Services" RichText="false"
                                TextMode="MultiLine" runat="server" Rows="4" title="Processes / Services"
                                class="ms-long" />
                        </td>
                    </tr>
                    <tr id="MonitoringRow13" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Command and User Name</nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblCommandAndUserName" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:InputFormTextBox Visible="false" ID="txtCommandAndUserName" RichText="false"
                                TextMode="MultiLine" runat="server" Rows="4" title="Command and User Name "
                                class="ms-long" />
                        </td>
                    </tr>
                    <tr id="ControlMRow1" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Script Name and Path<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblScriptNameAndPath" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:InputFormTextBox Visible="false" ID="txtScriptNameAndPath" RichText="false"
                                TextMode="Singleline" runat="server" MaxLength="255" title="Script Name and Path"
                                class="ms-long" />
                            <SharePoint:InputFormRequiredFieldValidator Visible="false" ID="txtScriptNameAndPathValidator"
                                class="ms-error" ControlToValidate="txtScriptNameAndPath" Text="Error" runat="server"
                                ErrorMessage="You must specify a value for this required field." EnableClientScript="true"
                                Display="Dynamic">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </td>
                    </tr>
                    <tr id="ControlMRow2" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Job Description<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblJobDescription" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:InputFormTextBox Visible="false" ID="txtJobDescription" RichText="false"
                                TextMode="Singleline" runat="server" MaxLength="255" title="Job Description"
                                class="ms-long" />
                            <SharePoint:InputFormRequiredFieldValidator Visible="false" ID="txtJobDescriptionValidator"
                                class="ms-error" ControlToValidate="txtJobDescription" Text="Error" runat="server"
                                ErrorMessage="You must specify a value for this required field." EnableClientScript="true"
                                Display="Dynamic">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </td>
                    </tr>
                    <tr id="ControlMRow3" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Script Log File Path</nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblScriptLogFilePath" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:InputFormTextBox Visible="false" ID="txtScriptLogFilePath" RichText="false"
                                TextMode="Singleline" runat="server" MaxLength="255" title="Script Log File Path"
                                class="ms-long" />
                        </td>
                    </tr>
                    <tr id="ControlMRow4" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Cyclic<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblCyclic" Text="&nbsp;" runat="server"></asp:Label>
                            <asp:RadioButtonList Visible="false" ID="rblCyclic" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="Yes (From Start)" onclick="javascript:ShowHideCyclicIntervalRow();">Yes (From Start)</asp:ListItem>
                                <asp:ListItem Value="Yes (From End)" onclick="javascript:ShowHideCyclicIntervalRow();">Yes (From End)</asp:ListItem>
                                <asp:ListItem Selected="True" Value="No" onclick="javascript:ShowHideCyclicIntervalRow();">No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr id="ControlMRow6" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Cyclic Restart Interval<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblCyclicRestartInterval" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:InputFormTextBox Visible="false" ID="txtCyclicRestartInterval" RichText="false"
                                TextMode="Singleline" runat="server" MaxLength="255" title="Cyclic Restart Interval"
                                class="ms-long" />
                            <SharePoint:InputFormRequiredFieldValidator Visible="false" ID="txtCyclicRestartIntervalValidator"
                                class="ms-error" ControlToValidate="txtCyclicRestartInterval" Text="Error" runat="server"
                                ErrorMessage="You must specify a value for this required field." EnableClientScript="true"
                                Display="Dynamic">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </td>
                    </tr>
                    <tr id="ControlMRow7" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Frequency<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblFrequency" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:InputFormTextBox Visible="false" ID="txtFrequency" RichText="false" TextMode="Singleline"
                                runat="server" MaxLength="255" title="Frequency" class="ms-long" />
                            <SharePoint:InputFormRequiredFieldValidator Visible="false" ID="txtFrequencyValidator"
                                class="ms-error" ControlToValidate="txtFrequency" Text="Error" runat="server"
                                ErrorMessage="You must specify a value for this required field." EnableClientScript="true"
                                Display="Dynamic">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </td>
                    </tr>
                    <tr id="ControlMRow8" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Condition<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblCondition" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:InputFormTextBox Visible="false" ID="txtCondition" RichText="false" TextMode="Singleline"
                                runat="server" MaxLength="255" title="Condition" class="ms-long" />
                            <SharePoint:InputFormRequiredFieldValidator Visible="false" ID="txtConditionValidator"
                                class="ms-error" ControlToValidate="txtCondition" Text="Error" runat="server"
                                ErrorMessage="You must specify a value for this required field." EnableClientScript="true"
                                Display="Dynamic">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </td>
                    </tr>
                    <tr id="ControlMRow9" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Test Server Name</nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblTestServerName" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:InputFormTextBox Visible="false" ID="txtTestServerName" RichText="false"
                                TextMode="Singleline" runat="server" MaxLength="255" title="Test Server Name"
                                class="ms-long" />
                        </td>
                    </tr>
                    <tr id="ControlMRow10" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Production Server Name<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblProductionServerName" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:InputFormTextBox Visible="false" ID="txtProductionServerName" RichText="false"
                                TextMode="Singleline" runat="server" MaxLength="255" title="Production Server Name"
                                class="ms-long" />
                            <SharePoint:InputFormRequiredFieldValidator Visible="false" ID="txtProductionServerNameValidator"
                                class="ms-error" ControlToValidate="txtProductionServerName" Text="Error" runat="server"
                                ErrorMessage="You must specify a value for this required field." EnableClientScript="true"
                                Display="Dynamic">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </td>
                    </tr>
                    <tr id="ControlMRow11" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">Is The Server Behind Firewall<span class="ms-formvalidation"> *</span>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblIsTheServerBehindFirewall" Text="&nbsp;" runat="server"></asp:Label>
                            <asp:RadioButtonList Visible="false" ID="rblIsTheServerBehindFirewall" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Value="True">Yes</asp:ListItem>
                                <asp:ListItem Selected="True" Value="False">No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr id="ControlMRow12" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">Mailing List Used For Notification<span class="ms-formvalidation"> *</span>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblMailingListUsedForNotification" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:PeopleEditor Visible="false" ID="peMailingListUsedForNotification" runat="server"
                                Rows="1" MultiSelect="true" PlaceButtonsUnderEntityEditor="false" PrincipalSource="All"
                                SelectionSet="User" AllowEmpty="false" />
                            <span style="display: none" class="ms-error" id="spanMailingListUsedForNotification"
                                runat="server">You must specify a value for this required field.</span>
                        </td>
                    </tr>
                    <tr id="ControlMRow13" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">Remedy Group Used For Failure Notification<span class="ms-formvalidation"> *</span>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblRemedyGroup" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:InputFormTextBox Visible="false" ID="txtRemedyGroup" RichText="false"
                                TextMode="Singleline" runat="server" MaxLength="128" title="Frequency" class="ms-long" />
                            <SharePoint:InputFormRequiredFieldValidator Visible="false" ID="txtRemedyGroupValidator"
                                class="ms-error" ControlToValidate="txtRemedyGroup" Text="Error" runat="server"
                                ErrorMessage="You must specify a value for this required field." EnableClientScript="true"
                                Display="Dynamic">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Comments</nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Literal ID="ltlCommentsHistory" runat="server" Mode="Transform"></asp:Literal>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>New Comment</nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:TextBox ID="txtComment" runat="server" ClientIDMode="Static" Rows="3"  TextMode="MultiLine" Width="400px" AutoPostBack="false"></asp:TextBox>
                            <%--<span style="display: none" class="ms-error" id="txtRequesterComment" runat="server">Comment is Required on what is to be updated
                            </span>--%>

                        </td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="0" width="100%">
                    <tbody>
                        <tr>
                            <td class="ms-formline">
                                <img height="1" alt="" src="/_layouts/images/blank.gif" width="1" />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table class="ms-formtoolbar" id="tblBottomButtons" cellspacing="0" cellpadding="2"
                    width="100%" border="0">
                    <tr>
                        <td class="ms-toolbar" nowrap width="99%">
                            <img height="18" alt="" src="/_layouts/images/blank.gif" width="1" />
                        </td>
                        <td class="ms-toolbar" nowrap>
                            <asp:Button class="ms-ButtonHeightWidth" ID="btnApprove" runat="server" Text="Approve"
                                OnClick="btnApprove_Click" />
                        </td>
                        <td class="ms-separator">&nbsp;
                        </td>
                        <td class="ms-toolbar" nowrap>
                            <asp:Button class="ms-ButtonHeightWidth120" ID="btnRequestChange" runat="server"
                                Text="Request Change" OnClick="btnRequestChange_Click"  ClientIDMode="Static" 
                                OnClientClick="return clientFunction()" />
                        </td>
                        <td class="ms-separator">&nbsp;
                        </td>
                        <td class="ms-toolbar" nowrap>
                            <asp:Button class="ms-ButtonHeightWidth120" ID="btnReject" runat="server" Text="Reject"
                                OnClick="btnReject_Click" ClientIDMode="Static" />
                        </td>
                        <td class="ms-separator">&nbsp;
                        </td>
                        <td class="ms-toolbar" nowrap>
                            <asp:Button class="ms-ButtonHeightWidth" ID="btnClose" runat="server" Text="Close"
                                OnClick="btnClose_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <script type="text/javascript" src="ClientStyles/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="ClientStyles/jquery-ui.min.js"></script>
    <script type="text/javascript">
        function ShowHideCyclicIntervalRow() {
            if (document.getElementById('ctl00_PlaceHolderMain_rblCyclic_2').getAttribute('CHECKED')) {
                ctl00_PlaceHolderMain_ControlMRow6.style["display"] = "none";
                document.getElementById('ctl00_PlaceHolderMain_txtCyclicRestartIntervalValidator').setAttribute('enabled', '0');
            }
            else {
                ctl00_PlaceHolderMain_ControlMRow6.style["display"] = "block";
                document.getElementById('ctl00_PlaceHolderMain_txtCyclicRestartIntervalValidator').setAttribute('enabled', '1');
            }
        } 

        $(document).ready(function () {
            $('#btnRequestChange').click(function () {
                if ($('#txtComment').val().length == " ") {
                    alert("Comment is Required");
                    return false;
                }
                return true;
            });
        });

        $(document).ready(function () {
            $('#btnReject').click(function () {
                if ($('#txtComment').val().length == " ") {
                    alert("Comment is Required");
                    return false;
                }
                return true;
            });
        });
         

    </script>

</asp:Content>

