﻿using System;
using System.Data;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using IS_ESM_SERVICE_REQUESTS.Code;
using IS_ESM_SERVICE_REQUESTS.UtilityCode;
using Microsoft.SharePoint.Search.Internal.Protocols.PeopleSoapProxy;

namespace IS_ESM_SERVICE_REQUESTS.Layouts.IS_ESM_SERVICE_REQUESTS
{
	public partial class NewForm : LayoutsPageBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				LoadLocation();
				MonitoringRow22.Visible = false;
				MonitoringRow24.Visible = false;
				ControlMRow1.Visible = false;
				ControlMRow2.Visible = false;
				ControlMRow3.Visible = false;
				ControlMRow4.Visible = false;
				ControlMRow6.Visible = false;
				ControlMRow7.Visible = false;
				ControlMRow8.Visible = false;
				ControlMRow9.Visible = false;
				ControlMRow10.Visible = false;
				ControlMRow11.Visible = false;
				ControlMRow12.Visible = false;
				ControlMRow13.Visible = false;

				ControlMRow14.Visible = false;
				ControlMRow15.Visible = false;
				ControlMRow16.Visible = false;
				ControlMRow17.Visible = false;
				ControlMRow18.Visible = false;
				ControlMRow19.Visible = false;
				ControlMRow20.Visible = false;
				ControlMRow21.Visible = false;
				ControlMRow22.Visible = false;
				ControlMRow23.Visible = false;
				ControlMRow24.Visible = false;

                dtcDateRequired.MinDate = DateTime.Now;
                //dtcDateRequired.MinDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            }
        }

		protected void _btnCancel_Click(object sender, EventArgs e)
		{
			Response.Redirect(Web.Url + "/_layouts/EsmServiceRequest/NewForm.aspx");
		}
		private void LoadLocation()
		{
			ddlOfficeLocation.DataSource = this.Web.Lists["Locations"].Items.GetDataTable();
			ddlOfficeLocation.DataValueField = "ID";
			ddlOfficeLocation.DataTextField = "Title";
			ddlOfficeLocation.DataBind();
			//ddlOfficeLocation.Items.Insert(0, new ListItem("", ""));
		}
		protected void _btnOK_Click(object sender, EventArgs e)
		{
			try
			{                
				using (SPSite site = new SPSite(SPContext.Current.Web.Url))
				{
					using (SPWeb web = site.OpenWeb())
					{
                        SPFieldUserValue checkUser = GetPeopleFromPickerControl(peLineManager, web);
                        if (checkUser != null) {
                            Web.AllowUnsafeUpdates = true;
                            SPList list = web.Lists["IS_ESM_Service_Requests"];
                            SPListItem item = list.Items.Add();
                            item["Title"] = "IS_ESM_SERVICE_REQUESTS" + " " + DateTime.Now;
                            item["Telephone No"] = txtTelephoneNo.Text;
                            //item["Request Type"] = "Server or Application Monitoring";
                            item["Request Type"] = rblRequestType.SelectedItem.Value;
                            item["Designation"] = txtDesignation.Text;
                            item["Line Manager"] = Helper.GetUserValue(peLineManager, web);
                            item["Remedy Change Number"] = txtChangeNumber.Text;
                            item["Date Required"] = Convert.ToDateTime((dtcDateRequired.SelectedDate).ToString());
                            item["Server Name"] = txtServerName.Text;
                            item["IP Address"] = txtIPAddress.Text;
                            item["Is DMZ Server"] = rblIsDMZServer.SelectedItem.Text;
                            item["Operating System"] = rblOperatingSystem.SelectedItem.Text;
                            item["Server Class"] = rblServerClass.SelectedItem.Text;
                            item["Oracle Installed"] = rblOracleInstalled.SelectedItem.Text;
                            item["MS Exchange Installed"] = rblMSExchangeInstalled.SelectedItem.Text;
                            item["MSSQL Installed"] = rblMSSQLInstalled.SelectedItem.Text;
                            item["Other Applications"] = SPHandler.RemoveHTMLTags(txtOtherApplications.Text);
                            item["Log File"] = txtLogFile.Text;
                            item["Search String / Monitoring Condition Note"] = SPHandler.RemoveHTMLTags(txtSearchString_MonitoringConditionNote.Text);
                            item["Processes / Services"] = SPHandler.RemoveHTMLTags(txtProcesses_Services.Text);
                            item["Command and User Name"] = SPHandler.RemoveHTMLTags(txtCommandAndUserName.Text);
                            item["Script Name and Path"] = txtScriptNameAndPath.Text;
                            item["Job Description"] = txtJobDescription.Text;
                            item["Script Log File Path"] = txtScriptLogFilePath.Text;

                            item["Cyclic"] = rblCyclic.SelectedItem.Text;
                            item["Frequency"] = txtFrequency.Text;
                            item["Condition"] = txtCondition.Text;
                            item["Test Server Name"] = txtTestServerName.Text;
                            item["Production Server Name"] = txtProductionServerName.Text;
                            item["Is The Server Behind Firewall"] = rblIsTheServerBehindFirewall.SelectedItem.Text;
                            item["Mailing List Used For Notification"] = Helper.GetUserValue(peMailingListUsedForNotification, web);
                            item["Remedy Group Used For Failure Notification"] = txtRemedyGroup.Text;
                            item["OfficeLocation"] = ddlOfficeLocation.SelectedItem.Text;
                            item["Application Name"] = txtApplicationName.Text;
                            item["Global Zone"] = rblGlobalZone.SelectedItem.Text;
                            //Other added Columns
                            item["Other Operating System"] = txtOtherOperatingSystem.Text;
                            item["Cyclic Restart Interval"] = txtCyclicRestartInterval.Text;
                            item["Is The Source Server Behind Firewall"] = rblSourceServerBehind.SelectedItem.Text;
                            item["Is The Destination Server Behind Firewall"] = rblDestinationServerBehind.SelectedItem.Text;
                            item["Source Server Name"] = txtSourceServerName.Text;
                            item["Source Server Path"] = txtSourceServerPath.Text;
                            item["File Naming Pattern"] = txtFileNamingPattern.Text;
                            item["Destination Server Name"] = txtDestinationServerName.Text;
                            item["Destination Server Path"] = txtDestinationServerPath.Text;
                            item["Source Username"] = txtSourceUsername.Text;
                            item["Destination Username"] = txtDestinationUsername.Text;
                            item["File Name and Path"] = txtFileNameAndPath.Text;
                            item["File Reader"] = txtUserThatWillRead.Text;
                            item.Update();
                            Web.AllowUnsafeUpdates = false;
                            Response.Redirect(SPContext.Current.Web.Url);
                            //Response.Redirect(SPContext.Current.Web.Url + "/_layouts/EsmServiceRequest/NewForm.aspx");
                            //Response.Redirect(SPContext.Current.Web.Url + "/_layouts/EsmServiceRequest/NewForm.aspx");
                            //Response.Redirect("
                            //https
                            //://sharepoint.mtnnigeria.net/sites/InformationSystems#/playlist1/local1");
                            //MessagePrompt.ShowMessage(this, "Request Submitted Successfully!!!", URL);
                        }
                        else {
                            spanLineManager.Visible = true;
                        }
                                                
					}
				}
			}
			catch (Exception ex)
			{
                File.WriteAllText(@"C:\Temp\date.log", ex.ToString());
				ErrorHandler.LogToWindows(ex, "ISESM_NewRequest_SubmitLog");
			}
		}

        public static SPFieldUserValue GetPeopleFromPickerControl(PeopleEditor people, SPWeb web)
        {
            SPFieldUserValue value = null;
            if (people.ResolvedEntities.Count > 0)
            {
                for (int i = 0; i < people.ResolvedEntities.Count; i++)
                {
                    try
                    {
                        PickerEntity user = (PickerEntity)people.ResolvedEntities[i];

                        switch ((string)user.EntityData["PrincipalType"])
                        {
                            case "User":
                                SPUser webUser = web.EnsureUser(user.Key);
                                value = new SPFieldUserValue(web, webUser.ID, webUser.Name);
                                break;

                            case "SharePointGroup":
                                SPGroup siteGroup = web.SiteGroups[user.EntityData["AccountName"].ToString()];
                                value = new SPFieldUserValue(web, siteGroup.ID, siteGroup.Name);
                                break;
                            default:
                                SPUser spUser = web.EnsureUser(people.Accounts[i].ToString());
                                value = new SPFieldUserValue(web, spUser.ID, spUser.Name);
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        // log or do something
                        SPUser spUser = web.EnsureUser(people.Accounts[i].ToString());
                        value = new SPFieldUserValue(web, spUser.ID, spUser.Name);
                    }
                }
            }
            return value;
        }

        protected void rblControlMselected(object sender, EventArgs e)
		{
			
			if (rblRequestType.SelectedItem.Text == "Control-M Job")
			{

				MonitoringRow1.Visible = false;
				MonitoringRow2.Visible = false;
				MonitoringRow3.Visible = false;
				MonitoringRow4.Visible = false;
				MonitoringRow5.Visible = false;
				MonitoringRow6.Visible = false;
				MonitoringRow7.Visible = false;
				MonitoringRow8.Visible = false;
				MonitoringRow9.Visible = false;
				MonitoringRow10.Visible = false;
				MonitoringRow11.Visible = false;
				MonitoringRow12.Visible = false;
				MonitoringRow13.Visible = false;
				MonitoringRow20.Visible = false;
				MonitoringRow21.Visible = false;
				MonitoringRow22.Visible = true;
				MonitoringRow24.Visible = false;
			}
			else
			{
				MonitoringRow1.Visible = true;
				MonitoringRow2.Visible = true;
				MonitoringRow3.Visible = true;
				MonitoringRow4.Visible = true;
				MonitoringRow5.Visible = true;
				MonitoringRow6.Visible = true;
				MonitoringRow7.Visible = true;
				MonitoringRow8.Visible = true;
				MonitoringRow9.Visible = true;
				MonitoringRow10.Visible = true;
				MonitoringRow11.Visible = true;
				MonitoringRow12.Visible = true;
				MonitoringRow13.Visible = true;
				MonitoringRow20.Visible = true;
				MonitoringRow21.Visible = true;
				MonitoringRow22.Visible = false;
				MonitoringRow24.Visible = false;


				ControlMRow4.Visible = false;
				ControlMRow6.Visible = false;
				ControlMRow7.Visible = false;
				ControlMRow8.Visible = false;
				ControlMRow9.Visible = false;
				ControlMRow10.Visible = false;
				ControlMRow11.Visible = false;
				ControlMRow12.Visible = false;
				ControlMRow13.Visible = false;
				ControlMRow14.Visible = false;
				ControlMRow15.Visible = false;
				ControlMRow16.Visible = false;
				ControlMRow17.Visible = false;
				ControlMRow18.Visible = false;
				ControlMRow19.Visible = false;
				ControlMRow20.Visible = false;
				ControlMRow21.Visible = false;
				ControlMRow22.Visible = false;
				ControlMRow23.Visible = false;
				ControlMRow24.Visible = false;			

			}
		}
		protected void rblControlMOption(object sender, EventArgs e)
		{

			if (rblControlOptions.SelectedItem.Text == "Script")
			{

				MonitoringRow1.Visible = false;
				MonitoringRow2.Visible = false;
				MonitoringRow3.Visible = false;
				MonitoringRow4.Visible = false;
				MonitoringRow5.Visible = false;
				MonitoringRow6.Visible = false;
				MonitoringRow7.Visible = false;
				MonitoringRow8.Visible = false;
				MonitoringRow9.Visible = false;
				MonitoringRow10.Visible = false;
				MonitoringRow11.Visible = false;
				MonitoringRow12.Visible = false;
				MonitoringRow13.Visible = false;
				MonitoringRow20.Visible = false;
				MonitoringRow21.Visible = false;
				MonitoringRow24.Visible = false;
				//MonitoringRow22.Visible = true;
				ControlMRow1.Visible = true;
				ControlMRow2.Visible = true;
				ControlMRow3.Visible = true;
				ControlMRow4.Visible = true;
				ControlMRow6.Visible = false;
				ControlMRow7.Visible = true;
				ControlMRow8.Visible = true;
				ControlMRow9.Visible = true;
				ControlMRow10.Visible = true;
				ControlMRow11.Visible = true;
				ControlMRow12.Visible = true;
				ControlMRow13.Visible = true;
			}
			else if (rblControlOptions.SelectedItem.Text == "File Transfer")
			{
				MonitoringRow1.Visible = false;
				MonitoringRow2.Visible = false;
				MonitoringRow3.Visible = false;
				MonitoringRow4.Visible = false;
				MonitoringRow5.Visible = false;
				MonitoringRow6.Visible = false;
				MonitoringRow7.Visible = false;
				MonitoringRow8.Visible = false;
				MonitoringRow9.Visible = false;
				MonitoringRow10.Visible = false;
				MonitoringRow11.Visible = false;
				MonitoringRow12.Visible = false;
				MonitoringRow13.Visible = false;
				MonitoringRow20.Visible = false;
				MonitoringRow21.Visible = false;
				MonitoringRow24.Visible = false;
				//MonitoringRow22.Visible = true;
				ControlMRow1.Visible = false;
				ControlMRow2.Visible = false;
				ControlMRow3.Visible = false;
				ControlMRow4.Visible = true;
				ControlMRow6.Visible = false;
				ControlMRow7.Visible = true;
				ControlMRow8.Visible = false;
				ControlMRow9.Visible = true;
				ControlMRow10.Visible = true;
				ControlMRow11.Visible = false;
				ControlMRow12.Visible = true;
				ControlMRow13.Visible = true;
				ControlMRow14.Visible = true;
				ControlMRow15.Visible = true;
				ControlMRow16.Visible = true;
				ControlMRow17.Visible = true;
				ControlMRow18.Visible = true;
				ControlMRow19.Visible = true;
				ControlMRow20.Visible = true;
				ControlMRow21.Visible = true;
				ControlMRow22.Visible = true;
			}
			else if (rblControlOptions.SelectedItem.Text == "File Watcher")
			{
				MonitoringRow1.Visible = false;
				MonitoringRow2.Visible = false;
				MonitoringRow3.Visible = false;
				MonitoringRow4.Visible = false;
				MonitoringRow5.Visible = false;
				MonitoringRow6.Visible = false;
				MonitoringRow7.Visible = false;
				MonitoringRow8.Visible = false;
				MonitoringRow9.Visible = false;
				MonitoringRow10.Visible = false;
				MonitoringRow11.Visible = false;
				MonitoringRow12.Visible = false;
				MonitoringRow13.Visible = false;
				MonitoringRow20.Visible = false;
				MonitoringRow21.Visible = false;
				MonitoringRow24.Visible = false;
				//MonitoringRow22.Visible = true;
				ControlMRow4.Visible = true;
				ControlMRow6.Visible = false;
				ControlMRow7.Visible = true;
				ControlMRow8.Visible = true;
				ControlMRow9.Visible = true;
				ControlMRow10.Visible = true;
				ControlMRow11.Visible = true;
				ControlMRow12.Visible = true;
				ControlMRow13.Visible = true;
				ControlMRow23.Visible = false;
				ControlMRow24.Visible = false;
				ControlMRow14.Visible = false;
				ControlMRow15.Visible = false;
				ControlMRow16.Visible = false;
				ControlMRow17.Visible = false;
				ControlMRow18.Visible = false;
				ControlMRow19.Visible = false;
				ControlMRow20.Visible = false;
				ControlMRow21.Visible = false;
				ControlMRow22.Visible = false;
				ControlMRow23.Visible = true;
				ControlMRow24.Visible = true;

			}
		}
		protected void rblOtherOperatingSystem(object sender, EventArgs e)
		{
			if (rblOperatingSystem.SelectedItem.Text == "Others")
			{
				MonitoringRow24.Visible = true;
			}
			else
			{
				MonitoringRow24.Visible = false;
			}
		}
		protected void rblCyclicSelected(object sender, EventArgs e)
		{
			if (rblCyclic.SelectedItem.Text == "Yes (From Start)" || rblCyclic.SelectedItem.Text == "Yes (From End)")
			{
				ControlMRow6.Visible = true;
			}
			else
			{
				ControlMRow6.Visible = false;
			}
		}
	}
}
