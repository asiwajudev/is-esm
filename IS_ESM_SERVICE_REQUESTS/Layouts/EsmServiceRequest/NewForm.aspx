﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewForm.aspx.cs" Inherits="IS_ESM_SERVICE_REQUESTS.Layouts.IS_ESM_SERVICE_REQUESTS.NewForm" DynamicMasterPageFile="~masterurl/default.master" %>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <link href="ClientStyles/Common.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
    <SharePoint:ListFormPageTitle runat="server" />
</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
    <SharePoint:ListProperty Property="LinkTitle" runat="server" ID="ID_LinkTitle" />
    : New Request
</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderPageImage" runat="server">
    <img src="/_layouts/images/blank.gif" width="1" height="1" alt="">
</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <table bordercolor="#008000" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td style="width: 100%" valign="top" align="left">
                <table class="ms-formtoolbar" id="tblTopButtons" cellspacing="0" cellpadding="2"
                    width="100%" border="0">
                    <tr>
                        <td class="ms-toolbar" nowrap width="99%">
                            <img height="18" alt="" src="/_layouts/images/blank.gif" width="1" />
                        </td>
                        <td class="ms-toolbar" nowrap>
                            <asp:Button class="ms-ButtonHeightWidth" ID="_btnOKTop" runat="server" Text="OK"
                                OnClick="_btnOK_Click" />
                        </td>
                        <%--<td class="ms-toolbar" nowrap>
                            <asp:Button class="ms-ButtonHeightWidth" ID="_btnOKTop" CausesValidation="false"
                                runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                        </td>--%>
                        <td class="ms-separator">&nbsp;
                        </td>
                        <td class="ms-toolbar" nowrap>
                            <asp:Button class="ms-ButtonHeightWidth" ID="_btnCancelTop" CausesValidation="false"
                                runat="server" Text="Cancel" OnClick="_btnCancel_Click" />
                        </td>
                    </tr>
                </table>
                <SharePoint:FormToolBar runat="server" ID="aaa" ControlMode="New">
                </SharePoint:FormToolBar>
                <table class="ms-formtable" style="margin-top: 8px" cellspacing="0" cellpadding="0"
                    width="100%" border="0">
                    <tr>
                        <td class="td_Label" valign="top" width="auto" colspan="2" style="font-weight: bold">
                            <div class="div_Header">
                                New Request
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Request Type<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblRequestType" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <asp:RadioButtonList ID="rblRequestType" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="rblControlMselected">
                                <asp:ListItem Value="Server or Application Monitoring" Selected="True">Server or Application Monitoring</asp:ListItem>
                                <asp:ListItem Value="Control-M Job">Control-M Job</asp:ListItem>
                                <%--<asp:ListItem Selected="True" Value="Server or Application Monitoring" onclick="javascript:ShowMonitoring();">Server or Application Monitoring</asp:ListItem>
                               <asp:ListItem Value="Server or Application Monitoring" OnClick="javascript:ShowMonitoring();">Control-M Job</asp:ListItem>--%>
                                <%--<asp:ListItem Value="Server or Application Monitoring" onclick="javascript:ShowControlM();">Control-M Job</asp:ListItem>--%>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Telephone No<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblTelephoneNo" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <SharePoint:InputFormTextBox ID="txtTelephoneNo" RichText="false" TextMode="Singleline"
                                runat="server" MaxLength="11" title="Telephone No" class="ms-long" />
                            <SharePoint:InputFormRequiredFieldValidator ID="InputFormRequiredFieldValidator2"
                                class="ms-error" ControlToValidate="txtTelephoneNo" IsRequiredField="true" Text="Error" runat="server"
                                ErrorMessage="You must specify a value for this required field." EnableClientScript="true"
                                Display="Dynamic">
                            </SharePoint:InputFormRequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtTelephoneNo"
                                ErrorMessage="Please Enter a Valid Telephone No" IsRequiredField="true" class="ms-error" Display="Dynamic"
                                ValidationExpression="^\d{11}$" SetFocusOnError="true"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Office Location<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblOfficeLocation" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <asp:DropDownList class="ms-RadioText" ID="ddlOfficeLocation" runat="server">
                            </asp:DropDownList>
                            <SharePoint:InputFormRequiredFieldValidator ID="InputFormRequiredFieldValidator1"
                                class="ms-error" ControlToValidate="ddlOfficeLocation" Text="Error" runat="server"
                                ErrorMessage="You must specify a value for this required field." EnableClientScript="true"
                                Display="Dynamic">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Designation</nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblDesignation" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <SharePoint:InputFormTextBox ID="txtDesignation" RichText="false" TextMode="Singleline"
                                runat="server" MaxLength="32" title="Designation" class="ms-long" />
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Line Manager<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblLineManager" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <SharePoint:PeopleEditor ID="peLineManager" runat="server" Rows="1" MaximumEntities="1"
                                MultiSelect="false" RequiredField="true" PlaceButtonsUnderEntityEditor="false" ValidatorEnabled="true" 
                                PrincipalSource="All" SelectionSet="User" EnableViewState="true" AllowEmpty="false" />
                            <%--<SharePoint:InputFormRequiredFieldValidator ID="rfvAssegnatario" runat="server" ControlToValidate="peLineManager" 
                              ErrorMessage="Campo obbligatorio" Display="Dynamic" BreakBefore="false" />--%>
                            <%--<asp:CustomValidator ID="CustomValidator1" EnableClientScript="true" runat="server" 
                                ValidateEmptyText="false" ControlToValidate="peLineManager" ErrorMessage="CustomValidator">
                            </asp:CustomValidator>--%>
                           <%-- <asp:CompareValidator ID="CompareValidator1" Type="String"
                                runat="server" ErrorMessage="Line Manager Required" ForeColor="Red" ControlToCompare="peLineManager"></asp:CompareValidator>--%>
                            <%-- <SharePoint:InputFormRequiredFieldValidator ID="InputFormRequiredFieldValidator5"
                                class="ms-error" ControlToValidate="peLineManager" Text="Error" runat="server"
                                ErrorMessage="You must specify a value for this required field." EnableClientScript="true"
                                Display="Dynamic">
                            </SharePoint:InputFormRequiredFieldValidator>--%>
                               <%--<asp:CustomValidator ID="rfvProjectManager" ForeColor="Red" runat="server"
                                ErrorMessage="Required" IsRequiredField="true" ControlToValidate="peLineManager" >  
                            </asp:CustomValidator>--%>
                              <span style="display: none" class="ms-error" visible="false" id="spanLineManager" runat="server">You
                                must specify a value for this required field.</span>
            
                            </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Remedy Incident Number</nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblChangeNumber" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <SharePoint:InputFormTextBox ID="txtChangeNumber" RichText="false" TextMode="Singleline"
                                runat="server" MaxLength="8" title="Change Number" IsRequiredField="true" class="ms-long" />
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Date Required<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblDateRequired" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <SharePoint:DateTimeControl ID="dtcDateRequired" LocaleId="2057" ToolTip="mm/dd/yyyy"
                                runat="server" IsRequiredField="true" DateOnly="true" >                               
                            </SharePoint:DateTimeControl>
                        </td>
                    </tr>
                    <tr id="MonitoringRow22" runat="server">
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblControlOptions" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <asp:RadioButtonList ID="rblControlOptions" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="rblControlMOption">
                                <asp:ListItem Value="Script">Script</asp:ListItem>
                                <asp:ListItem Value="File Transfer">File Transfer</asp:ListItem>
                                <asp:ListItem Value="File Watcher">File Watcher</asp:ListItem>
                                <%--<asp:ListItem Selected="True" Value="Server or Application Monitoring" onclick="javascript:ShowMonitoring();">Server or Application Monitoring</asp:ListItem>
                               <asp:ListItem Value="Server or Application Monitoring" OnClick="javascript:ShowMonitoring();">Control-M Job</asp:ListItem>--%>
                                <%--<asp:ListItem Value="Server or Application Monitoring" onclick="javascript:ShowControlM();">Control-M Job</asp:ListItem>--%>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr id="MonitoringRow1" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Server Name<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblServerName" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <SharePoint:InputFormTextBox ID="txtServerName" RichText="false" TextMode="Singleline"
                                runat="server" MaxLength="32" title="Server Name" class="ms-long" />
                            <SharePoint:InputFormRequiredFieldValidator ID="txtServerNameValidator" class="ms-error"
                                ControlToValidate="txtServerName" Text="Error" runat="server" ErrorMessage="You must specify a value for this required field."
                                EnableClientScript="true" Display="Dynamic">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </td>
                    </tr>
                    <tr id="MonitoringRow2" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>IP Address<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblIPAddress" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <SharePoint:InputFormTextBox ID="txtIPAddress" RichText="false" TextMode="Singleline"
                                runat="server" MaxLength="15" title="IP Address" class="ms-long" />
                            <SharePoint:InputFormRequiredFieldValidator ID="txtIPAddressValidator" class="ms-error"
                                ControlToValidate="txtIPAddress" Text="Error" runat="server" ErrorMessage="You must specify a value for this required field."
                                EnableClientScript="true" Display="Dynamic">
                            </SharePoint:InputFormRequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtIPAddress"
                                ErrorMessage="Please Enter a Valid IP Address" class="ms-error" Display="Dynamic"
                                ValidationExpression="^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$" SetFocusOnError="true"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr id="MonitoringRow3" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Is DMZ Server<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblIsDMZServer" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <asp:RadioButtonList ID="rblIsDMZServer" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="True">Yes</asp:ListItem>
                                <asp:ListItem Selected="True" Value="False">No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr id="MonitoringRow4" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Operating System<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblOperatingSystem" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <asp:RadioButtonList ID="rblOperatingSystem" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="rblOtherOperatingSystem">
                                <asp:ListItem Selected="True" Value="Windows">Windows</asp:ListItem>
                                <asp:ListItem Value="Unix">Unix</asp:ListItem>
                                <asp:ListItem Value="Linux">Linux</asp:ListItem>
                                <asp:ListItem Value="Others">Others</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr id="MonitoringRow24" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Other Operating System<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblOtherOperatingSystem" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <SharePoint:InputFormTextBox ID="txtOtherOperatingSystem" RichText="false" TextMode="Singleline"
                                runat="server" MaxLength="32" title="Server Name" class="ms-long" />
                            <SharePoint:InputFormRequiredFieldValidator ID="txtOtherOperatingSystemValidator" class="ms-error"
                                ControlToValidate="txtServerName" Text="Error" runat="server" ErrorMessage="You must specify a value for this required field."
                                EnableClientScript="true" Display="Dynamic">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </td>
                    </tr>

                    <tr id="MonitoringRow5" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Server Class<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblServerClass" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <asp:RadioButtonList ID="rblServerClass" runat="server" RepeatDirection="Vertical">
                                <asp:ListItem Value="Essential Business Support" Selected="True">Essential Business Support</asp:ListItem>
                                <asp:ListItem Value="Mission Critical">Mission Critical</asp:ListItem>
                                <asp:ListItem Value="Service Enabler">Service Enabler</asp:ListItem>
                                <asp:ListItem Value="Subscriber Critical">Subscriber Critical</asp:ListItem>
                                <asp:ListItem Value="Core Services">Core Services</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr id="MonitoringRow20" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Application Name <SPAN class="ms-formvalidation"> *</SPAN><br> State the Application that the Server host</nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblApplicationName" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <SharePoint:InputFormTextBox ID="txtApplicationName" RichText="false" TextMode="Singleline"
                                runat="server" MaxLength="32" title="Server Name" class="ms-long" />
                            <SharePoint:InputFormRequiredFieldValidator ID="InputFormRequiredFieldValidator3" class="ms-error"
                                ControlToValidate="txtApplicationName" Text="Error" runat="server" ErrorMessage="You must specify a value for this required field."
                                EnableClientScript="true" Display="Dynamic">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </td>
                    </tr>
                    <tr id="MonitoringRow21" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Global_zone/Physical Server<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblGlobalZone" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <asp:RadioButtonList ID="rblGlobalZone" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="True">Yes</asp:ListItem>
                                <asp:ListItem Value="False" Selected="True">No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr id="MonitoringRow6" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Oracle Installed<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblOracleInstalled" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <asp:RadioButtonList ID="rblOracleInstalled" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="True">Yes</asp:ListItem>
                                <asp:ListItem Value="False" Selected="True">No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr id="MonitoringRow7" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>MS Exchange Installed<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblMSExchangeInstalled" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <asp:RadioButtonList ID="rblMSExchangeInstalled" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="True">Yes</asp:ListItem>
                                <asp:ListItem Value="False" Selected="True">No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr id="MonitoringRow8" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>MSSQL Installed<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblMSSQLInstalled" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <asp:RadioButtonList ID="rblMSSQLInstalled" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="True">Yes</asp:ListItem>
                                <asp:ListItem Value="False" Selected="True">No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr id="MonitoringRow9" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Other Applications</nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblOtherApplications" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <SharePoint:InputFormTextBox ID="txtOtherApplications" RichText="false" TextMode="MultiLine"
                                runat="server" Rows="6" title="Other Applications" class="ms-long" />
                        </td>
                    </tr>
                    <tr id="MonitoringRow10" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Log File</nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblLogFile" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <SharePoint:InputFormTextBox ID="txtLogFile" RichText="false" TextMode="Singleline"
                                runat="server" MaxLength="255" title="Log File" class="ms-long" />
                        </td>
                    </tr>
                    <tr id="MonitoringRow11" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">Search String / Monitoring Condition Note
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblSearchString_MonitoringConditionNote" Text="&nbsp;" runat="server"
                                Visible="false"></asp:Label>
                            <SharePoint:InputFormTextBox ID="txtSearchString_MonitoringConditionNote" RichText="false"
                                TextMode="MultiLine" runat="server" Rows="4" title="Other Applications"
                                class="ms-long" />
                        </td>
                    </tr>
                    <tr id="MonitoringRow12" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Processes / Services</nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblProcesses_Services" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <SharePoint:InputFormTextBox ID="txtProcesses_Services" RichText="false" TextMode="MultiLine"
                                runat="server" Rows="4" title="Processes / Services" class="ms-long" />
                        </td>
                    </tr>
                    <tr id="MonitoringRow13" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Command and User Name</nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblCommandAndUserName" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <SharePoint:InputFormTextBox ID="txtCommandAndUserName" RichText="false" TextMode="MultiLine"
                                runat="server" Rows="4" title="Command and User Name " class="ms-long" />
                        </td>

                    </tr>
                    <tr id="ControlMRow1" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Script Name and Path<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblScriptNameAndPath" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <SharePoint:InputFormTextBox ID="txtScriptNameAndPath" RichText="false" TextMode="Singleline"
                                runat="server" MaxLength="255" title="Script Name and Path" class="ms-long" />
                            <SharePoint:InputFormRequiredFieldValidator ID="txtScriptNameAndPathValidator" class="ms-error"
                                ControlToValidate="txtScriptNameAndPath" Text="Error" runat="server" ErrorMessage="You must specify a value for this required field."
                                EnableClientScript="true" Display="Dynamic" Enabled="false">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </td>
                    </tr>
                    <tr id="ControlMRow2" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Job Description<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblJobDescription" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <SharePoint:InputFormTextBox ID="txtJobDescription" RichText="false" TextMode="Singleline"
                                runat="server" MaxLength="255" title="Job Description" class="ms-long" />
                            <SharePoint:InputFormRequiredFieldValidator ID="txtJobDescriptionValidator" class="ms-error"
                                ControlToValidate="txtJobDescription" Text="Error" runat="server" ErrorMessage="You must specify a value for this required field."
                                EnableClientScript="true" Display="Dynamic" Enabled="false">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </td>
                    </tr>
                    <tr id="ControlMRow3" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Script Log File Path</nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblScriptLogFilePath" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <SharePoint:InputFormTextBox ID="txtScriptLogFilePath" RichText="false" TextMode="Singleline"
                                runat="server" MaxLength="255" title="Script Log File Path" class="ms-long" />
                        </td>
                    </tr>
                    <tr id="ControlMRow4" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Cyclic<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblCyclic" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <asp:RadioButtonList ID="rblCyclic" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="rblCyclicSelected">
                                <asp:ListItem Value="Yes (From Start)">Yes (From Start)</asp:ListItem>
                                <asp:ListItem Value="Yes (From End)">Yes (From End)</asp:ListItem>
                                <asp:ListItem Selected="True" Value="No">No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr id="ControlMRow6" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Cyclic Restart Interval<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblCyclicRestartInterval" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <SharePoint:InputFormTextBox ID="txtCyclicRestartInterval" RichText="false" TextMode="Singleline"
                                runat="server" MaxLength="255" title="Cyclic Restart Interval" class="ms-long" />
                            <SharePoint:InputFormRequiredFieldValidator ID="txtCyclicRestartIntervalValidator"
                                class="ms-error" ControlToValidate="txtCyclicRestartInterval" Text="Error" runat="server"
                                ErrorMessage="You must specify a value for this required field." EnableClientScript="true"
                                Display="Dynamic" Enabled="false">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </td>
                    </tr>
                    <tr id="ControlMRow7" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Frequency<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblFrequency" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <SharePoint:InputFormTextBox ID="txtFrequency" RichText="false" TextMode="Singleline"
                                runat="server" MaxLength="255" title="Frequency" class="ms-long" />
                            <SharePoint:InputFormRequiredFieldValidator ID="txtFrequencyValidator" class="ms-error"
                                ControlToValidate="txtFrequency" Text="Error" runat="server" ErrorMessage="You must specify a value for this required field."
                                EnableClientScript="true" Display="Dynamic" Enabled="false">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </td>
                    </tr>
                    <tr id="ControlMRow8" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Condition<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblCondition" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <SharePoint:InputFormTextBox ID="txtCondition" RichText="false" TextMode="Singleline"
                                runat="server" MaxLength="255" title="Condition" class="ms-long" />
                            <SharePoint:InputFormRequiredFieldValidator ID="txtConditionValidator" class="ms-error"
                                ControlToValidate="txtCondition" Text="Error" runat="server" ErrorMessage="You must specify a value for this required field."
                                EnableClientScript="true" Display="Dynamic" Enabled="false">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </td>
                    </tr>
                    <tr id="ControlMRow9" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Test Server Name</nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblTestServerName" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <SharePoint:InputFormTextBox ID="txtTestServerName" RichText="false" TextMode="Singleline"
                                runat="server" MaxLength="255" title="Test Server Name" class="ms-long" />
                        </td>
                    </tr>
                    <tr id="ControlMRow10" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Production Server Name<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblProductionServerName" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <SharePoint:InputFormTextBox ID="txtProductionServerName" RichText="false" TextMode="Singleline"
                                runat="server" MaxLength="255" title="Production Server Name" class="ms-long" />
                            <SharePoint:InputFormRequiredFieldValidator ID="txtProductionServerNameValidator"
                                class="ms-error" ControlToValidate="txtProductionServerName" Text="Error" runat="server"
                                ErrorMessage="You must specify a value for this required field." EnableClientScript="true"
                                Display="Dynamic" Enabled="false">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </td>
                    </tr>
                    <tr id="ControlMRow11" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">Is The Server Behind Firewall<span class="ms-formvalidation"> *</span>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblIsTheServerBehindFirewall" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <asp:RadioButtonList ID="rblIsTheServerBehindFirewall" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="True">Yes</asp:ListItem>
                                <asp:ListItem Selected="True" Value="False">No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>

                    <tr id="ControlMRow14" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">Is The Source Server Behind Firewall<span class="ms-formvalidation"> *</span>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblSourceServerBehind" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <asp:RadioButtonList ID="rblSourceServerBehind" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="True">Yes</asp:ListItem>
                                <asp:ListItem Selected="True" Value="False">No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr id="ControlMRow15" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">Is The Destination Server Behind Firewall<span class="ms-formvalidation"> *</span>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblDestinationServerBehind" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <asp:RadioButtonList ID="rblDestinationServerBehind" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="True">Yes</asp:ListItem>
                                <asp:ListItem Selected="True" Value="False">No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr id="ControlMRow16" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">Source Server Name<span class="ms-formvalidation"> *</span>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblSourceServerName" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <SharePoint:InputFormTextBox ID="txtSourceServerName" RichText="false" TextMode="Singleline"
                                runat="server" MaxLength="128" title="Frequency" class="ms-long" />
                            <SharePoint:InputFormRequiredFieldValidator ID="txtSourceServerNameValidator" class="ms-error"
                                ControlToValidate="txtRemedyGroup" Text="Error" runat="server" ErrorMessage="You must specify a value for this required field."
                                EnableClientScript="true" Display="Dynamic" Enabled="false">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </td>
                    </tr>
                    <tr id="ControlMRow17" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">Source Server Path<span class="ms-formvalidation"> *</span>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblSourceServerPath" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <SharePoint:InputFormTextBox ID="txtSourceServerPath" RichText="false" TextMode="Singleline"
                                runat="server" MaxLength="128" title="Frequency" class="ms-long" />
                            <SharePoint:InputFormRequiredFieldValidator ID="txtSourceServerPathValidator" class="ms-error"
                                ControlToValidate="txtRemedyGroup" Text="Error" runat="server" ErrorMessage="You must specify a value for this required field."
                                EnableClientScript="true" Display="Dynamic" Enabled="false">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </td>
                    </tr>
                    <tr id="ControlMRow18" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">File Naming Pattern<span class="ms-formvalidation"> *</span>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblFileNamingPattern" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <SharePoint:InputFormTextBox ID="txtFileNamingPattern" RichText="false" TextMode="Singleline"
                                runat="server" MaxLength="128" title="Frequency" class="ms-long" />
                            <SharePoint:InputFormRequiredFieldValidator ID="txtFileNamingPatternValidator" class="ms-error"
                                ControlToValidate="txtRemedyGroup" Text="Error" runat="server" ErrorMessage="You must specify a value for this required field."
                                EnableClientScript="true" Display="Dynamic" Enabled="false">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </td>
                    </tr>
                    <tr id="ControlMRow19" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">Destination Server Name<span class="ms-formvalidation"> *</span>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblDestinationServerName" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <SharePoint:InputFormTextBox ID="txtDestinationServerName" RichText="false" TextMode="Singleline"
                                runat="server" MaxLength="128" title="Frequency" class="ms-long" />
                            <SharePoint:InputFormRequiredFieldValidator ID="txtDestinationServerNameValidator" class="ms-error"
                                ControlToValidate="txtRemedyGroup" Text="Error" runat="server" ErrorMessage="You must specify a value for this required field."
                                EnableClientScript="true" Display="Dynamic" Enabled="false">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </td>
                    </tr>
                    <tr id="ControlMRow20" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">Destination Server Path<span class="ms-formvalidation"> *</span>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblDestinationServerPath" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <SharePoint:InputFormTextBox ID="txtDestinationServerPath" RichText="false" TextMode="Singleline"
                                runat="server" MaxLength="128" title="Frequency" class="ms-long" />
                            <SharePoint:InputFormRequiredFieldValidator ID="lblDestinationServerPathValidator" class="ms-error"
                                ControlToValidate="txtRemedyGroup" Text="Error" runat="server" ErrorMessage="You must specify a value for this required field."
                                EnableClientScript="true" Display="Dynamic" Enabled="false">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </td>
                    </tr>
                    <tr id="ControlMRow21" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">Source Username<span class="ms-formvalidation"> *</span>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblSourceUsername" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <SharePoint:InputFormTextBox ID="txtSourceUsername" RichText="false" TextMode="Singleline"
                                runat="server" MaxLength="128" title="Frequency" class="ms-long" />
                            <SharePoint:InputFormRequiredFieldValidator ID="txtSourceUsernameValidator" class="ms-error"
                                ControlToValidate="txtRemedyGroup" Text="Error" runat="server" ErrorMessage="You must specify a value for this required field."
                                EnableClientScript="true" Display="Dynamic" Enabled="false">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </td>
                    </tr>
                    <tr id="ControlMRow22" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">Destination Username<span class="ms-formvalidation"> *</span>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblDestinationUsername" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <SharePoint:InputFormTextBox ID="txtDestinationUsername" RichText="false" TextMode="Singleline"
                                runat="server" MaxLength="128" title="Frequency" class="ms-long" />
                            <SharePoint:InputFormRequiredFieldValidator ID="txtDestinationUsernameValidator" class="ms-error"
                                ControlToValidate="txtRemedyGroup" Text="Error" runat="server" ErrorMessage="You must specify a value for this required field."
                                EnableClientScript="true" Display="Dynamic" Enabled="false">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </td>
                    </tr>
                    <tr id="ControlMRow12" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">Mailing List Used For Notification<span class="ms-formvalidation"> *</span>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblMailingListUsedForNotification" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <SharePoint:PeopleEditor ID="peMailingListUsedForNotification" runat="server" Rows="1"
                                MultiSelect="true" PlaceButtonsUnderEntityEditor="false" PrincipalSource="All"
                                SelectionSet="User" AllowEmpty="false" />
                            <span style="display: none" class="ms-error" id="spanMailingListUsedForNotification"
                                runat="server">You must specify a value for this required field.</span>
                        </td>
                    </tr>
                    <tr id="ControlMRow13" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">Remedy Group Used For Failure Notification<span class="ms-formvalidation"> *</span>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblRemedyGroup" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <SharePoint:InputFormTextBox ID="txtRemedyGroup" RichText="false" TextMode="Singleline"
                                runat="server" MaxLength="128" title="Frequency" class="ms-long" />
                            <SharePoint:InputFormRequiredFieldValidator ID="txtRemedyGroupValidator" class="ms-error"
                                ControlToValidate="txtRemedyGroup" Text="Error" runat="server" ErrorMessage="You must specify a value for this required field."
                                EnableClientScript="true" Display="Dynamic" Enabled="false">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </td>
                    </tr>
                    <tr id="ControlMRow23" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">File Name and Path<span class="ms-formvalidation"> *</span>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblFileNameAndPath" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <SharePoint:InputFormTextBox ID="txtFileNameAndPath" RichText="false" TextMode="Singleline"
                                runat="server" MaxLength="128" title="Frequency" class="ms-long" />
                            <SharePoint:InputFormRequiredFieldValidator ID="txtFileNameAndPathValidator" class="ms-error"
                                ControlToValidate="txtRemedyGroup" Text="Error" runat="server" ErrorMessage="You must specify a value for this required field."
                                EnableClientScript="true" Display="Dynamic" Enabled="false">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </td>
                    </tr>
                    <tr id="ControlMRow24" runat="server">
                        <td class="td_Field" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">User(that will read the file)<span class="ms-formvalidation"> *</span>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblUserThatWillRead" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                            <SharePoint:InputFormTextBox ID="txtUserThatWillRead" RichText="false" TextMode="Singleline"
                                runat="server" MaxLength="250" title="Frequency" class="ms-long" Width="90%" />
                            <SharePoint:InputFormRequiredFieldValidator ID="txtUserThatWillReadValidator" class="ms-error"
                                ControlToValidate="txtRemedyGroup" Text="Error" runat="server" ErrorMessage="You must specify a value for this required field."
                                EnableClientScript="true" Display="Dynamic" Enabled="false">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="0" width="100%">
                    <tbody>
                        <tr>
                            <td class="ms-formline">
                                <img height="1" alt="" src="/_layouts/images/blank.gif" width="1" />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table class="ms-formtoolbar" id="tblBottomButtons" cellspacing="0" cellpadding="2"
                    width="100%" border="0">
                    <tr>
                        <td class="ms-toolbar" nowrap width="99%">
                            <img height="18" alt="" src="/_layouts/images/blank.gif" width="1" />
                        </td>
                        <td class="ms-toolbar" nowrap>
                            <asp:Button class="ms-ButtonHeightWidth" ID="_btnOKBottom" runat="server" Text="OK"
                                OnClick="_btnOK_Click" />
                        </td>
                        <td class="ms-separator">&nbsp;
                        </td>
                        <%--<td class="ms-toolbar" nowrap>
                            <asp:Button class="ms-ButtonHeightWidth" ID="_btnOKBottom" CausesValidation="false"
                                runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                        </td>--%>
                        <td class="ms-toolbar" nowrap>
                            <asp:Button class="ms-ButtonHeightWidth" ID="_btnCancelBottom" CausesValidation="false"
                                runat="server" Text="Cancel" OnClick="_btnCancel_Click" />
                        </td>

                    </tr>

                </table>
            </td>
        </tr>
    </table>
    <script language="javascript" type="text/javascript">
        //Load Script:
        if (document.getElementById('ctl00_PlaceHolderMain_rblRequestType_0').getAttribute('CHECKED')) {
            //Permanent
            ShowMonitoring();
        }
        else {
            //Non Permanent
            ShowControlM();
        }

        //End of Load Script
        function ShowMonitoring() {
            ctl00_PlaceHolderMain_MonitoringRow1.style["display"] = "table-row";
            ctl00_PlaceHolderMain_MonitoringRow2.style["display"] = "table-row";
            ctl00_PlaceHolderMain_MonitoringRow3.style["display"] = "table-row";
            ctl00_PlaceHolderMain_MonitoringRow4.style["display"] = "table-row";
            ctl00_PlaceHolderMain_MonitoringRow5.style["display"] = "table-row";
            ctl00_PlaceHolderMain_MonitoringRow6.style["display"] = "table-row";
            ctl00_PlaceHolderMain_MonitoringRow7.style["display"] = "table-row";
            ctl00_PlaceHolderMain_MonitoringRow8.style["display"] = "table-row";
            ctl00_PlaceHolderMain_MonitoringRow9.style["display"] = "table-row";
            ctl00_PlaceHolderMain_MonitoringRow10.style["display"] = "table-row";
            ctl00_PlaceHolderMain_MonitoringRow11.style["display"] = "table-row";
            ctl00_PlaceHolderMain_MonitoringRow12.style["display"] = "table-row";
            ctl00_PlaceHolderMain_MonitoringRow13.style["display"] = "table-row";

            ctl00_PlaceHolderMain_ControlMRow1.style["display"] = "none";
            ctl00_PlaceHolderMain_ControlMRow2.style["display"] = "none";
            ctl00_PlaceHolderMain_ControlMRow3.style["display"] = "none";
            ctl00_PlaceHolderMain_ControlMRow4.style["display"] = "none";

            ctl00_PlaceHolderMain_ControlMRow6.style["display"] = "none";
            ctl00_PlaceHolderMain_ControlMRow7.style["display"] = "none";
            ctl00_PlaceHolderMain_ControlMRow8.style["display"] = "none";
            ctl00_PlaceHolderMain_ControlMRow9.style["display"] = "none";
            ctl00_PlaceHolderMain_ControlMRow10.style["display"] = "none";
            ctl00_PlaceHolderMain_ControlMRow11.style["display"] = "none";
            ctl00_PlaceHolderMain_ControlMRow12.style["display"] = "none";
            ctl00_PlaceHolderMain_ControlMRow13.style["display"] = "none";

            document.getElementById('ctl00_PlaceHolderMain_txtServerNameValidator').setAttribute('enabled', '1');
            document.getElementById('ctl00_PlaceHolderMain_txtIPAddressValidator').setAttribute('enabled', '1');

            document.getElementById('ctl00_PlaceHolderMain_txtScriptNameAndPathValidator').setAttribute('enabled', '0');
            document.getElementById('ctl00_PlaceHolderMain_txtJobDescriptionValidator').setAttribute('enabled', '0');
            document.getElementById('ctl00_PlaceHolderMain_txtCyclicRestartIntervalValidator').setAttribute('enabled', '0');
            document.getElementById('ctl00_PlaceHolderMain_txtFrequencyValidator').setAttribute('enabled', '0');
            document.getElementById('ctl00_PlaceHolderMain_txtConditionValidator').setAttribute('enabled', '0');
            document.getElementById('ctl00_PlaceHolderMain_txtProductionServerNameValidator').setAttribute('enabled', '0');
            document.getElementById('ctl00_PlaceHolderMain_txtRemedyGroupValidator').setAttribute('enabled', '0');

            ShowHideCyclicIntervalRow();
        }
        function ShowControlM() {
            ctl00_PlaceHolderMain_MonitoringRow1.style["display"] = "none";
            ctl00_PlaceHolderMain_MonitoringRow2.style["display"] = "none";
            ctl00_PlaceHolderMain_MonitoringRow3.style["display"] = "none";
            ctl00_PlaceHolderMain_MonitoringRow4.style["display"] = "none";
            ctl00_PlaceHolderMain_MonitoringRow5.style["display"] = "none";
            ctl00_PlaceHolderMain_MonitoringRow6.style["display"] = "none";
            ctl00_PlaceHolderMain_MonitoringRow7.style["display"] = "none";
            ctl00_PlaceHolderMain_MonitoringRow8.style["display"] = "none";
            ctl00_PlaceHolderMain_MonitoringRow9.style["display"] = "none";
            ctl00_PlaceHolderMain_MonitoringRow10.style["display"] = "none";
            ctl00_PlaceHolderMain_MonitoringRow11.style["display"] = "none";
            ctl00_PlaceHolderMain_MonitoringRow12.style["display"] = "none";
            ctl00_PlaceHolderMain_MonitoringRow13.style["display"] = "none";

            ctl00_PlaceHolderMain_ControlMRow1.style["display"] = "table-row";
            ctl00_PlaceHolderMain_ControlMRow2.style["display"] = "table-row";
            ctl00_PlaceHolderMain_ControlMRow3.style["display"] = "table-row";
            ctl00_PlaceHolderMain_ControlMRow4.style["display"] = "table-row";

            ctl00_PlaceHolderMain_ControlMRow6.style["display"] = "table-row";
            ctl00_PlaceHolderMain_ControlMRow7.style["display"] = "table-row";
            ctl00_PlaceHolderMain_ControlMRow8.style["display"] = "table-row";
            ctl00_PlaceHolderMain_ControlMRow9.style["display"] = "table-row";
            ctl00_PlaceHolderMain_ControlMRow10.style["display"] = "table-row";
            ctl00_PlaceHolderMain_ControlMRow11.style["display"] = "table-row";
            ctl00_PlaceHolderMain_ControlMRow12.style["display"] = "table-row";
            ctl00_PlaceHolderMain_ControlMRow13.style["display"] = "table-row";

            document.getElementById('ctl00_PlaceHolderMain_txtServerNameValidator').setAttribute('enabled', '0');
            document.getElementById('ctl00_PlaceHolderMain_txtIPAddressValidator').setAttribute('enabled', '0');

            document.getElementById('ctl00_PlaceHolderMain_txtScriptNameAndPathValidator').setAttribute('enabled', '1');
            document.getElementById('ctl00_PlaceHolderMain_txtJobDescriptionValidator').setAttribute('enabled', '1');
            document.getElementById('ctl00_PlaceHolderMain_txtCyclicRestartIntervalValidator').setAttribute('enabled', '1');
            document.getElementById('ctl00_PlaceHolderMain_txtFrequencyValidator').setAttribute('enabled', '1');
            document.getElementById('ctl00_PlaceHolderMain_txtConditionValidator').setAttribute('enabled', '1');
            document.getElementById('ctl00_PlaceHolderMain_txtProductionServerNameValidator').setAttribute('enabled', '1');
            document.getElementById('ctl00_PlaceHolderMain_txtRemedyGroupValidator').setAttribute('enabled', '1');

            ShowHideCyclicIntervalRow();
        }
        function ShowHideCyclicIntervalRow() {
            if (document.getElementById('ctl00_PlaceHolderMain_rblCyclic_2').getAttribute('CHECKED')) {
                ctl00_PlaceHolderMain_ControlMRow6.style["display"] = "none";
                document.getElementById('ctl00_PlaceHolderMain_txtCyclicRestartIntervalValidator').setAttribute('enabled', '0');
            }
            else {
                ctl00_PlaceHolderMain_ControlMRow6.style["display"] = "table-row";
                document.getElementById('ctl00_PlaceHolderMain_txtCyclicRestartIntervalValidator').setAttribute('enabled', '1');
            }
        } 
    </script>


</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderTitleLeftBorder" runat="server">
    <table cellpadding="0" height="100%" width="100%" cellspacing="0">
        <tr>
            <td class="ms-areaseparatorleft">
                <img src="/_layouts/images/blank.gif" width="1" height="1" alt="">
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderTitleAreaClass" runat="server">

    <script id="onetidPageTitleAreaFrameScript">
        document.getElementById("onetidPageTitleAreaFrame").className = "ms-areaseparator";
    </script>

</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderBodyAreaClass" runat="server">
    <style type="text/css">
        .ms-bodyareaframe {
            padding: 8px;
            border: none;
        }
    </style>
</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderBodyLeftBorder" runat="server">
    <div class='ms-areaseparatorleft'>
        <img src="/_layouts/images/blank.gif" width="8" height="100%" alt="">
    </div>
</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderTitleRightMargin" runat="server">
    <div class='ms-areaseparatorright'>
        <img src="/_layouts/images/blank.gif" width="8" height="100%" alt="">
    </div>
</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderBodyRightMargin" runat="server">
    <div class='ms-areaseparatorright'>
        <img src="/_layouts/images/blank.gif" width="8" height="100%" alt="">
    </div>
</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderTitleAreaSeparator" runat="server" />
