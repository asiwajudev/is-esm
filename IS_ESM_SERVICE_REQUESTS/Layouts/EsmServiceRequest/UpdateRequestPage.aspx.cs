﻿using System;
using System.IO;
using System.Web.UI.WebControls;
using IS_ESM_SERVICE_REQUESTS.Code;
using IS_ESM_SERVICE_REQUESTS.UtilityCode;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

namespace IS_ESM_SERVICE_REQUESTS.Layouts.EsmServiceRequest
{
    public partial class UpdateRequestPage : LayoutsPageBase
    { 

        protected void Page_Load(object sender, EventArgs e)
        {
            LoadLocation();
            MonitoringRow22.Visible = false;
            MonitoringRow24.Visible = false;
            ControlMRow1.Visible = false;
            ControlMRow2.Visible = false;
            ControlMRow3.Visible = false;
            ControlMRow4.Visible = false;
            ControlMRow6.Visible = false;
            ControlMRow7.Visible = false;
            ControlMRow8.Visible = false;
            ControlMRow9.Visible = false;
            ControlMRow10.Visible = false;
            ControlMRow11.Visible = false;
            ControlMRow12.Visible = false;
            ControlMRow13.Visible = false;

            ControlMRow14.Visible = false;
            ControlMRow15.Visible = false;
            ControlMRow16.Visible = false;
            ControlMRow17.Visible = false;
            ControlMRow18.Visible = false;
            ControlMRow19.Visible = false;
            ControlMRow20.Visible = false;
            ControlMRow21.Visible = false;
            ControlMRow22.Visible = false;
            ControlMRow23.Visible = false;
            ControlMRow24.Visible = false;
        
            try
            {
                if (!IsPostBack)
                {
                    if (base.Request.Params["ID"] != null)
                    {
                        SPWeb CurrentWeb = SPContext.Current.Web;
                        int _ItemID = int.Parse(base.Request.Params["ID"]);
                        SPList list = CurrentWeb.Lists["IS_ESM_Service_Requests"];
                        SPListItem item = list.GetItemById(_ItemID);

                        //item["Title"] = "IS_ESM_SERVICE_REQUESTS" + " " + DateTime.Now;
                        txtTelephoneNo.Text = item["Telephone No"].ToString();
                        //item["Request Type"] = "Server or Application Monitoring";
                        try
                        {
                            string requestTypeCollection = item["Request Type"].ToString();
                            foreach (ListItem requestTypeItem in rblRequestType.Items)
                            {
                                if (requestTypeItem.Text == requestTypeCollection)
                                {
                                    requestTypeItem.Selected = true;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorHandler.LogToWindows(ex, "ISESMServiceRequestWFTaskUpdate, RequestTypeError");
                        }

                        //rblRequestType.SelectedItem.Value = item["Request Type"].ToString();
                        txtDesignation.Text = item["Designation"].ToString();

                        SPFieldUserValue lineManager = new SPFieldUserValue(CurrentWeb, item["Line Manager"].ToString());
                        peLineManager.CommaSeparatedAccounts = getActualLogin(lineManager.User.LoginName);

                        txtChangeNumber.Text = item["Remedy Change Number"].ToString();
                        dtcDateRequired.SelectedDate = DateTime.Parse(item["Date Required"].ToString());

                        txtServerName.Text = item["Server Name"].ToString();
                        txtIPAddress.Text = item["IP Address"].ToString();

                        try
                        {
                            string isDMZCollection = item["Is DMZ Server"].ToString();
                            foreach (ListItem isDMZItem in rblIsDMZServer.Items)
                            {
                                if (isDMZItem.Text == isDMZCollection)
                                {
                                    isDMZItem.Selected = true;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorHandler.LogToWindows(ex, "ISESMServiceRequestWFTaskUpdate, DMZItem");
                        }
                        //rblIsDMZServer.SelectedItem.Text = item["Is DMZ Server"].ToString();
                        //rblOperatingSystem.SelectedItem.Text = item["Operating System"].ToString();
                        try
                        {
                            string operationSystemCollection = item["Operating System"].ToString();
                            foreach (ListItem operatingSystemItem in rblOperatingSystem.Items)
                            {
                                if (operatingSystemItem.Text == operationSystemCollection)
                                {
                                    operatingSystemItem.Selected = true;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorHandler.LogToWindows(ex, "ISESMServiceRequestWFTaskUpdate, OperatingSystemControl");
                        }
                        //rblServerClass.SelectedItem.Text = item["Server Class"].ToString();

                        try
                        {
                            string serverClassCollection = item["Server Class"].ToString();
                            foreach (ListItem serverClassItem in rblServerClass.Items)
                            {
                                if (serverClassItem.Text == serverClassCollection)
                                {
                                    serverClassItem.Selected = true;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorHandler.LogToWindows(ex, "ISESMServiceRequestWFTaskUpdate, ServerClassError");
                        }

                        txtApplicationName.Text = item["Application Name"].ToString();

                        // rblOracleInstalled.SelectedItem.Text = item["Oracle Installed"].ToString();
                        try
                        {
                            string oracleInstalledCollection = item["Oracle Installed"].ToString();
                            foreach (ListItem oracleInstalledItem in rblOracleInstalled.Items)
                            {
                                if (oracleInstalledItem.Text == oracleInstalledCollection)
                                {
                                    oracleInstalledItem.Selected = true;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorHandler.LogToWindows(ex, "ISESMServiceRequestWFTaskUpdate, OracleInstalled");
                        }
                        //rblMSExchangeInstalled.SelectedItem.Text = item["MS Exchange Installed"].ToString();
                        try
                        {
                            string msExchangeCollection = item["MS Exchange Installed"].ToString();
                            foreach (ListItem msExchangeItem in rblMSExchangeInstalled.Items)
                            {
                                if (msExchangeItem.Text == msExchangeCollection)
                                {
                                    msExchangeItem.Selected = true;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            File.WriteAllText(@"C:\Temp\msexchange.log", ex.ToString());
                            ErrorHandler.LogToWindows(ex, "ISESMServiceRequestWFTaskUpdate, MsExchangeError");
                        }
                        //rblMSSQLInstalled.SelectedItem.Text = item["MSSQL Installed"].ToString();
                        try
                        {
                            string mssqlCollection = item["MSSQL Installed"].ToString();
                            foreach (ListItem mssqlItem in rblMSSQLInstalled.Items)
                            {
                                if (mssqlItem.Text == mssqlCollection)
                                {
                                    mssqlItem.Selected = true;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            File.WriteAllText(@"C:\Temp\mssql.log", ex.ToString());
                            ErrorHandler.LogToWindows(ex, "ISESMServiceRequestWFTaskUpdate, MSSQL");
                        }
                        txtOtherApplications.Text = SPHandler.RemoveHTMLTags(item["Other Applications"].ToString());
                        txtLogFile.Text = item["Log File"].ToString();
                        txtSearchString_MonitoringConditionNote.Text = SPHandler.RemoveHTMLTags(item["Search String / Monitoring Condition Note"].ToString());
                        txtProcesses_Services.Text = SPHandler.RemoveHTMLTags(item["Processes / Services"].ToString());
                        txtCommandAndUserName.Text = SPHandler.RemoveHTMLTags(item["Command and User Name"].ToString());
                        txtScriptNameAndPath.Text = item["Script Name and Path"].ToString();
                        txtJobDescription.Text = item["Job Description"].ToString();
                        txtScriptLogFilePath.Text = item["Script Log File Path"].ToString();
                        rblCyclic.SelectedItem.Text = item["Cyclic"].ToString();
                        txtFrequency.Text = item["Frequency"].ToString();
                        txtCondition.Text = item["Condition"].ToString();
                        txtTestServerName.Text = item["Test Server Name"].ToString();
                        txtProductionServerName.Text = item["Production Server Name"].ToString();
                        rblIsTheServerBehindFirewall.SelectedItem.Text = item["Is The Server Behind Firewall"].ToString();
                        //item["Mailing List Used For Notification"] = Helper.GetUserValue(peMailingListUsedForNotification, web);
                        txtRemedyGroup.Text = item["Remedy Group Used For Failure Notification"].ToString();
                        ddlOfficeLocation.SelectedItem.Text = item["OfficeLocation"].ToString();
                                                 

                        //rblGlobalZone.SelectedItem.Text = item["Global Zone"].ToString();
                        try
                        {
                            string globalZoneCollection = item["Global Zone"].ToString();
                            foreach (ListItem globalZoneItem in rblGlobalZone.Items)
                            {
                                if (globalZoneItem.Text == globalZoneCollection)
                                {
                                    globalZoneItem.Selected = true;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorHandler.LogToWindows(ex, "ISESMServiceRequestWFTaskUpdate, GlobalZone");
                        }
                        //Other added Columns
                        txtOtherOperatingSystem.Text = item["Other Operating System"].ToString();
                        txtCyclicRestartInterval.Text = item["Cyclic Restart Interval"].ToString();
                        rblSourceServerBehind.SelectedItem.Text = item["Is The Source Server Behind Firewall"].ToString();
                        rblDestinationServerBehind.SelectedItem.Text = item["Is The Destination Server Behind Firewall"].ToString();
                        txtSourceServerName.Text = item["Source Server Name"].ToString();
                        txtSourceServerPath.Text = item["Source Server Path"].ToString();
                        txtFileNamingPattern.Text = item["File Naming Pattern"].ToString();
                        txtDestinationServerName.Text = item["Destination Server Name"].ToString();
                        txtDestinationServerPath.Text = item["Destination Server Path"].ToString();
                        txtSourceUsername.Text = item["Source Username"].ToString();
                        txtDestinationUsername.Text = item["Destination Username"].ToString();
                        txtFileNameAndPath.Text = item["File Name and Path"].ToString();
                        txtUserThatWillRead.Text = item["File Reader"].ToString();
                        item.Update(); 
                    }
                }
                 
            }
            catch (Exception ex)
            {
                File.WriteAllText(@"C:\Temp\requestError.log", ex.ToString());
                ErrorHandler.LogToWindows(ex, "ISESMServiceRequestWFTask, PageLoad");                
            }             
        }

        private void LoadLocation()
        {
            ddlOfficeLocation.DataSource = this.Web.Lists["Locations"].Items.GetDataTable();
            ddlOfficeLocation.DataValueField = "ID";
            ddlOfficeLocation.DataTextField = "Title";
            ddlOfficeLocation.DataBind();
            //ddlOfficeLocation.Items.Insert(0, new ListItem("", ""));
        }

        public string getActualLogin(string claimLogin)
        {
            try
            {
                int barLoc = claimLogin.IndexOf("|");
                if (barLoc > -1)
                {
                    claimLogin = claimLogin.Substring(barLoc + 1);
                }
            }
            catch (Exception ex)
            {
                File.WriteAllText(@"C:\Temp\getActualLoginError.log", ex.ToString());
            }
            return claimLogin;
        }

        protected void updateRequest_Click(object sender, EventArgs e)
        {
            try
            {
                using (SPSite site = new SPSite(SPContext.Current.Web.Url))
                {
                    using (SPWeb web = site.OpenWeb())
                    {
                        SPWeb CurrentWeb = SPContext.Current.Web;
                        int _ItemID = int.Parse(base.Request.Params["ID"]);
                        SPList list = CurrentWeb.Lists["IS_ESM_Service_Requests"];
                        SPListItem item = list.GetItemById(_ItemID);
                        
                        item["Title"] = "IS_ESM_SERVICE_REQUESTS" + " " + DateTime.Now;
                        item["Telephone No"] = txtTelephoneNo.Text;
                        //item["Request Type"] = "Server or Application Monitoring";
                        item["Request Type"] = rblRequestType.SelectedItem.Value;
                        item["Designation"] = txtDesignation.Text;
                        item["Line Manager"] = Helper.GetUserValue(peLineManager, web);
                        item["Remedy Change Number"] = txtChangeNumber.Text;
                        item["Date Required"] = Convert.ToDateTime(dtcDateRequired.SelectedDate.ToString());
                        item["Server Name"] = txtServerName.Text;
                        item["IP Address"] = txtIPAddress.Text;
                        item["Is DMZ Server"] = rblIsDMZServer.SelectedItem.Text;
                        item["Operating System"] = rblOperatingSystem.SelectedItem.Text;
                        item["Server Class"] = rblServerClass.SelectedItem.Text;
                        item["Oracle Installed"] = rblOracleInstalled.SelectedItem.Text;
                        item["MS Exchange Installed"] = rblMSExchangeInstalled.SelectedItem.Text;
                        item["MSSQL Installed"] = rblMSSQLInstalled.SelectedItem.Text;
                        item["Other Applications"] = SPHandler.RemoveHTMLTags(txtOtherApplications.Text);
                        item["Log File"] = txtLogFile.Text;
                        item["Search String / Monitoring Condition Note"] = SPHandler.RemoveHTMLTags(txtSearchString_MonitoringConditionNote.Text);
                        item["Processes / Services"] = SPHandler.RemoveHTMLTags(txtProcesses_Services.Text);
                        item["Command and User Name"] = SPHandler.RemoveHTMLTags(txtCommandAndUserName.Text);
                        item["Script Name and Path"] = txtScriptNameAndPath.Text;
                        item["Job Description"] = txtJobDescription.Text;
                        item["Script Log File Path"] = txtScriptLogFilePath.Text;
                        item["Cyclic"] = rblCyclic.SelectedItem.Text;
                        item["Frequency"] = txtFrequency.Text;
                        item["Condition"] = txtCondition.Text;
                        item["Test Server Name"] = txtTestServerName.Text;
                        item["Production Server Name"] = txtProductionServerName.Text;
                        item["Is The Server Behind Firewall"] = rblIsTheServerBehindFirewall.SelectedItem.Text;
                        item["Mailing List Used For Notification"] = Helper.GetUserValue(peMailingListUsedForNotification, web);
                        item["Remedy Group Used For Failure Notification"] = txtRemedyGroup.Text;
                        item["OfficeLocation"] = ddlOfficeLocation.SelectedItem.Text;
                        item["Application Name"] = txtApplicationName.Text;
                        item["Global Zone"] = rblGlobalZone.SelectedItem.Text;
                        //Other added Columns
                        item["Other Operating System"] = txtOtherOperatingSystem.Text;
                        item["Cyclic Restart Interval"] = txtCyclicRestartInterval.Text;
                        item["Is The Source Server Behind Firewall"] = rblSourceServerBehind.SelectedItem.Text;
                        item["Is The Destination Server Behind Firewall"] = rblDestinationServerBehind.SelectedItem.Text;
                        item["Source Server Name"] = txtSourceServerName.Text;
                        item["Source Server Path"] = txtSourceServerPath.Text;
                        item["File Naming Pattern"] = txtFileNamingPattern.Text;
                        item["Destination Server Name"] = txtDestinationServerName.Text;
                        item["Destination Server Path"] = txtDestinationServerPath.Text;
                        item["Source Username"] = txtSourceUsername.Text;
                        item["Destination Username"] = txtDestinationUsername.Text;
                        item["File Name and Path"] = txtFileNameAndPath.Text;
                        item["File Reader"] = txtUserThatWillRead.Text;
                        item.Update();

                        //gets name of the workflow Initiator to send an update message
                        SPFieldUserValue initiator = new SPFieldUserValue(web, item["Created By"].ToString());

                        //Update the activities for the Change Requester
                        string tasksID = item["Tasks ID"].ToString();
                        string initiatorName = initiator.User.Name;
                        string to = item["Change Requester Email"].ToString();
                        string from = "workflows@mtn.com";
                        string changeRequester = item["Change Requester"].ToString();
                        string updateChangeLink = web.Url + "/_layouts/15/EsmServiceRequest/ISESMServiceRequestWFTask.aspx?" +
                            "List=2019906e-605a-4841-a455-951dc4f44b0f&ID=" + tasksID + "&Source=" + web.Url + "/InformationSystems/Lists/ISESMServiceRequestTasks";

                        string Body = "Dear " + changeRequester + ",<br><br>";
                        //Body += "A new Request from " + _Requester.DisplayName + " is awaiting your kind approval. ";

                        Body += "Your requested update has been done by " + initiatorName + ".<br><br> Click <a href='" + updateChangeLink + "'>here</a> to view the updated request. <br><br>";

                        Body += "<br><br>Kind Regards,<br>";
                        Body += "Information Systems Management";
                        Body += "<br><br>NOTE: This email message is auto generated, Please do not reply to the email address.";
                        EMailHandler.reqestChangeSendEmail(to, from, null, "Information System Update", Body);

                        Response.Redirect(SPContext.Current.Web.Url); 
                    }
                }
            }
            catch (Exception ex)
            {
                File.WriteAllText(@"C:\Temp\updateError.log", ex.ToString());
                //ErrorHandler.LogToWindows(ex, "ISESMServiceRequestWFTaskUpdate, Upadate");
            }
        }       

        protected void _btnCancelUpdate_Click(object sender, EventArgs e)
        {
            Response.Redirect(Web.Url + "/_layouts/15/EsmServiceRequest/newform.aspx");
        }

        protected void rblControlMselected(object sender, EventArgs e)
        {

            if (rblRequestType.SelectedItem.Text == "Control-M Job")
            {

                MonitoringRow1.Visible = false;
                MonitoringRow2.Visible = false;
                MonitoringRow3.Visible = false;
                MonitoringRow4.Visible = false;
                MonitoringRow5.Visible = false;
                MonitoringRow6.Visible = false;
                MonitoringRow7.Visible = false;
                MonitoringRow8.Visible = false;
                MonitoringRow9.Visible = false;
                MonitoringRow10.Visible = false;
                MonitoringRow11.Visible = false;
                MonitoringRow12.Visible = false;
                MonitoringRow13.Visible = false;
                MonitoringRow20.Visible = false;
                MonitoringRow21.Visible = false;
                MonitoringRow22.Visible = true;
                MonitoringRow24.Visible = false;
            }
            else
            {
                MonitoringRow1.Visible = true;
                MonitoringRow2.Visible = true;
                MonitoringRow3.Visible = true;
                MonitoringRow4.Visible = true;
                MonitoringRow5.Visible = true;
                MonitoringRow6.Visible = true;
                MonitoringRow7.Visible = true;
                MonitoringRow8.Visible = true;
                MonitoringRow9.Visible = true;
                MonitoringRow10.Visible = true;
                MonitoringRow11.Visible = true;
                MonitoringRow12.Visible = true;
                MonitoringRow13.Visible = true;
                MonitoringRow20.Visible = true;
                MonitoringRow21.Visible = true;
                MonitoringRow22.Visible = false;
                MonitoringRow24.Visible = false;


                ControlMRow4.Visible = false;
                ControlMRow6.Visible = false;
                ControlMRow7.Visible = false;
                ControlMRow8.Visible = false;
                ControlMRow9.Visible = false;
                ControlMRow10.Visible = false;
                ControlMRow11.Visible = false;
                ControlMRow12.Visible = false;
                ControlMRow13.Visible = false;
                ControlMRow14.Visible = false;
                ControlMRow15.Visible = false;
                ControlMRow16.Visible = false;
                ControlMRow17.Visible = false;
                ControlMRow18.Visible = false;
                ControlMRow19.Visible = false;
                ControlMRow20.Visible = false;
                ControlMRow21.Visible = false;
                ControlMRow22.Visible = false;
                ControlMRow23.Visible = false;
                ControlMRow24.Visible = false;

            }
        }
        protected void rblControlMOption(object sender, EventArgs e)
        {

            if (rblControlOptions.SelectedItem.Text == "Script")
            {

                MonitoringRow1.Visible = false;
                MonitoringRow2.Visible = false;
                MonitoringRow3.Visible = false;
                MonitoringRow4.Visible = false;
                MonitoringRow5.Visible = false;
                MonitoringRow6.Visible = false;
                MonitoringRow7.Visible = false;
                MonitoringRow8.Visible = false;
                MonitoringRow9.Visible = false;
                MonitoringRow10.Visible = false;
                MonitoringRow11.Visible = false;
                MonitoringRow12.Visible = false;
                MonitoringRow13.Visible = false;
                MonitoringRow20.Visible = false;
                MonitoringRow21.Visible = false;
                MonitoringRow24.Visible = false;
                //MonitoringRow22.Visible = true;
                ControlMRow1.Visible = true;
                ControlMRow2.Visible = true;
                ControlMRow3.Visible = true;
                ControlMRow4.Visible = true;
                ControlMRow6.Visible = false;
                ControlMRow7.Visible = true;
                ControlMRow8.Visible = true;
                ControlMRow9.Visible = true;
                ControlMRow10.Visible = true;
                ControlMRow11.Visible = true;
                ControlMRow12.Visible = true;
                ControlMRow13.Visible = true;
            }
            else if (rblControlOptions.SelectedItem.Text == "File Transfer")
            {
                MonitoringRow1.Visible = false;
                MonitoringRow2.Visible = false;
                MonitoringRow3.Visible = false;
                MonitoringRow4.Visible = false;
                MonitoringRow5.Visible = false;
                MonitoringRow6.Visible = false;
                MonitoringRow7.Visible = false;
                MonitoringRow8.Visible = false;
                MonitoringRow9.Visible = false;
                MonitoringRow10.Visible = false;
                MonitoringRow11.Visible = false;
                MonitoringRow12.Visible = false;
                MonitoringRow13.Visible = false;
                MonitoringRow20.Visible = false;
                MonitoringRow21.Visible = false;
                MonitoringRow24.Visible = false;
                //MonitoringRow22.Visible = true;
                ControlMRow1.Visible = false;
                ControlMRow2.Visible = false;
                ControlMRow3.Visible = false;
                ControlMRow4.Visible = true;
                ControlMRow6.Visible = false;
                ControlMRow7.Visible = true;
                ControlMRow8.Visible = false;
                ControlMRow9.Visible = true;
                ControlMRow10.Visible = true;
                ControlMRow11.Visible = false;
                ControlMRow12.Visible = true;
                ControlMRow13.Visible = true;
                ControlMRow14.Visible = true;
                ControlMRow15.Visible = true;
                ControlMRow16.Visible = true;
                ControlMRow17.Visible = true;
                ControlMRow18.Visible = true;
                ControlMRow19.Visible = true;
                ControlMRow20.Visible = true;
                ControlMRow21.Visible = true;
                ControlMRow22.Visible = true;
            }
            else if (rblControlOptions.SelectedItem.Text == "File Watcher")
            {
                MonitoringRow1.Visible = false;
                MonitoringRow2.Visible = false;
                MonitoringRow3.Visible = false;
                MonitoringRow4.Visible = false;
                MonitoringRow5.Visible = false;
                MonitoringRow6.Visible = false;
                MonitoringRow7.Visible = false;
                MonitoringRow8.Visible = false;
                MonitoringRow9.Visible = false;
                MonitoringRow10.Visible = false;
                MonitoringRow11.Visible = false;
                MonitoringRow12.Visible = false;
                MonitoringRow13.Visible = false;
                MonitoringRow20.Visible = false;
                MonitoringRow21.Visible = false;
                MonitoringRow24.Visible = false;
                //MonitoringRow22.Visible = true;
                ControlMRow4.Visible = true;
                ControlMRow6.Visible = false;
                ControlMRow7.Visible = true;
                ControlMRow8.Visible = true;
                ControlMRow9.Visible = true;
                ControlMRow10.Visible = true;
                ControlMRow11.Visible = true;
                ControlMRow12.Visible = true;
                ControlMRow13.Visible = true;
                ControlMRow23.Visible = false;
                ControlMRow24.Visible = false;
                ControlMRow14.Visible = false;
                ControlMRow15.Visible = false;
                ControlMRow16.Visible = false;
                ControlMRow17.Visible = false;
                ControlMRow18.Visible = false;
                ControlMRow19.Visible = false;
                ControlMRow20.Visible = false;
                ControlMRow21.Visible = false;
                ControlMRow22.Visible = false;
                ControlMRow23.Visible = true;
                ControlMRow24.Visible = true;

            }
        }

        protected void rblOtherOperatingSystem(object sender, EventArgs e)
        {
            if (rblOperatingSystem.SelectedItem.Text == "Others")
            {
                MonitoringRow24.Visible = true;
            }
            else
            {
                MonitoringRow24.Visible = false;
            }
        }
        protected void rblCyclicSelected(object sender, EventArgs e)
        {
            if (rblCyclic.SelectedItem.Text == "Yes (From Start)" || rblCyclic.SelectedItem.Text == "Yes (From End)")
            {
                ControlMRow6.Visible = true;
            }
            else
            {
                ControlMRow6.Visible = false;
            }
        }
    }
}
