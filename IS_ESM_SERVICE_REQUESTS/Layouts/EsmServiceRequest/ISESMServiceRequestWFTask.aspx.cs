﻿using System;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Workflow;
using Microsoft.SharePoint.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using IS_ESM_SERVICE_REQUESTS.UtilityCode;
using System.IO;

namespace IS_ESM_SERVICE_REQUESTS.Layouts.IS_ESM_SERVICE_REQUESTS
{
    public partial class ISESMServiceRequestWFTask : LayoutsPageBase
    {


        #region Private Variable
        SPList _TaskList;
        SPListItem _TaskListItem;
        SPList _RequestsList;
        SPListItem _RequestItem;
        string _DisplayFormMode = "";
        static Hashtable _TaskExtendedProperties;
        int _ItemID = 0;
        string _ListTitle = "";
        string _AllComments = "";

        #endregion

        //protected void Page_PreInit(object sender, EventArgs e)
        //{
        //	//Page.MasterPageFile = SPContext.Current.Web.ServerRelativeUrl + "/_catalogs/masterpage/MTNNERecruitmentMasterPage.master";
        //	Page.MasterPageFile = SPContext.Current.Web.ServerRelativeUrl + "/_catalogs/masterpage/default.master";
        //}

        protected override void OnInit(EventArgs e)
        {
            if (dtcDateRequired != null)
            {
                dtcDateRequired.MinDate = DateTime.Now.AddDays(-1);//Min Date is Today's date;
                dtcDateRequired.MaxDate = DateTime.Now.Date.AddYears(50);
            }
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            bool isValid = true;
            try
            {
                if (!IsPostBack)
                {
                    if (base.Request.Params["ID"] != null)
                    {
                        SPWeb CurrentWeb = SPContext.Current.Web;
                        string id = Request.Params["ID"];
                        _TaskList = CurrentWeb.Lists["ISESMServiceRequestTasks"];
                        _TaskListItem = _TaskList.GetItemById(int.Parse(id));
                        _TaskExtendedProperties = SPWorkflowTask.GetExtendedPropertiesAsHashtable((SPListItem)_TaskListItem);
                        _DisplayFormMode = _TaskExtendedProperties["DisplayForm"].ToString();
                        ViewState["DisplayFormMode"] = _DisplayFormMode;
                        _ItemID = int.Parse(_TaskExtendedProperties["ItemID"].ToString());
                        _ListTitle = _TaskExtendedProperties["ListTitle"].ToString();
                        _AllComments = _TaskExtendedProperties["Comments"].ToString();
                        ltlCommentsHistory.Text = _AllComments;
                        _RequestsList = SPContext.Current.Web.Lists[_ListTitle];
                        _RequestItem = _RequestsList.GetItemById(_ItemID);
                        SPFieldUserValueCollection RequesterUserValueCollection = new SPFieldUserValueCollection(CurrentWeb, _RequestItem["Author"].ToString());
                        SPUser RequesterUser = RequesterUserValueCollection[0].User;
                        SPFieldUserValueCollection LineManagerValueCollection = new SPFieldUserValueCollection(CurrentWeb, _RequestItem["Line Manager"].ToString());
                        SPUser LineManagerUser = LineManagerValueCollection[0].User;
                        lblRequestType.Text = _RequestItem["Request Type"].ToString();
                        lblUserName.Text = RequesterUser.Name;
                        lblEMail.Text = RequesterUser.Email;
                        lblOfficeLocation.Text = _RequestItem["OfficeLocation"].ToString();
                        //lblOfficeLocation.Text = SPHandler.GetLocation(int.Parse(_RequestItem["OfficeLocationID"].ToString()), CurrentWeb.Site.RootWeb);
                        lblLineManager.Text = LineManagerUser.Name;
                        lblTelephoneNo.Text = _RequestItem["Telephone No"].ToString();

                        if (!String.IsNullOrEmpty((string)_RequestItem["Designation"]))
                            lblDesignation.Text = _RequestItem["Designation"].ToString();

                        if (_DisplayFormMode == "ESMTeam")
                            rowESMManager.Style["display"] = "block";
                        if (_DisplayFormMode == "Requester")
                        {
                            btnReject.CssClass = "ms-ButtonHeightWidth120";
                            //btnRequestChange.Visible = false;
                            btnApprove.Text = "Submit";
                            btnReject.Text = "Cancel Request";

                            rowMessage.Visible = true;
                            if (_TaskExtendedProperties["Rejector"].ToString() == ISESMServiceRequestWFRecjector.LineManager.ToString())
                                rowMessage.Cells[0].InnerText = "The Line Manager has requested change from you.";
                            else if (_TaskExtendedProperties["Rejector"].ToString() == ISESMServiceRequestWFRecjector.ESMTeam.ToString())
                                rowMessage.Cells[0].InnerText = "The ESM Team has requested change from you.";
                            else if (_TaskExtendedProperties["Rejector"].ToString() == ISESMServiceRequestWFRecjector.ESMManager.ToString())
                                rowMessage.Cells[0].InnerText = "The ESM Manager has requested change from you.";

                            ShowRequesterControls();

                            //txtTelephoneNo.Text = _RequestItem["Telephone No"].ToString();
                            //txtDesignation.Text = _RequestItem["Designation"].ToString();

                            if (!String.IsNullOrEmpty((string)_RequestItem["Remedy Change Number"]))
                                txtChangeNumber.Text = _RequestItem["Remedy Change Number"].ToString();
                            dtcDateRequired.SelectedDate = ((DateTime)_RequestItem["Date Required"]);

                            if (lblRequestType.Text == "Server or Application Monitoring")
                            {
                                HideControlM();

                                txtServerName.Text = _RequestItem["Server Name"].ToString();
                                txtIPAddress.Text = _RequestItem["IP Address"].ToString();
                                txtApplicationName.Text = _RequestItem["Application Name"].ToString();
                                rblIsDMZServer.Text = _RequestItem["Is DMZ Server"].ToString();
                                rblOperatingSystem.Text = _RequestItem["Operating System"].ToString();
                                rblServerClass.Text = _RequestItem["Server Class"].ToString();
                                rblGlobalZone.Text = _RequestItem["Global Zone"].ToString();
                                rblOracleInstalled.Text = _RequestItem["Oracle Installed"].ToString();
                                rblMSExchangeInstalled.Text = _RequestItem["MS Exchange Installed"].ToString();
                                rblMSSQLInstalled.Text = _RequestItem["MSSQL Installed"].ToString();
                                if (!String.IsNullOrEmpty((string)_RequestItem["Other Applications"]))
                                    txtOtherApplications.Text = SPHandler.RemoveHTMLTags(_RequestItem["Other Applications"].ToString());
                                if (!String.IsNullOrEmpty((string)_RequestItem["Log File"]))
                                    txtLogFile.Text = _RequestItem["Log File"].ToString();
                                if (!String.IsNullOrEmpty((string)_RequestItem["Search String / Monitoring Condition Note"]))
                                    txtSearchString_MonitoringConditionNote.Text = SPHandler.RemoveHTMLTags(_RequestItem["Search String / Monitoring Condition Note"].ToString());
                                if (!String.IsNullOrEmpty((string)_RequestItem["Processes / Services"]))
                                    txtProcesses_Services.Text = SPHandler.RemoveHTMLTags(_RequestItem["Processes / Services"].ToString());
                                if (!String.IsNullOrEmpty((string)_RequestItem["Command and User Name"]))
                                    txtCommandAndUserName.Text = SPHandler.RemoveHTMLTags(_RequestItem["Command and User Name"].ToString());
                            }
                            else//Control-M Job
                            {
                                HideMonitoring();

                                txtScriptNameAndPath.Text = _RequestItem["Script Name and Path"].ToString();
                                txtJobDescription.Text = _RequestItem["Job Description"].ToString();
                                if (!String.IsNullOrEmpty((string)_RequestItem["Script Log File Path"]))
                                    txtScriptLogFilePath.Text = _RequestItem["Script Log File Path"].ToString();
                                rblCyclic.Text = _RequestItem["Cyclic"].ToString();
                                if (rblCyclic.Text != "No")
                                    txtCyclicRestartInterval.Text = _RequestItem["Cyclic Restart Interval"].ToString();
                                else
                                {
                                    ControlMRow6.Style["display"] = "none";
                                    txtCyclicRestartIntervalValidator.Visible = false;
                                }
                                txtFrequency.Text = _RequestItem["Frequency"].ToString();
                                txtCondition.Text = _RequestItem["Condition"].ToString();
                                if (!String.IsNullOrEmpty((string)_RequestItem["Test Server Name"]))
                                    txtTestServerName.Text = _RequestItem["Test Server Name"].ToString();
                                txtProductionServerName.Text = _RequestItem["Production Server Name"].ToString();
                                rblIsTheServerBehindFirewall.Text = _RequestItem["Is The Server Behind Firewall"].ToString();
                                //osaka
                                string tempMailingList = "";
                                SPFieldUserValueCollection MailingListUserValues = new SPFieldUserValueCollection(CurrentWeb, _RequestItem["Mailing List Used For Notification"].ToString());
                                for (int i = 0; i < MailingListUserValues.Count; i++)
                                {
                                    tempMailingList += MailingListUserValues[i].User.LoginName + ",";
                                }
                                tempMailingList = tempMailingList.Remove(tempMailingList.Length - 1, 1);
                                peMailingListUsedForNotification.CommaSeparatedAccounts = tempMailingList;

                                txtRemedyGroup.Text = _RequestItem["Remedy Group Used For Failure Notification"].ToString();
                            }
                        }
                        else // Not Request Change (i.e Task assigned to anyone except requester)
                        {
                            if (!String.IsNullOrEmpty((string)_RequestItem["Remedy Change Number"]))
                                lblChangeNumber.Text = _RequestItem["Remedy Change Number"].ToString();
                            lblDateRequired.Text = ((DateTime)_RequestItem["Date Required"]).ToString("dd MMM yyyy");

                            if (lblRequestType.Text == "Server or Application Monitoring")
                            {
                                HideControlM();
                                lblApplicationName.Text = _RequestItem["Application Name"].ToString();
                                lblGlobalZone.Text = Convert.ToString(_RequestItem["Global Zone"]);
                                // Othe added controls are as above
                                lblServerName.Text = _RequestItem["Server Name"].ToString();
                                lblIPAddress.Text = _RequestItem["IP Address"].ToString();
                                lblIsDMZServer.Text = Convert.ToString(_RequestItem["Is DMZ Server"]);

                                lblOperatingSystem.Text = _RequestItem["Operating System"].ToString();
                                lblServerClass.Text = _RequestItem["Server Class"].ToString();
                                lblOracleInstalled.Text = Convert.ToString(_RequestItem["Oracle Installed"]);

                                lblMSExchangeInstalled.Text = Convert.ToString(_RequestItem["MS Exchange Installed"]);
                                lblMSSQLInstalled.Text = Convert.ToString(_RequestItem["MSSQL Installed"]);
                                lblOfficeLocation.Text = Convert.ToString(_RequestItem["OfficeLocation"]);

                                if (!String.IsNullOrEmpty((string)_RequestItem["Other Applications"]))
                                    lblOtherApplications.Text = SPHandler.RemoveHTMLTags(_RequestItem["Other Applications"].ToString());
                                if (!String.IsNullOrEmpty((string)_RequestItem["Log File"]))
                                    lblLogFile.Text = _RequestItem["Log File"].ToString();
                                if (!String.IsNullOrEmpty((string)_RequestItem["Search String / Monitoring Condition Note"]))
                                    lblSearchString_MonitoringConditionNote.Text = SPHandler.RemoveHTMLTags(_RequestItem["Search String / Monitoring Condition Note"].ToString());
                                if (!String.IsNullOrEmpty((string)_RequestItem["Processes / Services"]))
                                    lblProcesses_Services.Text = SPHandler.RemoveHTMLTags(_RequestItem["Processes / Services"].ToString());
                                if (!String.IsNullOrEmpty((string)_RequestItem["Command and User Name"]))
                                    lblCommandAndUserName.Text = SPHandler.RemoveHTMLTags(_RequestItem["Command and User Name"].ToString());
                            }
                            else//Control-M Job
                            {
                                HideMonitoring();

                                lblScriptNameAndPath.Text = _RequestItem["Script Name and Path"].ToString();
                                lblJobDescription.Text = _RequestItem["Job Description"].ToString();
                                if (!String.IsNullOrEmpty((string)_RequestItem["Script Log File Path"]))
                                    lblScriptLogFilePath.Text = _RequestItem["Script Log File Path"].ToString();
                                lblCyclic.Text = _RequestItem["Cyclic"].ToString();
                                if (lblCyclic.Text != "No")
                                    lblCyclicRestartInterval.Text = _RequestItem["Cyclic Restart Interval"].ToString();
                                else
                                    ControlMRow6.Style["display"] = "none";

                                lblFrequency.Text = _RequestItem["Frequency"].ToString();
                                lblCondition.Text = _RequestItem["Condition"].ToString();
                                if (!String.IsNullOrEmpty((string)_RequestItem["Test Server Name"]))
                                    lblTestServerName.Text = _RequestItem["Test Server Name"].ToString();
                                lblProductionServerName.Text = _RequestItem["Production Server Name"].ToString();
                                if ((bool)_RequestItem["Is The Server Behind Firewall"])
                                    lblIsTheServerBehindFirewall.Text = "Yes";
                                else
                                    lblIsTheServerBehindFirewall.Text = "No";

                                string tempMailingList = "";
                                SPFieldUserValueCollection MailingListUserValues = new SPFieldUserValueCollection(CurrentWeb, _RequestItem["Mailing List Used For Notification"].ToString());
                                for (int i = 0; i < MailingListUserValues.Count; i++)
                                {
                                    tempMailingList += MailingListUserValues[i].User.Name + " ; ";
                                }
                                tempMailingList = tempMailingList.Remove(tempMailingList.Length - 3, 3);
                                lblMailingListUsedForNotification.Text = tempMailingList; ;

                                lblRemedyGroup.Text = _RequestItem["Remedy Group Used For Failure Notification"].ToString();
                            }
                        }
                    }
                }
                else
                {
                    //if post back:
                    _DisplayFormMode = (string)ViewState["DisplayFormMode"];
                    if (lblRequestType.Text == "Control-M Job" && _DisplayFormMode == "Requester")
                    {
                        if (peMailingListUsedForNotification.ResolvedEntities.Count != 0)
                            spanMailingListUsedForNotification.Style["display"] = "none";
                        if (rblCyclic.Text != "No")
                        {
                            txtCyclicRestartInterval.Visible = true;
                            txtCyclicRestartIntervalValidator.Visible = true;
                            ControlMRow6.Style["display"] = "block";
                        }
                        else
                        {
                            ControlMRow6.Style["display"] = "none";
                            txtCyclicRestartIntervalValidator.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogToWindows(ex, "ISESMServiceRequestWFTask, PageLoad");
                isValid = false;
                btnApprove.Visible = false;
                btnReject.Visible = false;
                btnClose.Visible = false;
                //btnRequestChange.Visible = false;
            }
            if (!isValid)
                Response.Redirect(SPContext.Current.Web.Url);
            //Response.Redirect(Web.Url + "/_layouts/EsmServiceRequest/NewForm.aspx");

            //Response.Redirect("" +
            //"https
            //://sharepoint.mtnnigeria.net/sites/InformationSystems#/playlist1/local1");


            //Response.Redirect(SPContext.Current.Web.Url + "/_layouts/EsmServiceRequest/ISESMServiceRequestWFTask.aspx");
        }

        private void ShowRequesterControls()
        {
            //txtTelephoneNo.Visible = true;
            //txtTelephoneNoValidator.Visible = true;
            //lblTelephoneNo.Visible = false;

            //txtDesignation.Visible = true;
            //txtDesignationValidator.Visible = true;
            //lblDesignation.Visible = false;

            txtChangeNumber.Visible = true;
            lblChangeNumber.Visible = false;

            dtcDateRequired.Visible = true;
            lblDateRequired.Visible = false;

            if (lblRequestType.Text == "Server or Application Monitoring")
            {
                txtServerName.Visible = true;
                txtServerNameValidator.Visible = true;
                lblServerName.Visible = false;

                txtApplicationName.Visible = true;
                txtApplicationNameValidator.Visible = true;
                lblApplicationName.Visible = false;

                txtIPAddress.Visible = true;
                txtIPAddressValidator.Visible = true;
                lblIPAddress.Visible = false;
                rblIsDMZServer.Visible = true;
                lblIsDMZServer.Visible = false;
                rblOperatingSystem.Visible = true;
                lblOperatingSystem.Visible = false;
                rblServerClass.Visible = true;
                lblServerClass.Visible = false;
                rblGlobalZone.Visible = true;
                lblGlobalZone.Visible = false;
                rblOracleInstalled.Visible = true;
                lblOracleInstalled.Visible = false;
                rblMSExchangeInstalled.Visible = true;
                lblMSExchangeInstalled.Visible = false;
                rblMSSQLInstalled.Visible = true;
                lblMSSQLInstalled.Visible = false;
                txtOtherApplications.Visible = true;
                lblOtherApplications.Visible = false;
                txtLogFile.Visible = true;
                lblLogFile.Visible = false;
                txtSearchString_MonitoringConditionNote.Visible = true;
                lblSearchString_MonitoringConditionNote.Visible = false;
                txtProcesses_Services.Visible = true;
                lblProcesses_Services.Visible = false;
                txtCommandAndUserName.Visible = true;
                lblCommandAndUserName.Visible = false;
            }
            else//Control-M Job
            {
                HideMonitoring();

                txtScriptNameAndPath.Visible = true;
                txtScriptNameAndPathValidator.Visible = true;
                lblScriptNameAndPath.Visible = false;
                txtJobDescription.Visible = true;
                txtJobDescriptionValidator.Visible = true;
                lblJobDescription.Visible = false;
                txtScriptLogFilePath.Visible = true;
                lblScriptLogFilePath.Visible = false;
                rblCyclic.Visible = true;
                lblCyclic.Visible = false;
                txtCyclicRestartInterval.Visible = true;
                txtCyclicRestartIntervalValidator.Visible = true;
                lblCyclicRestartInterval.Visible = false;
                txtFrequency.Visible = true;
                txtFrequencyValidator.Visible = true;
                lblFrequency.Visible = false;
                txtCondition.Visible = true;
                txtConditionValidator.Visible = true;
                lblCondition.Visible = false;
                txtTestServerName.Visible = true;
                lblTestServerName.Visible = false;
                txtProductionServerName.Visible = true;
                txtProductionServerNameValidator.Visible = true;
                lblProductionServerName.Visible = false;
                rblIsTheServerBehindFirewall.Visible = true;
                lblIsTheServerBehindFirewall.Visible = false;
                lblMailingListUsedForNotification.Visible = false;
                peMailingListUsedForNotification.Visible = true;
                lblRemedyGroup.Visible = false;
                txtRemedyGroup.Visible = true;
                txtRemedyGroupValidator.Visible = true;
            }
        }

        private void HideControlM()
        {
            ControlMRow1.Visible = false;
            ControlMRow2.Visible = false;
            ControlMRow3.Visible = false;
            ControlMRow4.Visible = false;
            ControlMRow6.Visible = false;
            ControlMRow7.Visible = false;
            ControlMRow8.Visible = false;
            ControlMRow9.Visible = false;
            ControlMRow10.Visible = false;
            ControlMRow11.Visible = false;
            ControlMRow12.Visible = false;
            ControlMRow13.Visible = false;
        }

        private void HideMonitoring()
        {
            MonitoringRow1.Visible = false;
            MonitoringRow2.Visible = false;
            MonitoringRow3.Visible = false;
            MonitoringRow4.Visible = false;
            MonitoringRow5.Visible = false;
            MonitoringRow6.Visible = false;
            MonitoringRow7.Visible = false;
            MonitoringRow8.Visible = false;
            MonitoringRow9.Visible = false;
            MonitoringRow10.Visible = false;
            MonitoringRow11.Visible = false;
            MonitoringRow12.Visible = false;
            MonitoringRow13.Visible = false;
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            bool Success = true;
            try
            {
                _DisplayFormMode = (string)ViewState["DisplayFormMode"];

                if (lblRequestType.Text == "Control-M Job" && _DisplayFormMode == "Requester")
                {
                    if (peMailingListUsedForNotification.ResolvedEntities.Count == 0)
                    {
                        spanMailingListUsedForNotification.Style["display"] = "block";
                        return;
                    }
                    else
                        spanMailingListUsedForNotification.Style["display"] = "none";
                }
                else
                    spanMailingListUsedForNotification.Style["display"] = "none";



                SPContext.Current.Web.AllowUnsafeUpdates = true;
                string _comments = GetComments();
                Hashtable _hash = new Hashtable();
                _hash.Add("CurrentUserId", SPContext.Current.Web.CurrentUser.ID);
                _hash.Add("Action", "Approved");
                _hash.Add("Comments", _comments);
                if (_DisplayFormMode == "ESMTeam")// && _TaskExtendedProperties["OD"].ToString() == "FirstTime")
                {
                    //Validate PeopleEditor Controls
                    if (_peESMManager.ResolvedEntities.Count == 0)
                        _spanESMManager.Style["display"] = "block";
                    else
                        _spanESMManager.Style["display"] = "none";
                    if (_peESMManager.ResolvedEntities.Count == 0)
                        return;

                    string _Url = SPContext.Current.Web.Url;
                    SPUser LineManagerUser = null;
                    SPSite _site = null;
                    SPWeb _web = null;

                    SPSecurity.RunWithElevatedPrivileges(delegate ()
                    {
                        _site = new SPSite(_Url);
                        _web = _site.OpenWeb();
                        _web.AllowUnsafeUpdates = true;
                        ////////////////
                        PickerEntity LineManagerEntity = (PickerEntity)_peESMManager.ResolvedEntities[0];
                        if (LineManagerEntity.IsResolved == true)
                            LineManagerUser = _web.EnsureUser(LineManagerEntity.Key);
                        else
                            LineManagerUser = null;
                        _web.AllowUnsafeUpdates = false;

                        if (_web != null)
                            _web.Dispose();
                        if (_site != null)
                            _site.Dispose();
                    }
                     );
                    //string OD = _DisplayRRUserControl.GetPeopleEditor();
                    //if (OD == "")
                    //    return;
                    //else
                    _hash.Add("ESMManagerLoginName", LineManagerUser.LoginName);
                }
                else if (_DisplayFormMode == "Requester")
                {
                    string _Url = SPContext.Current.Web.Url;
                    SPSite _site = null;
                    SPWeb _web = null;

                    SPSecurity.RunWithElevatedPrivileges(delegate ()
                    {
                        _site = new SPSite(_Url);
                        _web = _site.OpenWeb();
                        _web.AllowUnsafeUpdates = true;
                        ////////////////////////////////
                        _TaskListItem = SPContext.Current.ListItem;
                        _TaskExtendedProperties = SPWorkflowTask.GetExtendedPropertiesAsHashtable((SPListItem)_TaskListItem);
                        _ListTitle = _TaskExtendedProperties["ListTitle"].ToString();
                        SPList _RequestsList = _web.Lists[_ListTitle];
                        SPListItem _RequestItem = _RequestsList.GetItemById(int.Parse(_TaskExtendedProperties["ItemID"].ToString()));

                        //_RequestItem["Request Type"] = rblRequestType.SelectedItem.Text;
                        //_RequestItem["Telephone No"] = txtTelephoneNo.Text;
                        //_RequestItem["OfficeLocationID"] = ddlOfficeLocation.Text;
                        //_RequestItem["Designation"] = txtDesignation.Text;
                        if (!String.IsNullOrEmpty(txtChangeNumber.Text))
                            _RequestItem["Remedy Change Number"] = SPHandler.RemoveHTMLTags(txtChangeNumber.Text);
                        if (!dtcDateRequired.IsDateEmpty)
                            _RequestItem["Date Required"] = dtcDateRequired.SelectedDate;

                        ////////////////
                        //if (LineManagerUser != null)
                        //{
                        //    SPFieldUserValue LineManagerUserValue = new SPFieldUserValue(_web, LineManagerUser.ID, LineManagerUser.LoginName);
                        //    _RequestItem["Line Manager"] = LineManagerUserValue;
                        //}
                        ///////////////////

                        if (lblRequestType.Text == "Server or Application Monitoring")
                        {
                            _RequestItem["Server Name"] = SPHandler.RemoveHTMLTags(txtServerName.Text);
                            _RequestItem["IP Address"] = SPHandler.RemoveHTMLTags(txtIPAddress.Text);

                            _RequestItem["Application Name"] = SPHandler.RemoveHTMLTags(txtApplicationName.Text);
                            _RequestItem["Is DMZ Server"] = bool.Parse(rblIsDMZServer.Text);
                            _RequestItem["Operating System"] = rblOperatingSystem.Text;
                            _RequestItem["Server Class"] = rblServerClass.Text;
                            _RequestItem["Global Zone"] = rblGlobalZone.Text;
                            _RequestItem["Oracle Installed"] = bool.Parse(rblOracleInstalled.Text);
                            _RequestItem["MS Exchange Installed"] = bool.Parse(rblMSExchangeInstalled.Text);
                            _RequestItem["MSSQL Installed"] = bool.Parse(rblMSSQLInstalled.Text);
                            if (!String.IsNullOrEmpty(txtOtherApplications.Text))
                                _RequestItem["Other Applications"] = SPHandler.RemoveHTMLTags(txtOtherApplications.Text);
                            if (!String.IsNullOrEmpty(txtLogFile.Text))
                                _RequestItem["Log File"] = SPHandler.RemoveHTMLTags(txtLogFile.Text);
                            if (!String.IsNullOrEmpty(txtSearchString_MonitoringConditionNote.Text))
                                _RequestItem["Search String / Monitoring Condition Note"] = SPHandler.RemoveHTMLTags(txtSearchString_MonitoringConditionNote.Text);
                            if (!String.IsNullOrEmpty(txtProcesses_Services.Text))
                                _RequestItem["Processes / Services"] = SPHandler.RemoveHTMLTags(txtProcesses_Services.Text);
                            if (!String.IsNullOrEmpty(txtCommandAndUserName.Text))
                                _RequestItem["Command and User Name"] = SPHandler.RemoveHTMLTags(txtCommandAndUserName.Text);
                        }
                        else//Control-M Job
                        {
                            _RequestItem["Script Name and Path"] = SPHandler.RemoveHTMLTags(txtScriptNameAndPath.Text);
                            _RequestItem["Job Description"] = SPHandler.RemoveHTMLTags(txtJobDescription.Text);
                            if (!String.IsNullOrEmpty(txtScriptLogFilePath.Text))
                                _RequestItem["Script Log File Path"] = SPHandler.RemoveHTMLTags(txtScriptLogFilePath.Text);
                            _RequestItem["Cyclic"] = rblCyclic.Text;
                            if (rblCyclic.Text != "No")
                                _RequestItem["Cyclic Restart Interval"] = SPHandler.RemoveHTMLTags(txtCyclicRestartInterval.Text);
                            _RequestItem["Frequency"] = SPHandler.RemoveHTMLTags(txtFrequency.Text);
                            _RequestItem["Condition"] = SPHandler.RemoveHTMLTags(txtCondition.Text);
                            if (!String.IsNullOrEmpty(txtTestServerName.Text))
                                _RequestItem["Test Server Name"] = SPHandler.RemoveHTMLTags(txtTestServerName.Text);
                            _RequestItem["Production Server Name"] = SPHandler.RemoveHTMLTags(txtProductionServerName.Text);
                            _RequestItem["Is The Server Behind Firewall"] = bool.Parse(rblIsTheServerBehindFirewall.Text);

                            List<SPUser> MailingListUsers = new List<SPUser>();
                            ///////////////////////////////////////
                            for (int i = 0; i < peMailingListUsedForNotification.ResolvedEntities.Count; i++)
                            {
                                PickerEntity TempEntity = (PickerEntity)peMailingListUsedForNotification.ResolvedEntities[i];
                                SPUser TempUser;
                                if (TempEntity.IsResolved == true)
                                    TempUser = _web.EnsureUser(TempEntity.Key);
                                else
                                    TempUser = null;
                                if (TempUser != null)
                                    MailingListUsers.Add(TempUser);
                            }
                            ///////////////////////////////////////

                            SPFieldUserValueCollection MailingListUserValues = new SPFieldUserValueCollection();
                            for (int i = 0; i < MailingListUsers.Count; i++)
                            {
                                SPFieldUserValue fuv = new SPFieldUserValue(_web, MailingListUsers[i].ID, MailingListUsers[i].LoginName);
                                MailingListUserValues.Add(fuv);
                            }
                            _RequestItem["Mailing List Used For Notification"] = MailingListUserValues;
                            _RequestItem["Remedy Group Used For Failure Notification"] = SPHandler.RemoveHTMLTags(txtRemedyGroup.Text);
                        }
                        _RequestItem.Update();
                        //File.WriteAllText(@"C:\Temp\UpdateApprove.log", _hash.Values.ToString());
                        ////////////////////////////////
                        _web.AllowUnsafeUpdates = false;

                        if (_web != null)
                            _web.Dispose();
                        if (_site != null)
                            _site.Dispose();
                    }
                     );
                }
                //if (_DisplayFormMode == "HRExec")
                //    _hash.Add("Actor", SPContext.Current.Web.CurrentUser.Name + "," + SPContext.Current.Web.CurrentUser.LoginName.Split('\\')[1]);

                SPWorkflowTask.AlterTask(SPContext.Current.ListItem, _hash, true);
                File.WriteAllText(@"C:\Temp\UpdateApprove.log", _hash.Values.ToString());
                SPContext.Current.Web.AllowUnsafeUpdates = false;
            }
            catch (Exception ex)
            {
                File.WriteAllText(@"C:\Temp\approve.log", ex.ToString());
                ErrorHandler.LogToWindows(ex, "ISESMServiceRequestWFTask, _btnApprove_Click");
                Success = false;
            }
            if (Success)
                GoBack();
        }

        protected void btnRequestChange_Click(object sender, EventArgs e)
        {
            bool Success = true;
            try
            { 
                if (base.Request.Params["ID"] != null)
                {
                    SPWeb CurrentWeb = SPContext.Current.Web;
                    CurrentWeb.AllowUnsafeUpdates = true;
                    _TaskList = CurrentWeb.Lists["ISESMServiceRequestTasks"];
                    _TaskListItem = _TaskList.GetItemById(int.Parse(base.Request.Params["ID"]));
                    _TaskExtendedProperties = SPWorkflowTask.GetExtendedPropertiesAsHashtable((SPListItem)_TaskListItem);
                    _ItemID = int.Parse(_TaskExtendedProperties["ItemID"].ToString());


                    using (SPSite site = new SPSite(SPContext.Current.Web.Url))
                    {
                        using (SPWeb Currentweb = site.OpenWeb())
                        {
                            _ListTitle = _TaskExtendedProperties["ListTitle"].ToString();
                            SPList lists = Currentweb.Lists["IS_ESM_Service_Requests"];
                            SPListItem item = lists.GetItemById(_ItemID);
                            SPFieldUserValue initiator = new SPFieldUserValue(CurrentWeb, item["Created By"].ToString());


                            string requestChangeLink = CurrentWeb.Url + "/_layouts/15/EsmServiceRequest/UpdateRequestPage.aspx?ID=" + _ItemID;
                            SPFieldUrlValue statusChange = new SPFieldUrlValue();
                            statusChange.Description = "Change Request";
                            statusChange.Url = requestChangeLink;

                            item["Change Requested"] = "Yes";
                            item["Change Requester"] = SPContext.Current.Web.CurrentUser.Name;
                            item["Change Requester Email"] = SPContext.Current.Web.CurrentUser.Email;
                            item["Change Request Stage"] = "1";
                            item["ChangeRequesterComment"] = txtComment.Text;
                            item["Status Stage"] = statusChange;
                            item["Tasks ID"] = base.Request.Params["ID"].ToString();
                            item.SystemUpdate();
                            item.Update();
                            //lists.Update();

                            //SendRequesterEmail                    
                            string to = initiator.User.Email;
                            string from = "workflows@mtn.com";
                            string changeRequester = SPContext.Current.Web.CurrentUser.Name;

                            string Body = "Dear " + initiator.User.Name + ",<br><br>";
                            //Body += "A new Request from " + _Requester.DisplayName + " is awaiting your kind approval. ";

                            Body += "A change have been requested on your Information Systems Management request by " + changeRequester + "<br><br>Request Change Comment: " + item["ChangeRequesterComment"].ToString() + ".<br><br> Click <a href='" + requestChangeLink + "'>here</a> to update the request. <br><br>";

                            Body += "<br><br>Kind Regards,<br>";
                            Body += "Information Systems Management";
                            Body += "<br><br>NOTE: This email message is auto generated, Please do not reply to the email address.";
                            EMailHandler.reqestChangeSendEmail(to, from, null, "Information Systems Change Request", Body);
                        }
                    }
                    CurrentWeb.AllowUnsafeUpdates = false;

                }
                Response.Redirect(SPContext.Current.Web.Url);
                //File.WriteAllText(@"C:\Temp\URLError.log", SPContext.Current.Web.Url);
            }
            catch (Exception ex)
            {
                File.WriteAllText(@"C:\Temp\RequestError.log", ex.ToString());
                ErrorHandler.LogToWindows(ex, "ISESMServiceRequestWFTask, _btnRequestChange_Click");
                Success = false;
            }
            if (Success)
                GoBack();
        }



        protected void btnReject_Click(object sender, EventArgs e)
        {
            bool Success = true;
            try
            {
                if (base.Request.Params["ID"] != null)
                {
                    SPWeb CurrentWeb = SPContext.Current.Web;
                    _TaskList = CurrentWeb.Lists["ISESMServiceRequestTasks"];
                    _TaskListItem = _TaskList.GetItemById(int.Parse(base.Request.Params["ID"]));
                    _TaskExtendedProperties = SPWorkflowTask.GetExtendedPropertiesAsHashtable((SPListItem)_TaskListItem);
                    _ItemID = int.Parse(_TaskExtendedProperties["ItemID"].ToString());

                    using (SPSite site = new SPSite(SPContext.Current.Web.Url))
                    {
                        using (SPWeb Currentweb = site.OpenWeb())
                        {
                            _ListTitle = _TaskExtendedProperties["ListTitle"].ToString();
                            SPList lists = CurrentWeb.Lists["IS_ESM_Service_Requests"];
                            SPListItem items = lists.GetItemById(_ItemID);

                            items["ChangeRequesterComment"] = txtComment.Text;
                            items.SystemUpdate();
                            items.Update();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                File.WriteAllText(@"C:\Temp\rejectError.log", ex.ToString());
            }

            try
            {
                _DisplayFormMode = (string)ViewState["DisplayFormMode"];
                SPContext.Current.Web.AllowUnsafeUpdates = true;
                string _comments = GetComments();
                Hashtable _hash = new Hashtable();
                _hash.Add("CurrentUserId", SPContext.Current.Web.CurrentUser.ID);
                _hash.Add("Action", "Rejected");
                //_hash.Add("Comments", _comments);                

                //if (_DisplayFormMode == "HRRecruitAdmin" && _TaskExtendedProperties["OD"].ToString() == "FirstTime")
                //{
                //    string OD = _DisplayRRUserControl.GetPeopleEditor();
                //    if (OD == "")
                //        return;
                //    else
                //        _hash.Add("ODName", _DisplayRRUserControl.GetPeopleEditor());
                //}
                //if (_DisplayFormMode == "HRExec")
                //    _hash.Add("Actor", SPContext.Current.Web.CurrentUser.Name + "," + SPContext.Current.Web.CurrentUser.LoginName.Split('\\')[1]);
                SPWorkflowTask.AlterTask(SPContext.Current.ListItem, _hash, true);
                                
            }
            catch (Exception ex)
            {
                File.WriteAllText(@"C:\Temp\rejectError3.log", ex.ToString());
                ErrorHandler.LogToWindows(ex, "ISESMServiceRequestWFTask, _btnReject_Click");
                Success = false;
            }           

            SPContext.Current.Web.AllowUnsafeUpdates = false;
            if (Success)
                GoBack();
        } 

        protected void btnClose_Click(object sender, EventArgs e)
        {
            GoBack();
        }

        void GoBack()
        {
            if (Page.Request.QueryString["Source"] != null)
                //Response.Redirect(Page.Request.QueryString["Source"].ToString());
                Response.Redirect(SPContext.Current.Web.Url);

            //Response.Redirect(Page.Request.QueryString["Source"].ToString());
            else
                Response.Redirect(SPContext.Current.Web.Url);
            //Response.Redirect(Web.Url + "/_layouts/EsmServiceRequest/NewForm.aspx");
            //Response.Redirect("
            //https
            //://sharepoint.mtnnigeria.net/sites/InformationSystems#/playlist1/local1");
        }       

        public string GetComments()
        {
            System.Text.StringBuilder comments = new System.Text.StringBuilder(ltlCommentsHistory.Text);
            if (!String.IsNullOrEmpty(txtComment.Text))
            {
                comments.Append("<table width='100%' cellpadding='4' cellspacing='0' border='1' class='commentstablestyle'>");
                comments.Append("<tr>");
                comments.Append("<td width='140' valign='top'>");
                comments.Append("<strong>");
                comments.Append(SPContext.Current.Web.CurrentUser.Name);
                comments.Append("</strong><br>");
                comments.Append("<span class='commentsdate'>(");
                comments.Append(DateTime.Now.ToString("dd MMM yyyy hh:mm:ss"));
                comments.Append(")</span>");
                comments.Append("</td>");
                comments.Append("<td>");
                comments.Append(SPHandler.RemoveHTMLTags(txtComment.Text).Replace("\r\n", "<br>"));
                comments.Append("</td>");
                comments.Append("</tr>");
                comments.Append("</table>");
            }
            return comments.ToString();
        }
    }
    public enum ISESMServiceRequestWFRecjector
    {
        LineManager = 0, ESMTeam = 1, ESMManager = 2
    }
}
