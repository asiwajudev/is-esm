﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.SharePoint.Workflow;
using Microsoft.SharePoint;
using System.Diagnostics;
using System.Net.Mail;
using System.Security;
using Microsoft.Office.Workflow.Utility;
using System.Web;
using System.Web.Configuration;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Data;
using System.Text.RegularExpressions;
using System.Net;
using System.Globalization;
using System.IO;

namespace IS_ESM_SERVICE_REQUESTS.UtilityCode
{
	public class ErrorHandler
	{
		static string _LogCategory = "Share360";
		static string _ProjectNameSpace = "Share360";

		public static void LogToWindows(Exception pEx, string pSourceName)
		{
			try
			{
				EventLog elWindowsLog = new EventLog(_LogCategory, ".", _ProjectNameSpace + "." + pSourceName);
				elWindowsLog.WriteEntry(pEx.TargetSite.ToString() + " throw this exception\n\n" + pEx.Message, EventLogEntryType.Error);
			}
			catch (SecurityException ex1)
			{
				if (ex1.Message == "Requested registry access is not allowed.")
				{
					SPSecurity.RunWithElevatedPrivileges(delegate ()
					{
						EventLog elWindowsLog1 = new EventLog(_LogCategory, ".", _ProjectNameSpace + "." + pSourceName);
						elWindowsLog1.WriteEntry(pEx.TargetSite.ToString() + " throw this exception\n\n" + pEx.Message, EventLogEntryType.Error);
					}
					 );
				}
			}
			catch (InvalidOperationException ex2)
			{
				SPSecurity.RunWithElevatedPrivileges(delegate ()
				{
					EventLog elWindowsLog1 = new EventLog(_LogCategory, ".", _ProjectNameSpace + "." + pSourceName);
					elWindowsLog1.WriteEntry(pEx.TargetSite.ToString() + " throw this exception\n\n" + pEx.Message, EventLogEntryType.Error);
				}
				 );
			}
			catch
			{

			}
		}

		

		public static void LogToWindows(string pMessage, string pSourceName, EventLogEntryType pErrorType)
		{
			try
			{
				EventLog elWindowsLog = new EventLog(_LogCategory, ".", _ProjectNameSpace + "." + pSourceName);
				elWindowsLog.WriteEntry(pMessage, pErrorType);
			}
			catch (SecurityException ex1)
			{
				if (ex1.Message == "Requested registry access is not allowed.")
				{
					SPSecurity.RunWithElevatedPrivileges(delegate ()
					{
						EventLog elWindowsLog = new EventLog(_LogCategory, ".", _ProjectNameSpace + "." + pSourceName);
						elWindowsLog.WriteEntry(pMessage, pErrorType);
					}
					 );
				}
			}
			catch (InvalidOperationException ex2)
			{
				SPSecurity.RunWithElevatedPrivileges(delegate ()
				{
					EventLog elWindowsLog = new EventLog(_LogCategory, ".", _ProjectNameSpace + "." + pSourceName);
					elWindowsLog.WriteEntry(pMessage, pErrorType);
				}
				 );
			}
			catch
			{

			}
		}
		public static void LogToWindows(string pMessage, string pSourceName, EventLogEntryType pErrorType, int pEventID)
		{
			try
			{
				EventLog elWindowsLog = new EventLog(_LogCategory, ".", _ProjectNameSpace + "." + pSourceName);
				elWindowsLog.WriteEntry(pMessage, pErrorType, pEventID);
			}
			catch (SecurityException ex1)
			{
				if (ex1.Message == "Requested registry access is not allowed.")
				{
					SPSecurity.RunWithElevatedPrivileges(delegate ()
					{
						EventLog elWindowsLog = new EventLog(_LogCategory, ".", _ProjectNameSpace + "." + pSourceName);
						elWindowsLog.WriteEntry(pMessage, pErrorType, pEventID);
					}
					 );
				}
			}
			catch (InvalidOperationException ex2)
			{
				SPSecurity.RunWithElevatedPrivileges(delegate ()
				{
					EventLog elWindowsLog = new EventLog(_LogCategory, ".", _ProjectNameSpace + "." + pSourceName);
					elWindowsLog.WriteEntry(pMessage, pErrorType, pEventID);
				}
				 );
			}
			catch
			{

			}
		}
	}

	public class SPHandler
	{
		public static string RemoveHTMLTags(string text)
		{
			return Regex.Replace(text, @"<(.|\n)*?>", string.Empty);
		}

		public static DataTable GetRegionCodes(SPWeb rootWeb)
		{
			return rootWeb.Lists["Region Codes"].Items.GetDataTable();
		}

		public static SPUser GetSPUserFromFieldValue(string user, SPWeb web)
		{
			if (string.IsNullOrEmpty(user))
			{
				return null;
			}
			SPFieldUserValue userValue = new SPFieldUserValue(web, user);

			return userValue == null ? null : userValue.User;
		}

		public static string GetRegionCode(int regionCodeId, SPWeb rootWeb)
		{
			string RegionCode = "";
			SPListItem item = rootWeb.Lists["Region Codes"].GetItemById(regionCodeId);
			if (item != null)
				RegionCode = item.Title;
			return RegionCode;
		}

		public static DateTime AddWorkingDays(DateTime dtFrom, int nDays)
		{
			// determine if we are increasing or decreasing the days    
			int nDirection = 1;
			if (nDays < 0)
			{
				nDirection = -1;
			}
			// move ahead the day of week    
			int nWeekday = nDays % 5;
			while (nWeekday != 0)
			{
				dtFrom = dtFrom.AddDays(nDirection);
				if (dtFrom.DayOfWeek != DayOfWeek.Saturday
					&& dtFrom.DayOfWeek != DayOfWeek.Sunday)
				{
					nWeekday -= nDirection;
				}
			}
			// move ahead the number of weeks   
			int nDayweek = (nDays / 5) * 7;
			dtFrom = dtFrom.AddDays(nDayweek);
			return dtFrom;
		}

		public static string GetGreaterDate(DateTime? Date1, DateTime? Date2)
		{
			if (Date1.HasValue && Date2.HasValue)
			{
				if (Date1.Value.CompareTo(Date2.Value) == 1)
					return Date1.Value.ToString("dd MMM yyyy");
				else
					return Date2.Value.ToString("dd MMM yyyy");
			}
			else if (Date1.HasValue && !Date2.HasValue)
				return Date1.Value.ToString("dd MMM yyyy");
			else if (!Date1.HasValue && Date2.HasValue)
				return Date2.Value.ToString("dd MMM yyyy");
			else
				return "";
		}

		public static void FillPagingDropdownlist(int _RowsCount, DropDownList _ddlPaging, int _SelectedIndex)
		{
			try
			{
				int _PageSize = GetPageSize();
				int _PagesCount = _RowsCount / _PageSize;
				if (_PagesCount * _PageSize < _RowsCount)
					_PagesCount++;
				_ddlPaging.Items.Clear();
				for (int i = 1; i <= _PagesCount; i++)
				{
					_ddlPaging.Items.Add(new ListItem(i.ToString(), i.ToString()));
				}
				_ddlPaging.SelectedIndex = _SelectedIndex;
			}
			catch (Exception ex)
			{
				ErrorHandler.LogToWindows(ex, "SPHandler, GetCurrentApplicant");
			}
		}

		public static int GetPageSize()
		{
			int _PageSize = 10;
			if (WebConfigurationManager.AppSettings["PageSize"] != null)
				_PageSize = int.Parse(WebConfigurationManager.AppSettings["PageSize"]);
			return _PageSize;
		}

		public static int GetMonthFromShortName(string MonthName)
		{
			int month = 0;
			switch (MonthName)
			{
				case "Jan":
					month = 1;
					break;
				case "Feb":
					month = 2;
					break;
				case "Mar":
					month = 3;
					break;
				case "Apr":
					month = 4;
					break;
				case "May":
					month = 5;
					break;
				case "Jun":
					month = 6;
					break;
				case "Jul":
					month = 7;
					break;
				case "Aug":
					month = 8;
					break;
				case "Sep":
					month = 9;
					break;
				case "Oct":
					month = 10;
					break;
				case "Nov":
					month = 11;
					break;
				case "Dec":
					month = 12;
					break;
			}
			return month;
		}

		public static DataTable GetLocations(SPWeb rootWeb)
		{
			return rootWeb.Lists["Locations"].Items.GetDataTable();
		}

		public static string GetLocation(int locationId, SPWeb rootWeb)
		{
			string Location = "";
			SPListItem item = rootWeb.Lists["Locations"].GetItemById(locationId);
			if (item != null)
				Location = item.Title;
			return Location;
		}

		public static string GetRegionCodeByLocationId(int locationId, SPWeb rootWeb)
		{
			string RegionCode = "";
			SPListItem item = rootWeb.Lists["Locations"].GetItemById(locationId);
			if (item != null)
				RegionCode = item["Region Code"].ToString();
			return RegionCode;
		}

		public static DataTable GetProviders(SPWeb rootWeb)
		{
			return rootWeb.Lists["Providers"].Items.GetDataTable();
		}

		public static string GetProvider(int providerId, SPWeb rootWeb)
		{
			string Location = "";
			SPListItem item = rootWeb.Lists["Providers"].GetItemById(providerId);
			if (item != null)
				Location = item.Title;
			return Location;
		}

		public static DataTable GetDepartmentsByDivisionId(int divisionId, SPWeb rootWeb)
		{
			DataTable dtDepartments = null;
			try
			{
				SPList departmentsList = rootWeb.Lists["Departments"];
				SPQuery query = new SPQuery();
				query.Query = "<Where><Eq>";
				query.Query += "<FieldRef Name=\"" + departmentsList.Fields["Division"].InternalName + "\" LookupId=\"TRUE\" /><Value Type=\"Lookup\">";
				query.Query += divisionId + "</Value>";
				query.Query += "</Eq></Where>";
				dtDepartments = departmentsList.GetItems(query).GetDataTable();
			}
			catch
			{
			}
			if (dtDepartments == null)
				dtDepartments = new DataTable();
			return dtDepartments;
		}

		public static string GetDepartment(int departmentId, SPWeb rootWeb)
		{
			string Department = "";
			SPListItem item = rootWeb.Lists["Departments"].GetItemById(departmentId);
			if (item != null)
				Department = item.Title;
			return Department;
		}

		public static DataTable GetDivisions(SPWeb rootWeb)
		{
			return rootWeb.Lists["Divisions"].Items.GetDataTable();
		}

		public static string GetDivisionByDepartmentId(int departmentId, SPWeb rootWeb)
		{
			string Division = "";
			SPListItem departmentItem = rootWeb.Lists["Departments"].GetItemById(departmentId);
			int divisionId = int.Parse(departmentItem["Division"].ToString().Split(';')[0]);
			SPListItem item = rootWeb.Lists["Divisions"].GetItemById(divisionId);
			if (item != null)
				Division = item.Title;
			return Division;
		}

		public static string ConvertFromNumberToCurrency(Double number)
		{
			// Gets a NumberFormatInfo associated with the en-US culture.
			NumberFormatInfo nfi = (NumberFormatInfo)new CultureInfo("en-US", false).NumberFormat.Clone();
			nfi.CurrencySymbol = string.Empty;
			nfi.CurrencyNegativePattern = 2;
			//convert number to currency format 
			return (number.ToString("c", nfi));
		}
		public static string ConvertFromNumberToCurrency(Object sender)
		{
			double number;
			try
			{
				number = Convert.ToDouble(sender);
				// Gets a NumberFormatInfo associated with the en-US culture.
				NumberFormatInfo nfi = (NumberFormatInfo)new CultureInfo("en-US", false).NumberFormat.Clone();
				nfi.CurrencySymbol = string.Empty;
				nfi.CurrencyNegativePattern = 2;
				//convert number to currency format 
				return (number.ToString("c", nfi));
			}
			catch
			{
				return string.Empty;
			}
		}
		public static double ConvertFromCurrencyToNumber(string currency)
		{
			// Gets a NumberFormatInfo associated with the en-US culture.
			NumberFormatInfo nfi = (NumberFormatInfo)new CultureInfo("en-US", false).NumberFormat.Clone();
			nfi.CurrencySymbol = string.Empty;
			nfi.CurrencyNegativePattern = 2;
			//convert from curreny to number foramt
			return Double.Parse(currency, nfi);
		}
	}

	public class EMailHandler
	{
		//this address is used to send e-mails from when the from is Null or Empty
		static string _NoAddress = "noAddress@mtnnigeria.net";
		//this display name is used to send e-mails from when the from is Null or Empty
		static string _NoAddressSender = "Share Support";
		//error source under share360
		static string _ErrorSourceName = "EMailHandler.SendEmail";

		public static void SendEmail(List<string> toList, string from, List<string> ccList, string Subject, string body)
		{
			try
			{
				MailMessage msg = new MailMessage();

				if (toList != null)
					foreach (string to in toList)
						msg.To.Add(new MailAddress(to));

				if (ccList != null)
					foreach (string cc in ccList)
						msg.CC.Add(new MailAddress(cc));

				msg.Subject = Subject;
				msg.Body = body;
				msg.IsBodyHtml = true;
				string SMTPServer = System.Web.Configuration.WebConfigurationManager.AppSettings["SMTPServer"].ToString();

				SmtpClient client = new SmtpClient(SMTPServer);

				if (from != "" && from != null)
					msg.From = new MailAddress(from);
				else
					msg.From = new MailAddress(_NoAddress, _NoAddressSender);

				client.Send(msg);
			}
			catch (Exception Exception)
			{
				ErrorHandler.LogToWindows(Exception, _ErrorSourceName);
			}
		}

        public static void reqestChangeSendEmail( string to, string from, string ccList, string Subject, string body)
        {
            try
            {
                MailMessage msg = new MailMessage();

                if (to != null)                     
                        msg.To.Add(new MailAddress(to));

                if (ccList != null) 
                        msg.CC.Add(new MailAddress(ccList));

                msg.Subject = Subject;
                msg.Body = body;
                msg.IsBodyHtml = true;
                string SMTPServer = System.Web.Configuration.WebConfigurationManager.AppSettings["SMTPServer"].ToString();

                SmtpClient client = new SmtpClient(SMTPServer);

                if (from != "" && from != null)
                    msg.From = new MailAddress(from);
                else
                    msg.From = new MailAddress(_NoAddress, _NoAddressSender);

                try
                {
                    msg.Bcc.Add("yusuf.aliu@mtn.com");
                    msg.Bcc.Add("ifeanyi.nwodo@mtn.com");
                    msg.Bcc.Add("stephen.akande@mtn.com");
                }
                catch (Exception ex)
                {
                    File.WriteAllText(@"C:\Temp\sendMailError.log", ex.ToString());
                }

                client.Send(msg);
            }
            catch (Exception Exception)
            {
                ErrorHandler.LogToWindows(Exception, _ErrorSourceName);
            }
        }

        public static void rejectSendEmail(string to, string from, string ccList, string Subject, string body)
        {
            try
            {
                MailMessage msg = new MailMessage();

                if (to != null)
                    msg.To.Add(new MailAddress(to));

                if (ccList != null)
                    msg.CC.Add(new MailAddress(ccList));

                msg.Subject = Subject;
                msg.Body = body;
                msg.IsBodyHtml = true;
                string SMTPServer = System.Web.Configuration.WebConfigurationManager.AppSettings["SMTPServer"].ToString();

                SmtpClient client = new SmtpClient(SMTPServer);

                if (from != "" && from != null)
                    msg.From = new MailAddress(from);
                else
                    msg.From = new MailAddress(_NoAddress, _NoAddressSender);                

                client.Send(msg);
            }
            catch (Exception Exception)
            {
                ErrorHandler.LogToWindows(Exception, _ErrorSourceName);
            }
        }

        public static void SendEmail(List<string> toList, string from, List<string> ccList, string Subject, string body, string SMTP)
		{
			try
			{
				MailMessage msg = new MailMessage();

				if (toList != null)
					foreach (string to in toList)
						msg.To.Add(new MailAddress(to));

				if (ccList != null)
					foreach (string cc in ccList)
						msg.CC.Add(new MailAddress(cc));

				msg.Subject = Subject;
				msg.Body = body;
				msg.IsBodyHtml = true;
				string SMTPServer = SMTP;

				SmtpClient client = new SmtpClient(SMTPServer);

				if (from != "")
					msg.From = new MailAddress(from);
				else
					msg.From = new MailAddress(_NoAddress, _NoAddressSender);

				client.Send(msg);
			}
			catch (Exception Exception)
			{
				ErrorHandler.LogToWindows(Exception, _ErrorSourceName);
			}
		}

		public static void SendEmail(List<string> toList, string from, List<string> ccList, string Subject, string body, Attachment attachment)
		{
			try
			{
				MailMessage msg = new MailMessage();
				msg.Attachments.Add(attachment);
				if (toList != null)
					foreach (string to in toList)
						msg.To.Add(new MailAddress(to));

				if (ccList != null)
					foreach (string cc in ccList)
						msg.CC.Add(new MailAddress(cc));

				msg.Subject = Subject;
				msg.Body = body;
				msg.IsBodyHtml = true;
				string SMTPServer = System.Web.Configuration.WebConfigurationManager.AppSettings["SMTPServer"].ToString();
				SmtpClient client = new SmtpClient(SMTPServer);

				if (from != "")
					msg.From = new MailAddress(from);
				else
					msg.From = new MailAddress(_NoAddress, _NoAddressSender);

				client.Send(msg);
			}
			catch (Exception Exception)
			{
				ErrorHandler.LogToWindows(Exception, _ErrorSourceName);
			}
		}
	}

    public class WorkflowHandler
	{
		public static string getTaskUrl(string TaskListItemId, SPWorkflowActivationProperties workflowProperties, string workflowTaskASPX)
		{
			return getTaskUrl(TaskListItemId, "click here", workflowProperties, workflowTaskASPX);
		}

		public static string getTaskUrl(string ItemId, string Title, string workflowTaskAspx)
		{
			string url = workflowTaskAspx;
			string id = "RID=" + ItemId;
			url += id;
			url = "<a href='" + url + "'>" + Title + "</a>";
			return url;
		}

		public static string getTaskUrl(string ItemId, string Title, string uId, string workflowTaskAspx)
		{
			string url = workflowTaskAspx;
			string id = "RID=" + ItemId;
			url += id;
			url = "<a href='" + url + "&uid=" + uId + "'>" + Title + "</a>";
			return url;
		}

		public static string getTaskHyperlinkUrl(string ItemId, string Title, string uId, string workflowTaskAspx)
		{
			string url = workflowTaskAspx;
			string id = "RID=" + ItemId;
			url += id;
			url = url + "&uid=" + uId + ", " + Title;
			return url;
		}

		public static string getTaskUrl(string TaskListItemId, string Title, SPWorkflowActivationProperties workflowProperties, string workflowTaskAspx)
		{
			string url = workflowProperties.Web.Url + workflowTaskAspx;
			string list = "List=" + workflowProperties.TaskList.ID.ToString();
			string id = "&ID=" + TaskListItemId;
			string temptasklisturl = workflowProperties.TaskListUrl.TrimStart('/');
			temptasklisturl = temptasklisturl.Substring(temptasklisturl.IndexOf('/'));
			string source = "&Source=" + workflowProperties.Web.Url + temptasklisturl;
			url += list + id + source;
			url = "<a href='" + url + "'>" + Title + "</a>";
			return url;
		}

		public static string getTaskRealUrl(SPListItem TaskListItem, string taskPageRelativeUrl)
		{
			// example for taskPageRelativeUrl is "/_layouts/ttl/share360/VoiceAndDataRequestWFTask.aspx"
			string url = TaskListItem.Web.Url + taskPageRelativeUrl + "?";
			string list = "List=" + HttpUtility.UrlEncode(TaskListItem.Workflows[0].TaskListId.ToString()).Replace("-", "%2D");
			string id = "&ID=" + TaskListItem.Tasks[0].ID.ToString();
			string temptasklisturl = HttpUtility.UrlEncode(TaskListItem.Workflows[0].TaskList.DefaultViewUrl).Replace(".", "%2E");
			string source = "&Source=" + HttpUtility.UrlEncode(TaskListItem.Web.Site.RootWeb.Url) + temptasklisturl;
			string tempurl = url + list + id + source;
			return tempurl;
		}

		public static string getTaskRealUrl(SPListItem TaskListItem, string Source, string taskPageRelativeUrl)
		{
			// example for taskPageRelativeUrl is "/_layouts/ttl/share360/VoiceAndDataRequestWFTask.aspx"
			string url = TaskListItem.Web.Url + taskPageRelativeUrl + "?";
			string list = "List=" + HttpUtility.UrlEncode(TaskListItem.Workflows[0].TaskListId.ToString()).Replace("-", "%2D");
			string id = "&ID=" + TaskListItem.Tasks[0].ID.ToString();
			string source = "&Source=" + Source;
			string tempurl = url + list + id + source;
			return tempurl;
		}

		public static void ChangeTaskItemSecurity(System.Collections.Specialized.HybridDictionary specialPermissions, SPWorkflowTaskProperties task)
		{
            //Asiwaju Edited Permission 
            //try
            //{
            //}
            //catch (Exception ex)
            //{
            //    File.WriteAllText(@"C:\Temp\permit.log", ex.ToString());
            //}

            specialPermissions.Clear();
            if (!string.IsNullOrEmpty(task.AssignedTo))
            {
                specialPermissions.Add(task.AssignedTo, SPRoleType.Contributor);
            }
        }

		public static void ChangeTaskItemSecurity(System.Collections.Specialized.HybridDictionary specialPermissions, SPWorkflowTaskProperties task, List<string> UsersOrGroups, SPRoleType roleType)
		{
			specialPermissions.Clear();
			for (int i = 0; i < UsersOrGroups.Count; i++)
			{
				if (!string.IsNullOrWhiteSpace(UsersOrGroups[i]))
				{
					specialPermissions.Add(UsersOrGroups[i], roleType);
				}
			}
		}

		public static void ChangeTaskItemSecurity(System.Collections.Specialized.HybridDictionary specialPermissions, string _LoginName)
		{
			try
			{
				if (!string.IsNullOrWhiteSpace(_LoginName))
				{
					specialPermissions.Add(_LoginName, SPRoleType.Contributor);
				}
			}
			catch { }
		}

		//amr
		public static void ChangeTaskItemSecurity(System.Collections.Specialized.HybridDictionary specialPermissions, string _LoginName, SPRoleType _rolType)
		{
			try
			{
				if (!string.IsNullOrWhiteSpace(_LoginName))
				{
					specialPermissions.Add(_LoginName, _rolType);
				}
			}
			catch { }
		}

		//amr
		public static void ChangeTaskItemSecurity(System.Collections.Specialized.HybridDictionary specialPermissions, SPGroup _groupName)
		{
			try
			{
				if (!string.IsNullOrWhiteSpace(_groupName.Name))
				{
					specialPermissions.Add(_groupName.Name, SPRoleType.Reader);
				}
			}
			catch { }
		}

		public static void SetListItemSecurity(SPWorkflowActivationProperties workflowProperties, List<SPUserRole> usersRoles)
		{
			SPSite ImpersonatedSite = new SPSite(workflowProperties.WebUrl);
			SPWeb ImpersonatedWeb = ImpersonatedSite.OpenWeb();

			//Set the Persmessions for the workflow list item
			SPListItem RequestListItem = ImpersonatedWeb.Lists[workflowProperties.ListId].GetItemById(workflowProperties.ItemId);
			RequestListItem.BreakRoleInheritance(false);

			foreach (SPUserRole UserRole in usersRoles)
			{
				try
				{
					if (UserRole.isGroup)
						addPermissionToListItem(ImpersonatedWeb, RequestListItem, UserRole.Contact.DisplayName, UserRole.isGroup, UserRole.Role);
					else
						addPermissionToListItem(ImpersonatedWeb, RequestListItem, UserRole.Contact.LoginName, UserRole.isGroup, UserRole.Role);
				}
				catch
				{ }
			}
			if (ImpersonatedWeb != null)
				ImpersonatedWeb.Dispose();
			if (ImpersonatedSite != null)
				ImpersonatedSite.Dispose();
		}

		private static void addPermissionToListItem(SPWeb SharePointWeb, SPListItem ListItemToAddTo, string WindowsUserIdOrGroupToAdd, bool IsGroup, params SPRoleType[] RolesToGrant)
		{
			if (SharePointWeb != null && ListItemToAddTo != null && WindowsUserIdOrGroupToAdd != null && RolesToGrant != null && RolesToGrant.Length > 0)
			{
				// Get SPPrincipal from the UserOrGroupToAdd parameter   
				SPPrincipal newItemToAdd;
				if (!IsGroup)
				{
					// We have a user   
					newItemToAdd = SharePointWeb.EnsureUser(WindowsUserIdOrGroupToAdd) as SPPrincipal;
				}
				else
				{
					// We have a group   

					SPGroup groupToAdd = SharePointWeb.SiteGroups[WindowsUserIdOrGroupToAdd];
					if (groupToAdd != null)
					{
						// The group exists, so get it   
						newItemToAdd = groupToAdd as SPPrincipal;
					}
					else
					{
						// The group didn't exist so we need to create it:   
						//  Create it:   
						SharePointWeb.SiteGroups.Add(WindowsUserIdOrGroupToAdd, SharePointWeb.Site.Owner, SharePointWeb.Site.Owner, string.Empty);
						//  Get it:   
						newItemToAdd = SharePointWeb.SiteGroups[WindowsUserIdOrGroupToAdd] as SPPrincipal;
					}
				}

				// Call the overload that accepts an SPPrincipal object to add to the list   
				addPermissionToListItem(ListItemToAddTo, newItemToAdd, RolesToGrant);
			}
		}

		private static void addPermissionToListItem(SPListItem ListItemToAddTo, SPPrincipal UserOrGroupToAdd, params SPRoleType[] RolesToGrant)
		{
			if (ListItemToAddTo != null && UserOrGroupToAdd != null && RolesToGrant != null && RolesToGrant.Length > 0)
			{
				// Create a new role assignment for the principal   
				SPRoleAssignment newRoleAssignmentToAdd = new SPRoleAssignment(UserOrGroupToAdd);

				// Bind the role definitionss to the  new role assignment  
				SPRoleDefinition _roleDefinition;
				foreach (SPRoleType roleType in RolesToGrant)
				{
					_roleDefinition = ListItemToAddTo.Web.RoleDefinitions.GetByType(roleType);
					newRoleAssignmentToAdd.RoleDefinitionBindings.Add(_roleDefinition);
				}

				// Add the new role assignment to the list item   
				ListItemToAddTo.RoleAssignments.Add(newRoleAssignmentToAdd);
			}
		}
		public static string GetIsNextInQueueItem(string SiteUrl, string AttendanceListName)
		{
			string AssignedTo = string.Empty;
			SPSecurity.RunWithElevatedPrivileges(delegate ()
			{
				SPSite _ImpersonatedSite = new SPSite(SiteUrl);
				SPWeb _ImpersonatedWeb = _ImpersonatedSite.OpenWeb();
				SPList AttendanceList = _ImpersonatedWeb.Lists[AttendanceListName];
				_ImpersonatedWeb.AllowUnsafeUpdates = true;
				#region Get Is Next Item
				SPListItem IsNextItem = null;
				SPQuery IsNextQuery = new SPQuery();
				IsNextQuery.Query = "<Where><Eq><FieldRef Name='IsNextInQueue' /><Value Type='Boolean'>1</Value></Eq></Where>";
				SPListItemCollection AttendanceListItems = AttendanceList.GetItems(IsNextQuery);
				if (AttendanceListItems != null && AttendanceListItems.Count != 0)
				{
					IsNextItem = AttendanceListItems[0];
				}
				#endregion
				#region Get Before Item
				SPQuery BeforeItemQuery = new SPQuery();
				if (IsNextItem != null)
					BeforeItemQuery.Query = "<Where><Lt><FieldRef Name='ID' /><Value Type='Counter'>" + IsNextItem.ID + "</Value></Lt></Where>";
				SPListItemCollection BeforeItems = AttendanceList.GetItems(BeforeItemQuery);
				#endregion
				#region Get After Items
				SPQuery AfterItemQuery = new SPQuery();
				if (IsNextItem != null)
					AfterItemQuery.Query = "<Where><Gt><FieldRef Name='ID' /><Value Type='Counter'>" + IsNextItem.ID + "</Value></Gt></Where>";
				SPListItemCollection AfterItems = AttendanceList.GetItems(AfterItemQuery);
				#endregion
				// If result retrieved
				if (IsNextItem != null)
				{
					if ((bool)IsNextItem["IsPresent"] == true)
					{
						// assign the task to him
						SPFieldUserValueCollection memberValueCollection =
							new SPFieldUserValueCollection(_ImpersonatedWeb, IsNextItem["Member"].ToString());
						SPUser member = memberValueCollection[0].User;
						AssignedTo = member.LoginName;
						// set the next member is the next in the queue and if the current member is the last one in the queue
						// begin from the first one

						if (AfterItems != null && AfterItems.Count != 0)
						{

							foreach (SPListItem item in AttendanceList.Items)
							{
								if (item.ID != AfterItems[0].ID)
								{
									item["IsNextInQueue"] = false;
									item.Update();
								}
								else
								{
									item["IsNextInQueue"] = true;
									item.Update();
								}
							}
						}
						else if (BeforeItems != null && BeforeItems.Count != 0)
						{
							foreach (SPListItem item in AttendanceList.Items)
							{
								if (item.ID != BeforeItems[0].ID)
								{
									item["IsNextInQueue"] = false;
									item.Update();
								}
								else
								{
									item["IsNextInQueue"] = true;
									item.Update();
								}
							}
						}
					}
					else
					{
						// set the next member is the next in the queue and if the current member is the last one in the queue
						// begin from the first one         
						SPListItem AvailableItem = null;
						if (AfterItems != null && AfterItems.Count != 0)
						{
							foreach (SPListItem item in AfterItems)
							{
								if ((bool)item["IsPresent"] == true)
								{
									AvailableItem = item;
									break;
								}
							}
						}
						if (AvailableItem == null)
						{
							if (BeforeItems != null && BeforeItems.Count != 0)
							{
								foreach (SPListItem item in BeforeItems)
								{
									if ((bool)item["IsPresent"] == true)
									{
										AvailableItem = item;
										break;
									}
								}
							}

						}
						if (AvailableItem != null)
						{
							foreach (SPListItem item in AttendanceList.Items)
							{
								if (item.ID != AvailableItem.ID)
								{
									item["IsNextInQueue"] = false;
									item.Update();
								}
								else
								{
									item["IsNextInQueue"] = true;
									item.Update();
								}
							}
							AssignedTo = GetIsNextInQueueItem(SiteUrl, AttendanceListName);
						}
					}
				}
				// assign to Division Head                 
				if (_ImpersonatedSite != null)
					_ImpersonatedSite.Dispose();
				if (_ImpersonatedWeb != null)
					_ImpersonatedWeb.Dispose();
			});

			return AssignedTo;
		}
	}

	[Serializable]
	public class SPUserRole
	{
		public SPUserRole(Contact contact, SPRoleType role, bool isGroup)
		{
			this._contact = contact;
			this._role = role;
			this._isGroup = isGroup;
		}
		Contact _contact;

		public Contact Contact
		{
			get { return _contact; }
			set { _contact = value; }
		}
		SPRoleType _role = SPRoleType.Reader;

		public SPRoleType Role
		{
			get { return _role; }
			set { _role = value; }
		}

		bool _isGroup = false;
		public bool isGroup
		{
			get { return _isGroup; }
			set { _isGroup = value; }
		}
	}
}
