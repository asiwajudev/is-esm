﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IS_ESM_SERVICE_REQUESTS
{
	class LogHelper
	{
		public string GetTempPath()
		{
			string path = System.Environment.GetEnvironmentVariable("TEMP");
			if (!path.EndsWith("\\")) path += "\\";
			return path;
		}

		public static void LogMessageToFile(string msg)
		{
			System.IO.StreamWriter sw = System.IO.File.AppendText("C:\\Temp\\Logger.txt");  // Change
			try
			{
				string logLine = System.String.Format(
					"{0:G}: {1}.", System.DateTime.Now, msg);
				sw.WriteLine(logLine);
			}
			finally
			{
				sw.Close();
			}
		}

		public static void LogMessageToFile(Exception ex)
		{
			System.IO.StreamWriter sw = System.IO.File.AppendText(@"C:\Temp\Logger.txt");
			try
			{
				string logLine = System.String.Format(
					"{0:G}: {1}.", System.DateTime.Now, ex.StackTrace);
				sw.WriteLine(logLine);
				string logLine2 = System.String.Format(
				"{0:G}: {1}.", System.DateTime.Now, ex.Message);
				sw.WriteLine(logLine2);
			}
			finally
			{
				sw.Close();
			}
		}
	}
}
