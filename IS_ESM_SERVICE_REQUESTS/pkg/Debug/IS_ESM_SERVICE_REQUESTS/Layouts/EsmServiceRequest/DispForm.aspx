﻿<%@ Assembly Name="IS_ESM_SERVICE_REQUESTS, Version=1.0.0.0, Culture=neutral, PublicKeyToken=4fb6b353b68afa84" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DispForm.aspx.cs" Inherits="IS_ESM_SERVICE_REQUESTS.Layouts.IS_ESM_SERVICE_REQUESTS.DispForm" DynamicMasterPageFile="~masterurl/default.master" %>

<asp:Content ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
    <SharePoint:ListFormPageTitle runat="server" />
</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
    <SharePoint:ListProperty Property="LinkTitle" runat="server" ID="ID_LinkTitle" />
    :Disp Form
    <SharePoint:ListItemProperty ID="ID_ItemProperty" MaxLength="40" runat="server" />
</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderPageImage" runat="server">
    <img src="/_layouts/images/blank.gif" width="1" height="1" alt="">
</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <table bordercolor="#008000" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td style="width: 100%" valign="top" align="left">
                <table class="ms-formtoolbar" id="tblTopButtons" cellspacing="0" cellpadding="2"
                    width="100%" border="0">
                    <tr>
                        <td class="ms-toolbar" nowrap width="99%">
                            <img height="18" alt="" src="/_layouts/images/blank.gif" width="1" />
                        </td>
                        <td class="ms-toolbar" nowrap>
                            <SharePoint:GoBackButton ID="GoBackButton1" runat="server" ControlMode="Display" />
                        </td>
                    </tr>
                </table>
                <SharePoint:FormToolBar runat="server" ID="aaa" ControlMode="Display">
                </SharePoint:FormToolBar>
                <table class="ms-formtable" style="margin-top: 8px" cellspacing="0" cellpadding="0"
                    width="100%" border="0">
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Request Type<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblRequestType" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Telephone No<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblTelephoneNo" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Office Location<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblOfficeLocation" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Designation</nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblDesignation" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Line Manager<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblLineManager" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Remedy Change Number</nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblChangeNumber" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Date Required<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblDateRequired" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr id="MonitoringRow1" runat="server">
                        <td class="ms-formlabel" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Server Name<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblServerName" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr id="MonitoringRow2" runat="server">
                        <td class="ms-formlabel" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>IP Address<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblIPAddress" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr id="MonitoringRow3" runat="server">
                        <td class="ms-formlabel" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Is DMZ Server<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblIsDMZServer" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr id="MonitoringRow4" runat="server">
                        <td class="ms-formlabel" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Operating System<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblOperatingSystem" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr id="MonitoringRow5" runat="server">
                        <td class="ms-formlabel" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Server Class<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblServerClass" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr id="MonitoringRow6" runat="server">
                        <td class="ms-formlabel" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Oracle Installed<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblOracleInstalled" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr id="MonitoringRow7" runat="server">
                        <td class="ms-formlabel" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>MS Exchange Installed<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblMSExchangeInstalled" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr id="MonitoringRow8" runat="server">
                        <td class="ms-formlabel" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>MSSQL Installed<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblMSSQLInstalled" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr id="MonitoringRow9" runat="server">
                        <td class="ms-formlabel" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Other Applications</nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblOtherApplications" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr id="MonitoringRow10" runat="server">
                        <td class="ms-formlabel" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Log File</nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblLogFile" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr id="MonitoringRow11" runat="server">
                        <td class="ms-formlabel" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                Search String / Monitoring Condition Note
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblSearchString_MonitoringConditionNote" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr id="MonitoringRow12" runat="server">
                        <td class="ms-formlabel" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Processes / Services</nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblProcesses_Services" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr id="MonitoringRow13" runat="server">
                        <td class="ms-formlabel" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Command and User Name</nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblCommandAndUserName" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr id="ControlMRow1" runat="server">
                        <td class="ms-formlabel" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Script Name and Path<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblScriptNameAndPath" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr id="ControlMRow2" runat="server">
                        <td class="ms-formlabel" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Job Description<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblJobDescription" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr id="ControlMRow3" runat="server">
                        <td class="ms-formlabel" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Script Log File Path</nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblScriptLogFilePath" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr id="ControlMRow4" runat="server">
                        <td class="ms-formlabel" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Cyclic<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblCyclic" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr id="ControlMRow6" runat="server">
                        <td class="ms-formlabel" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Cyclic Restart Interval<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblCyclicRestartInterval" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr id="ControlMRow7" runat="server">
                        <td class="ms-formlabel" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Frequency<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblFrequency" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr id="ControlMRow8" runat="server">
                        <td class="ms-formlabel" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Condition<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblCondition" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr id="ControlMRow9" runat="server">
                        <td class="ms-formlabel" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Test Server Name</nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblTestServerName" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr id="ControlMRow10" runat="server">
                        <td class="ms-formlabel" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                <nobr>Production Server Name<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblProductionServerName" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr id="ControlMRow11" runat="server">
                        <td class="ms-formlabel" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                Is The Server Behind Firewall<span class="ms-formvalidation"> *</span>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblIsTheServerBehindFirewall" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr id="ControlMRow12" runat="server">
                        <td class="ms-formlabel" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                Mailing List Used For Notification<SPAN class="ms-formvalidation"> *</SPAN>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblMailingListUsedForNotification" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr id="ControlMRow13" runat="server">
                        <td class="ms-formlabel" valign="top" nowrap width="190">
                            <h3 class="ms-standardheader">
                                Remedy Group Used For Failure Notification<span class="ms-formvalidation"> *</span>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblRemedyGroup" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="0" width="100%">
                    <tbody>
                        <tr>
                            <td class="ms-formline">
                                <img height="1" alt="" src="/_layouts/images/blank.gif" width="1" />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table class="ms-formtoolbar" id="tblBottomButtons" cellspacing="0" cellpadding="2"
                    width="100%" border="0">
                    <tr>
                        <td class="ms-toolbar" nowrap width="99%">
                            <img height="18" alt="" src="/_layouts/images/blank.gif" width="1" />
                        </td>
                        <td class="ms-toolbar" nowrap>
                            <SharePoint:GoBackButton ID="GoBackButton2" runat="server" ControlMode="Display" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderTitleLeftBorder" runat="server">
    <table cellpadding="0" height="100%" width="100%" cellspacing="0">
        <tr>
            <td class="ms-areaseparatorleft">
                <img src="/_layouts/images/blank.gif" width="1" height="1" alt="">
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderTitleAreaClass" runat="server">

    <script id="onetidPageTitleAreaFrameScript">
        document.getElementById("onetidPageTitleAreaFrame").className = "ms-areaseparator";
    </script>

</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderBodyAreaClass" runat="server">
    <style type="text/css">
        .ms-bodyareaframe
        {
            padding: 8px;
            border: none;
        }
    </style>
</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderBodyLeftBorder" runat="server">
    <div class='ms-areaseparatorleft'>
        <img src="/_layouts/images/blank.gif" width="8" height="100%" alt=""></div>
</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderTitleRightMargin" runat="server">
    <div class='ms-areaseparatorright'>
        <img src="/_layouts/images/blank.gif" width="8" height="100%" alt=""></div>
</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderBodyRightMargin" runat="server">
    <div class='ms-areaseparatorright'>
        <img src="/_layouts/images/blank.gif" width="8" height="100%" alt=""></div>
</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderTitleAreaSeparator" runat="server" />

