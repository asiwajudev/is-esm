﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IS_ESM_SERVICE_REQUESTS
{
	class TasksListEventHandler: SPItemEventReceiver
    {
        public override void ItemDeleting(SPItemEventProperties properties)
        {
            try
            {
                if (properties.UserDisplayName.ToUpper() != "SYSTEM ACCOUNT")
                {
                    properties.Cancel = true;
                    properties.Status = SPEventReceiverStatus.CancelNoError;
                }
            }
            catch (Exception ex)
            {
                //ErrorHandler.LogToULS(ex, "TAC.NMC.INTERNET.TasksListEventHandler, ItemDeleting");
            }
        }
    }
}
